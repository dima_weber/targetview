#include "serieeditwidget.h"
#include "seriecollection.h"

#include <QtWidgets/QFormLayout>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QColorDialog>
#include <QtWidgets/QBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QColorDialog>

SerieEditWidget::SerieEditWidget(SerieCollection& collection, QWidget *parent) :
    QWidget(parent),
    collection(collection)
{
    QFormLayout* pLayout = new QFormLayout(this);

    pPositionSelect = new QComboBox(this);
    pScopeSelect = new QComboBox(this);
    pFirestyleSelect = new QComboBox(this);
    pColorEdit = new QLineEdit(this);
    QPushButton* pButton = new QPushButton("...", this);
    QHBoxLayout* pColorLayout = new QHBoxLayout();
    pColorLayout->addWidget(pColorEdit);
    pColorLayout->addWidget(pButton);
    connect (pButton, &QPushButton::clicked, [&]()
    {
        QColor col = QColorDialog::getColor();
        pColorEdit->setText(col.name());
    });
    pNameEdit = new QLineEdit(this);
    pNumberEdit = new QSpinBox(this);

    QHBoxLayout* pRangeLayout = new QHBoxLayout();
    pRangeEdit = new QSpinBox(this);
    pRangeUnit = new QComboBox(this);
    pRangeLayout->addWidget(pRangeEdit);
    pRangeLayout->addWidget(pRangeUnit);
    pRangeUnit->addItem("m", (int)Length::Unit::m);
    pRangeUnit->addItem("yard", (int)Length::Unit::yard);

    pCommentEdit = new QTextEdit(this);

    for (int i=0; i<=(int)Serie::Position::_End; i++)
        pPositionSelect->addItem(Serie::positionToString(static_cast<Serie::Position>(i)));

    for (int i=0; i<=(int)Serie::Scope::_End; i++)
        pScopeSelect->addItem(Serie::scopeToString(static_cast<Serie::Scope>(i)));

    for (int i=0; i<=(int)Serie::Firestyle::_End; i++)
        pFirestyleSelect->addItem(Serie::firestyleToString(static_cast<Serie::Firestyle>(i)));

    pLayout->addRow(tr("order num"), pNumberEdit);
    pLayout->addRow(tr("name"), pNameEdit);
    pLayout->addRow(SerieCollection::filterTypeToString(SerieCollection::FilterType::Position), pPositionSelect);
    pLayout->addRow(SerieCollection::filterTypeToString(SerieCollection::FilterType::Scope), pScopeSelect);
    pLayout->addRow(SerieCollection::filterTypeToString(SerieCollection::FilterType::Firestyle), pFirestyleSelect);
    pLayout->addRow(SerieCollection::filterTypeToString(SerieCollection::FilterType::Range), pRangeLayout);
    pLayout->addRow(tr("color"), pColorLayout);
    pLayout->addRow(tr("comment"), pCommentEdit);


    connect (pNameEdit, &QLineEdit::textChanged, [&](const QString& str)
    {
        if (serie)
            serie->setName(str);
    });

    connect (pPositionSelect, &QComboBox::currentTextChanged, [&](const QString& str)
    {
        if (serie)
            serie->position = Serie::stringToPosition(str);
    });

    connect (pScopeSelect, &QComboBox::currentTextChanged, [&](const QString& str)
    {
        if (serie)
            serie->scope = Serie::stringToScope(str);
    });

    connect (pFirestyleSelect, &QComboBox::currentTextChanged, [&](const QString& str)
    {
        if (serie)
            serie->firestyle = Serie::stringToFirestyle(str);
    });

    connect (pCommentEdit, &QTextEdit::textChanged, [&]()
    {
        if (serie)
            serie->comment = pCommentEdit->toPlainText();
    });

    connect (pRangeEdit, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), [&](int v)
    {
        if (serie)
            serie->range.setValue(v, static_cast<Length::Unit>(pRangeUnit->currentData().toInt()));
    });

    connect (pRangeUnit, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [&](int )
    {
        if (serie)
            serie->range.setValue(pRangeEdit->value(), static_cast<Length::Unit>(pRangeUnit->currentData().toInt()));
    });
}

SerieEditWidget::~SerieEditWidget()
{
}

void SerieEditWidget::setSerie(SeriePtr serie)
{
    this->serie = serie;
    pPositionSelect->setCurrentText( Serie::positionToString(serie->position));
    pScopeSelect->setCurrentText( Serie::scopeToString(serie->scope));
    pFirestyleSelect->setCurrentText( Serie::firestyleToString(serie->firestyle));
    pColorEdit->setText(serie->getColor().name());
    pNameEdit->setText(serie->name());
    pNumberEdit->setValue(serie->number);
    pRangeEdit->setValue(serie->range.value(Length::Unit::m));
    pRangeUnit->setCurrentText("m");
    pCommentEdit->setText(serie->comment);
}


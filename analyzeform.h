#ifndef ANALYZEFORM_H
#define ANALYZEFORM_H

#include <QDialog>
#include "seriecollection.h"

namespace Ui {
class AnalyzeForm;
}

class AnalyzeForm : public QDialog
{
    Q_OBJECT
public:
    explicit AnalyzeForm(SerieCollection& collection, QWidget *parent = 0);
    ~AnalyzeForm();

private:
    Ui::AnalyzeForm *ui;
    SerieCollection& collection;

    void drawShotsBar(const QMap<QString, QVector<SeriePtr>>& data);
    void drawScoreGraph(const QMap<QString, QVector<SeriePtr>>& data);
    void drawColorMap(const QMap<QString, QVector<SeriePtr>>& data);
    void drawDispersionGraph(const QMap<QString, QVector<SeriePtr>>& data);

public slots:
    void onFilterChanged();
};

#endif // ANALYZEFORM_H

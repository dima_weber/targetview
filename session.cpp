#include "session.h"
#include "types.h"
#include "targetbuilder.h"

#include <QtCore/QFile>

#include <QtWidgets/QPushButton>
#include <QtWidgets/QGraphicsItemGroup>

void Session::selectSerie(SeriePtr p)
{
    selectedSeries.append(p);
    invalidateCache();
}

void Session::unselectSerie(SeriePtr p)
{
    selectedSeries.removeOne(p);
    invalidateCache();
}

Holes Session::holes() const
{
   Holes vec;
   for(SeriePtr spSerie: selectedSeries)
   {
        if (spSerie)
            vec.mutexAppend( spSerie->holes());
   }
   return vec;
}

Session::Session()
    :Serie(nullptr)
{

}

Session::~Session()
{
    clear();
    //delete pSessionGroup;
}

QColor Session::getColor() const
{
    QColor color = Serie::getColor();
    color.setAlpha(50);
    return color;
}

void Session::clear()
{
    series.clear();
}

bool Session::save(Sessions sessions, const QString& filePath, Session::SaveFormat format)
{
    bool ok;
    QFile file(filePath);
    ok = file.open(QFile::WriteOnly);
    if (ok)
    {
        switch (format)
        {
            case Session::SaveFormat::Csv:
                qWarning() << "NOT IMPLEMENTED YET";
                ok = false;
                break;
            case Session::SaveFormat::Xml:
                {
                    QDomDocument doc("doc");
                    QDomElement root = doc.createElement("targetshow");
                    doc.appendChild(root);
                    root.setAttribute("version", 1);
                    QVector<SessionPtr> vec = QVector<SessionPtr>::fromList(sessions.values());
                    writeChildArray(doc, root, "sessions", "session", vec);
                    QTextStream stream(&file);
                    doc.save(stream, 1);
                    ok = true;
                    break;
                }
            default:
                qWarning() << "NOT IMPLEMENTED YET";
                ok = false;
                break;
        }
        file.close();
    }
    return ok;
}

QString Session::name() const
{
    return uuid();
}

QDate Session::date() const
{
    return _date;
}

Caliber Session::caliber() const
{
    return _caliber;
}

const QString& Session::targetName() const
{
    return _targetName;
}

const QString& Session::shooter() const
{
    return _shooter;
}

void Session::setCaliber(const Caliber& caliber)
{
    _caliber = caliber;
}

void Session::setDate(const QDate& date)
{
    _date = date;
}

void Session::setTargetName(const QString& target)
{
    _targetName = target;
    _targetCache.reset();
}

void Session::setShooter(const QString& shooter)
{
    _shooter = shooter;
}

TargetPtr Session::target() const
{
    if (!_targetCache)
        _targetCache = TargetBuilder::ref().target(targetName());
    return _targetCache;
}

void Session::addSerie(const SeriePtr& serie)
{
    series.mutexAppend(serie);
    serie->setSession(shared_from_this());
    selectSerie(serie);
}

bool readXmlTag(const QDomElement& sessionElement, Session& session, const ReadXmlContext& context, bool optional)
{
    ReadXmlContext modifiedContext = context.clone();
    Caliber caliber;
    QDate date;
    QString shooter;
    QString target;
    bool ok;

    QDomElement scaleElement = sessionElement.firstChildElement("scale");
    Length imageSize;
    Length realSize;

    ok =    readChildTag(scaleElement, "image", imageSize, context)
            && readChildTag(scaleElement, "real", realSize, context);
    if (ok)
    {
        modifiedContext.dpm =
        session.dpm = imageSize / realSize;
    }

    if (ok)
    {
        ok =  readChildTag(sessionElement, "caliber", caliber, modifiedContext)
                && readChildTag(sessionElement, "date", date, modifiedContext, false)
                && readChildTag(sessionElement, "target", target, modifiedContext, false)
                && readChildTag(sessionElement, "shooter", shooter, modifiedContext);

        session.setCaliber(caliber);
        session.setDate(date);
        session.setShooter(shooter);
        session.setTargetName(target);
    }

    if (ok)
    {
        using namespace std::placeholders;
        ProcessFunction<SeriesItem> f = std::bind(&Session::addSerie, &session, _1);
        ok = readChildArray(sessionElement, "series", "serie", f, modifiedContext);
    }

    session.setUuid(sessionElement.attribute("uuid"));

    return ok || optional;
}

bool writeXmlTag(QDomDocument &doc, QDomElement& sessionElement, const Session& session)
{
    sessionElement.setAttribute("uuid", session.uuid());

    writeChildTag(doc, sessionElement, "caliber", session.caliber());
    writeChildTag(doc, sessionElement, "date", session.date());
    writeChildTag(doc, sessionElement, "target", session.targetName());
    writeChildTag(doc, sessionElement, "shooter", session.shooter());

    QDomElement scaleElement = doc.createElement("scale");
    Length l(1);
    writeChildTag(doc, scaleElement, "image", l);
    writeChildTag(doc, scaleElement, "real", l);
    sessionElement.appendChild(scaleElement);

    writeChildArray(doc, sessionElement, "series", "serie", session.series);

    return true;
}


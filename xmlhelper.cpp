#include "xmlhelper.h"
#include "types.h"

#include <QtCore/QPointF>
#include <QtCore/QDebug>
#include <QtCore/QDate>

#include <QtScript/QScriptEngine>

#include <QtGui/QPolygonF>
#include <QtGui/QColor>

#include <QtXml/QDomElement>

bool readXmlTag(const QDomElement& intElement, int& value, const ReadXmlContext& context, bool optional)
{
    QString stringRepresentation;
    bool ok = readXmlTag(intElement, stringRepresentation, context, false);
    if(ok)
    {
        int radix=10;
        if (stringRepresentation.startsWith("#"))
        {
            stringRepresentation.remove(0,1);
            radix=16;
        }
        value = stringRepresentation.toInt(&ok, radix);
        if (!ok)
        {
            QScriptValue expressionValue = context.pScriptEngine->evaluate(stringRepresentation);
            if (expressionValue.isNumber())
                value = expressionValue.toNumber();
            else
                qWarning() << QString("Tag %1 present, but it's value %2 is not int").arg(intElement.tagName()).arg(intElement.text());
        }
    }
    else
        qWarning() << QString("no value for tag %1 found").arg(intElement.tagName());

    return ok || optional;
}

bool writeXmlTag(QDomDocument &doc, QDomElement& intElement, int value)
{
    return writeXmlTag(doc, intElement, QString::number(value));
}

bool readXmlTag(const QDomElement& doubleElement, double& value, const ReadXmlContext& context, bool optional)
{
    QString stringRepresentation;
    bool ok = readXmlTag(doubleElement, stringRepresentation, context, false);
    if(ok)
    {
        value = stringRepresentation.toDouble(&ok);
        if (!ok)
        {
            QScriptValue expressionValue = context.pScriptEngine->evaluate(stringRepresentation);
            if (expressionValue.isNumber())
                value = expressionValue.toNumber();
            else
                qWarning() << QString("Tag %1 present, but it's value %2 is not double").arg(doubleElement.tagName()).arg(doubleElement.text());
        }
    }
    else
        qWarning() << QString("no value for tag %1 found").arg(doubleElement.tagName());

    return ok || optional;
}

bool writeXmlTag(QDomDocument &doc, QDomElement& doubleElement, double value)
{
    return writeXmlTag(doc, doubleElement, QString::number(value));
}

bool readXmlTag(const QDomElement& lengthElement, Length& value, const ReadXmlContext& context, bool optional)
{
    bool ok = !lengthElement.isNull();
    if (ok)
    {
        double v = 0;
        ok = readXmlTag(lengthElement, v, context);
        v  *= context.scale;
        if (ok)
        {
            QString unit = lengthElement.attribute("unit", "mm");
            if (unit=="mm" || unit == "milimeter")
                value.setValue(v, Length::Unit::mm);
            else if (unit=="cm" || unit == "centimeter")
                value.setValue(v, Length::Unit::cm);
            else if (unit == "inch")
                value.setValue(v, Length::Unit::inch);
            else if (unit == "m" || unit == "meter")
                value.setValue(v, Length::Unit::m);
            else if (unit == "y" || unit == "yard")
                value.setValue(v, Length::Unit::yard);
            else if (unit == "px" || unit == "pixel")
                value.setValue(v, Length::Unit::px, context.dpm);
            else
                qWarning() << "unknonw length unit";
        }
    }
    return ok || optional;
}

bool readXmlTag(const QDomElement &caliberElement, Caliber &caliber, const ReadXmlContext& context, bool optional)
{
    bool ok;
    ok = readXmlTag(caliberElement, static_cast<Length&>(caliber), context, optional);
    if (ok)
    {
        QString name = caliberElement.attribute("name", ".223 Rem");
        caliber.setName(name);
    }
    return ok | optional;
}

bool writeXmlTag(QDomDocument &doc, QDomElement &caliberElement, const Caliber &value)
{
    bool ok = writeXmlTag(doc, caliberElement, static_cast<const Length&>(value));
    caliberElement.setAttribute("name", value.name());
    return ok;
}

bool readXmlTag(const QDomElement& stringElement, QString& value, const ReadXmlContext& /*context*/, bool optional)
{
    bool ok = false;
    if(!stringElement.isNull())
    {
        ok = true;
        value = stringElement.text();
    }
    else
        qWarning() << QString("no child tag %1 found").arg(stringElement.tagName());

    return ok || optional;
}

bool readXmlTag(const QDomElement& sizeElement, FontSize& fontSize, const ReadXmlContext& context, bool optional)
{
    bool ok = !sizeElement.isNull();

    if (ok)
    {
        ok = readXmlTag(sizeElement, fontSize.size, context);
        fontSize.size  *= context.scale;
        if (ok)
        {
            fontSize.unit = sizeElement.attribute("unit", "px");
        }
    }

    return ok || optional;
}

bool readXmlTag(const QDomElement& angleElement, Angle& value, const ReadXmlContext& context, bool optional)
{
    bool ok = false;
    if (!angleElement.isNull())
    {
        double v;
        ok = readXmlTag(angleElement, v, context);
        if (ok)
        {
            if (angleElement.attribute("unit", "degree") == "radian")
                value.setValue(v, Angle::Unit::rad);
            else
                value.setValue(v, Angle::Unit::deg);
        }
    }
    return ok || optional;
}

bool readXmlTag(const QDomElement& pointElement, QPointF& value, const ReadXmlContext& context, bool optional)
{
    bool ok = false;
    if (!pointElement.isNull())
    {
        double x;
        double y;

        QString system = pointElement.attribute("system", "cartesian");
        if (system == "cartesian")
        {
            Length x_l;
            Length y_l;
            ok =    readChildTag(pointElement, "x", x_l, context)
                 && readChildTag(pointElement, "y", y_l, context);
            x = x_l.value(Length::Unit::mm);
            y = y_l.value(Length::Unit::mm);
        }
        else if (system == "polar")
        {
            Angle angle;
            Length radius;

            ok =    readChildTag(pointElement, "radius", radius, context)
                 && readChildTag(pointElement, "angle", angle, context);

            x = radius.value(Length::Unit::mm) * angle.cos();
            y = radius.value(Length::Unit::mm) * angle.sin();
        }
        else
        {
            qWarning() << "unknown coordinate system " << system;
            ok = false;
        }
        if (ok)
        {
            value.setX(x);
            value.setY(-y);
        }
    }
    else
        qWarning() << QString("no child tag %1 found").arg(pointElement.tagName());
    return ok || optional;
}

bool readXmlTag(const QDomElement& polygonElement, QPolygonF& poly, const ReadXmlContext& context, bool optional)
{
    bool ok = false;

    if (!polygonElement.isNull())
    {
        QPointF point;
        QDomNodeList pointList = polygonElement.elementsByTagName("point");
        for(int pointIdx =0; pointIdx<pointList.size(); pointIdx++)
        {
            QDomNode pointNode = pointList.item(pointIdx);
            ok = readXmlTag(pointNode.toElement(), point, context);
            if (ok)
                poly.append(point);
            else
                qWarning() << "fail to read point in point list";
        }
    }
    else
        qWarning() << QString("no child tag %1 found").arg(polygonElement.tagName());

    return ok || optional;
}

bool readXmlTag(const QDomElement& colorElement, QColor& color, const ReadXmlContext& /*context*/, bool optional)
{
    bool ok = false;

    if(!colorElement.isNull())
    {
        QString s = colorElement.text();
        color.setNamedColor(s);
        ok = color.isValid();

        if (colorElement.hasAttribute("alpha"))
        {
            s = colorElement.attribute("alpha");
            int radix=10;
            if (s.startsWith("#"))
            {
                radix = 16;
                s.remove(0,1);
            }
            int alpha = s.toInt(&ok, radix);
            color.setAlpha(alpha);
        }
    }
    else
        qWarning() << QString("no child tag %1 found").arg(colorElement.tagName());
    return ok || optional;
}


bool readXmlTag(const QDomElement& dateElement, QDate& date, const ReadXmlContext& /*context*/, bool optional)
{
    bool ok = !dateElement.isNull();
    if (ok)
    {
        QString format = dateElement.attribute("format", "yyyy-MM-dd");
        date = QDate::fromString(dateElement.text(), format);
    }
    return ok || optional;
}

bool writeXmlTag(QDomDocument& doc, QDomElement &lengthElement, const Length &value)
{
    lengthElement.setAttribute("unit", "mm");
    writeXmlTag(doc, lengthElement, value.value(Length::Unit::mm));
    return true;
}

bool writeXmlTag(QDomDocument& doc, QDomElement &stringElement, const QString &value)
{
    QDomText t = doc.createTextNode(value);
    return !stringElement.appendChild(t).isNull();
}

bool writeXmlTag(QDomDocument& doc, QDomElement &dateElement, const QDate &date)
{
    QString format = "yyyy-MM-dd";
    dateElement.setAttribute("format", format);
    return writeXmlTag(doc, dateElement, date.toString(format));
}

bool writeXmlTag(QDomDocument &doc, QDomElement &pointElement, const QPointF &point)
{
    Length x;
    Length y;
    x.setValue(point.x(), Length::Unit::mm);
    y.setValue(-point.y(), Length::Unit::mm);
    pointElement.setAttribute("system", "cartesian");
    bool ok =    writeChildTag(doc, pointElement, "x", x)
              && writeChildTag(doc, pointElement, "y", y);
    return ok;
}

bool writeXmlTag(QDomDocument &doc, QDomElement &polarElement, const PolarCoordinate& coord)
{
//    Length radius;
//    Angle angle;
//    radius.setValue(coord.radius());
//    angle.setDegree(coord.angle());

//    polarElement.setAttribute("system", "polar");
    bool ok =    writeXmlTag(doc, polarElement, coord.coord());
    return ok;
}

bool writeXmlTag(QDomDocument &doc, QDomElement &angleElement, const Angle &angle)
{
    angleElement.setAttribute("unit", "degree");
    return writeXmlTag(doc, angleElement, angle.value(Angle::Unit::deg));
}

bool readXmlTag(const QDomElement& polarElement, PolarCoordinate& coord, const ReadXmlContext& context, bool optional)
{
    bool ok;
    QPointF point;
    ok = readXmlTag(polarElement, point, context, optional);
    coord = PolarCoordinate(point);
    return ok;
}

bool writeXmlTag(QDomDocument& doc, QDomElement& colorElement, const QColor& color)
{
    if (color.alpha() != 255)
        colorElement.setAttribute("alpha", color.alpha());
    return writeXmlTag(doc, colorElement, color.name(QColor::HexRgb));
}

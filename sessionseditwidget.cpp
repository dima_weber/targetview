#include "sessionseditwidget.h"
#include "sessioneditwidget.h"
#include "session.h"

#include <QtWidgets/QListWidget>
#include <QtWidgets/QBoxLayout>

SessionsEditWidget::SessionsEditWidget(Sessions& sessions, QWidget *parent) :
    QWidget(parent),
    sessions(sessions),
    collection(sessions)
{
    QBoxLayout* pLayout = new QHBoxLayout(this);
    pSessionList = new QListWidget(this);
    pSessionEdit = new SessionEditWidget(collection, this);

    pLayout->addWidget(pSessionList);
    pLayout->addWidget(pSessionEdit);

    for(QString sessionName: sessions.keys())
    {
        pSessionList->addItem(sessionName);
    }

    connect (pSessionList, &QListWidget::currentTextChanged, [&](const QString& name)
    {
       pSessionEdit->setSession(sessions[name]);
    });
}

SessionsEditWidget::~SessionsEditWidget()
{
}

#ifndef TYPES_H
#define TYPES_H

#include "units.h"

#include <QtCore/QRectF>
#include <QtCore/QPointF>
#include <QtCore/QtMath>
#include <QtCore/QDebug>
#include <QVector>
#include <QMutex>
#include <QMutexLocker>

#include <QtGui/QFont>
#include <QtGui/QColor>

#include <memory>

double square_dist(const QPointF& a, const QPointF& b = QPointF(0,0));
double dist(const QPointF& a, const QPointF& b = QPoint(0,0));


class FontSize
{
public:
	int size;
	QString unit;
	FontSize(int s =14, const QString& u = QStringLiteral("pt"))
		:size(s), unit(u)
	{}

	QFont setFontSize(QFont& font)
	{
		if (unit == QStringLiteral("pt"))
			font.setPointSize(size?size:1);
		else if (unit == QStringLiteral("px"))
			font.setPixelSize(size?size:1);
		else
			qWarning() << "unknonwn size unit";

		return font;
	}
};


class Caliber : public Length
{
public:
	Caliber(const QString& name, double length = 0, Unit u = Unit::mm, double dpm=1)
		:Length(length, u, dpm), _name(name)
	{}
	Caliber (double length = 0, Unit u = Unit::mm, double dpm=1)
		:Length(length, u, dpm), _name(QStringLiteral(".223 Rem"))
	{}
	QString name() const
	{ return _name; }
	void setName(const QString& name)
	{ this->_name = name; }

private:
	QString _name;
};

class PolarCoordinate
{
private:
	double _radius;
	Angle _angle;
	QPointF _coord;

	void calculcateCartesianCoordinates()
	{
		_coord.setX( _radius * _angle.cos() );
		_coord.setY( _radius * _angle.sin() );
	}

public:

	const QPointF& coord() const
	{
		return _coord;
	}

	double x() const
	{
		return coord().x();
	}

	double y() const
	{
		return coord().y();
	}

	double radius() const
	{
		return _radius;
	}

	double angle() const
	{
		return _angle.value(Angle::Unit::deg);
	}

	PolarCoordinate operator +(const PolarCoordinate& other) const
	{
		return PolarCoordinate(coord() + other.coord());
	}

	PolarCoordinate operator - (const PolarCoordinate& other) const
	{
		return PolarCoordinate(coord() - other.coord());
	}

	double distanceTo (const PolarCoordinate& other = PolarCoordinate(0,0)) const
	{
		return dist(coord(), other.coord());
	}

	PolarCoordinate()
		:_radius(0),_angle(0), _coord(0,0)
	{}

	PolarCoordinate(const QPointF& coord)
	{
		_radius = dist(coord);
		_angle.setValue(qAtan2(coord.y(), coord.x()), Angle::Unit::rad);
		_coord = coord;
	}

	PolarCoordinate(double r, double a)
		:_radius(r), _angle(a)
	{
		calculcateCartesianCoordinates();
	}

	PolarCoordinate(const PolarCoordinate& a):_radius(a._radius), _angle(a._angle), _coord(a._coord){}

	operator QPointF() const {return coord(); }
};

template <class T>
class MutexVector : public QVector<T>
{
	QMutex mutex;
public:
	MutexVector()
		:QVector<T>()
	{}
	MutexVector(const MutexVector<T>& other)
		:QVector<T>(other)
	{}

	void mutexAppend(const T& t)
	{
		QMutexLocker lock(&mutex);
		QVector<T>::append(t);
	}
	void mutexAppend(const QVector<T>& v)
	{
		QMutexLocker lock(&mutex);
		QVector<T>::append(v);
	}

private:
	using QVector<T>::append;
};


struct TargetParams
{
	Length rangeBegin;
	Length rangeEnd;
	Length rangeStep;
	Length targetHeight;
	Length targetWidth;
};

QColor getSerieColor(int num);

class Session;
class Serie;
class Hole;

typedef std::shared_ptr<Serie> SeriePtr;
typedef std::weak_ptr<Serie> SerieWPtr;
typedef SeriePtr SeriesItem;
typedef MutexVector<SeriesItem> Series;

typedef std::shared_ptr<Session> SessionPtr;
typedef std::weak_ptr<Session> SessionWPtr;
typedef SessionPtr SessionsItem;
typedef QMap<QString, SessionsItem> Sessions;

typedef std::shared_ptr<Hole> HolePtr;
typedef std::weak_ptr<Hole> HoleWPtr;
typedef HolePtr HolesItem;
typedef MutexVector<HolesItem> Holes;

#endif // TYPES_H


#include "guiserie.h"

#include <QtWidgets/QGraphicsScene>
#include <QtWidgets/QGraphicsItem>
#include <QtWidgets/QPushButton>

GuiSerie::GuiSerie(QGraphicsScene& scene, SeriePtr serie, QWidget* parent)
    :QObject (parent),
     scene(scene), wpSerie(serie), pSerieGui (scene.createItemGroup(QList<QGraphicsItem*>())),
    _pGuiMassCenter(nullptr), _pGuiBounds(nullptr), _pGuiMaxDistance(nullptr),
    _pGuiMoaCircles(nullptr), _pGuiMilradCircles(nullptr), _pGuiHoles(nullptr)
{
//	pSerieGui = new QGraphicsItemGroup();
//	scene.addItem(pSerieGui);

//	scene.addItem(guiHoles());
//	scene.addItem(guiBounds());
//	scene.addItem(guiMassCenter());
//	scene.addItem(guiMaxDistance());
//	scene.addItem(guiMilradCircles());
//	scene.addItem(guiMoaCircles());

    pSerieGui->addToGroup(guiHoles());
    pSerieGui->addToGroup(guiBounds());
    pSerieGui->addToGroup(guiMassCenter());
    pSerieGui->addToGroup(guiMaxDistance());
    pSerieGui->addToGroup(guiMilradCircles());
    pSerieGui->addToGroup(guiMoaCircles());

    pButton = new QPushButton(parent);

    pButton->setCheckable(true);
    pButton->setChecked(true);
    pButton->setText(QString("Serie '%1'").arg(serie->name()));
    pButton->setVisible(parent != nullptr);

    connect (pButton, &QPushButton::toggled, [=](bool on)
    {
        pSerieGui->setVisible(on);
    });
}

GuiSerie::~GuiSerie()
{
    clear();
}

void GuiSerie::showMassCenter(bool show)
{
    guiMassCenter()->setVisible(show);
}

void GuiSerie::showBoundingRect(bool show)
{
    guiBounds()->setVisible(show);
}

void GuiSerie::showMaxDistance(bool show)
{
    guiMaxDistance()->setVisible(show);
}

void GuiSerie::showCircle(GuiSerie::CircleType type)
{
    guiMoaCircles()->setVisible(type == GuiSerie::CircleType::MOA);
    guiMilradCircles()->setVisible(type == GuiSerie::CircleType::MilRad);
}

void GuiSerie::invalidate()
{
    _pGuiBounds.reset();
    _pGuiHoles.reset();
    _pGuiMassCenter.reset();
    _pGuiMaxDistance.reset();
    _pGuiMilradCircles.reset();
    _pGuiMoaCircles.reset();
}

SerieWPtr GuiSerie::serie()
{
    return wpSerie;
}

void GuiSerie::clear()
{
    scene.destroyItemGroup(pSerieGui);
//    delete pSerieGui;
    pSerieGui = nullptr;

    delete pButton;
    pButton = nullptr;
}

QGraphicsItem* GuiSerie::guiMassCenter()
{
    if (!_pGuiMassCenter)
        _pGuiMassCenter.reset(buildGuiMassCenter());
    return _pGuiMassCenter.get();
}

QGraphicsItem* GuiSerie::guiBounds()
{
    if (!_pGuiBounds)
        _pGuiBounds.reset(buildGuiBounds());
    return _pGuiBounds.get();
}

QGraphicsItem* GuiSerie::guiMaxDistance()
{
    if (!_pGuiMaxDistance)
        _pGuiMaxDistance.reset(buildGuiMaxDistance());
    return _pGuiMaxDistance.get();
}

QGraphicsItem* GuiSerie::guiMoaCircles()
{
    SeriePtr spSerie = wpSerie.lock();
    if (!_pGuiMoaCircles)
        _pGuiMoaCircles.reset(buildGuiCircles(spSerie->range.value(Length::Unit::m) / 2 / 3.4377));
    return _pGuiMoaCircles.get();
}

QGraphicsItem* GuiSerie::guiMilradCircles()
{
    SeriePtr spSerie = wpSerie.lock();
    if (!_pGuiMilradCircles)
        _pGuiMilradCircles.reset(buildGuiCircles(spSerie->range.value(Length::Unit::m) / 2));
    return _pGuiMilradCircles.get();
}

QGraphicsItem* GuiSerie::guiHoles()
{
    if (!_pGuiHoles)
        _pGuiHoles.reset(buildGuiHoles());
    return _pGuiHoles.get();
}

void GuiSerie::addGuiSerie(GuiSeriePtr spGuiSerie)
{
//    pSerieGui->addToGroup(spGuiSerie->pSerieGui);
    subGuis.append(spGuiSerie);
}

void GuiSerie::setVisible(bool show)
{
    pSerieGui->setVisible(show);
    for (GuiSerieWPtr wp : subGuis)
    {
        GuiSeriePtr sp = wp.lock();
        if (sp)
        {
            sp->pButton->setVisible(false);
        }
    }
}

const QPen& GuiSerie::seriePen()
{
    if (!_seriePenCache)
    {
        QColor color;
        SeriePtr spSerie = wpSerie.lock();
        if (spSerie)
        {
            color = spSerie->getColor();
            //color.setAlpha(200);
        }
        _seriePenCache.reset(new QPen(color, 2));
    }
    return *_seriePenCache;
}

const QPen& GuiSerie::holePen()
{
    if (!_holePenCache)
        _holePenCache.reset(new QPen(QBrush(Qt::black), 2));
    return *_holePenCache;
}

const QPen& GuiSerie::textPen()
{
    if (!_textPenCache)
        _textPenCache.reset(new QPen(QBrush(Qt::black), 1));
    return *_textPenCache;
}

const QBrush& GuiSerie::holeBrush()
{
    if (!_holeBrushCache)
    {
        SeriePtr spSerie = wpSerie.lock();
        QColor color;
        if (spSerie)
        {
            color = spSerie->getColor();
            //color.setAlpha(200);
        }
        _holeBrushCache.reset(new QBrush(color));
    }
    return *_holeBrushCache;
}

QGraphicsItem* GuiSerie::buildGuiMaxDistance()
{
    SeriePtr spSerie = wpSerie.lock();
    QGraphicsItemGroup* pGui = new QGraphicsItemGroup();
    if (spSerie)
    {
        QLineF maxDistLine(spSerie->mostDistantPoints().first, spSerie->mostDistantPoints().second);
        QGraphicsLineItem* pLine = new QGraphicsLineItem(maxDistLine, pGui);
        pLine->setPen(seriePen());
    }
    return pGui;
}

QGraphicsItem* GuiSerie::buildGuiCircles(int radius)
{
    SeriePtr spSerie = wpSerie.lock();
    QGraphicsItemGroup* pGui = new QGraphicsItemGroup();
    if (radius > 0 && spSerie)
    {
        QRectF rect;
        double x = spSerie->massCenter().x();
        double y = spSerie->massCenter().y();
        int radNum = 0;
        double milRadRadius = 0;
        QGraphicsEllipseItem* pCirle;
        do
        {
            radNum++;
            milRadRadius =  radNum * radius;

            rect.setLeft(x - milRadRadius);
            rect.setRight(x + milRadRadius);
            rect.setTop(y -  milRadRadius);
            rect.setBottom(y +  milRadRadius);
            pCirle = new QGraphicsEllipseItem(rect, pGui);
            pCirle->setPen(seriePen());
            pGui->addToGroup(pCirle);
        } while (milRadRadius <= spSerie->maxRadius() );
    }
    return pGui;
}

QGraphicsItem* GuiSerie::buildGuiHoles()
{
    SeriePtr spSerie = wpSerie.lock();
    QGraphicsItemGroup* pGui = new QGraphicsItemGroup();
    if (spSerie)
    {
        for(HolesItem& hole: spSerie->holes())
        {
            QPointF coord = hole->coord();
            int value = spSerie->holeValue(coord);

            // draw
            QGraphicsItem* pShape = buildHole(hole, spSerie->caliber(), spSerie->getShape());
            pGui->addToGroup( pShape);

            QPointF delta = QPointF(spSerie->caliber().value(Length::Unit::mm), spSerie->caliber().value(Length::Unit::mm));
            QGraphicsSimpleTextItem* pText = new QGraphicsSimpleTextItem(QString::number(value), pShape); //holeIdx+1
            pText->setPen(textPen());
            QFont textFont = pText->font();
            textFont.setPixelSize(5);
            pText->setFont(textFont);
            QRectF holeTextRect = pText->boundingRect();
            delta.setX(holeTextRect.width()/2);
            delta.setY(holeTextRect.height()/2);
            pText->moveBy(coord.x() - delta.x(), coord.y() - delta.y());

            pText->setParentItem(pShape);
        }
    }

    return pGui;
}

QGraphicsItem* GuiSerie::buildHole(const HolePtr& hole, Length caliber, Serie::HoleShape shape)
{
    auto polyFromCircle = [](double radius, int n) -> QPolygonF
    {
        static QMap<double, QPolygonF> polygons; // key is some kind of checksum of n and r;

        double key = n * 1000 + radius;
        if (polygons.contains(key))
            return polygons[key];

        QPolygonF poly;
        double R = radius / qCos(3.1415 / n);
        for  (double angle = 0; angle <= 360; angle += (360 / n))
        {
            double x = R * qCos(qDegreesToRadians(angle));
            double y = R * qSin(qDegreesToRadians(angle));
            poly.append(QPointF(x,y));
        }
        polygons[key] = poly;
        return poly;
    };


    QAbstractGraphicsShapeItem * pHoleItem = nullptr;
    double caliberRadius = caliber.value(Length::Unit::mm);
    QPointF coord (hole->coord().x() - caliberRadius, hole->coord().y()-caliberRadius);
    QRectF holeRect (coord, QSize(2*caliberRadius, 2*caliberRadius));
    switch (shape)
    {
        default:
        case Serie::HoleShape::CrossCirle:
        case Serie::HoleShape::PlusCirlce:
        case Serie::HoleShape::Unknown:
        case Serie::HoleShape::Cirle:
            pHoleItem = new QGraphicsEllipseItem(holeRect);
        break;
        case Serie::HoleShape::Square:
            pHoleItem = new QGraphicsRectItem(holeRect);
            break;
        case Serie::HoleShape::Triangle:
        case Serie::HoleShape::Diamond:
        case Serie::HoleShape::Pentagon:
        case Serie::HoleShape::Hexagon:
        {
            QPolygonF poly = polyFromCircle(caliberRadius, static_cast<int>(shape));
            poly.translate(hole->coord());
            pHoleItem = new QGraphicsPolygonItem(poly);
        }
    }

    pHoleItem->setToolTip(QString("%1 mm").arg(QString::number(qSqrt(square_dist(coord)), 'f', 1)));
    pHoleItem->setPen(holePen());
    pHoleItem->setBrush(holeBrush());

    return pHoleItem;
}

QGraphicsItem* GuiSerie::buildGuiMassCenter()
{
    SeriePtr spSerie = wpSerie.lock();
    QGraphicsItemGroup* pGui = new QGraphicsItemGroup();
    if (spSerie)
    {
        PolarCoordinate massCoord = spSerie->massCenter();
        double x = massCoord.x();
        double y = massCoord.y();

        QGraphicsLineItem* pLine;
        pLine = new QGraphicsLineItem(x-3,y-3,x+3,y+3, pGui);
        pLine->setPen(seriePen());
        pGui->addToGroup(pLine);
        pLine = new QGraphicsLineItem(x-3,y+3,x+3,y-3, pGui);
        pLine->setPen(seriePen());
        pGui->addToGroup(pLine);
    }
    return pGui;
}

QGraphicsItem* GuiSerie::buildGuiBounds()
{
    SeriePtr spSerie = wpSerie.lock();
    QGraphicsItemGroup* pGui = new QGraphicsItemGroup();
    if (spSerie)
    {
        QRectF boundingRect = spSerie->boundingRect(); //pSerieGui->boundingRect();
        QGraphicsRectItem* pRect = new QGraphicsRectItem(boundingRect, pGui);
        pRect->setPen(seriePen());
    }
    return pGui;
}

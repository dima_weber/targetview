#ifndef TARGET_H
#define TARGET_H

#include <QString>
#include <QSize>

#include <QtWidgets/QGraphicsItemGroup>
#include <QObject>

#include <memory>

struct Zone;
struct Target;
class QDomElement;
struct ReadXmlContext;
class QGraphicsItem;
class QPointF;

class ClippedGroup: public QObject, public QGraphicsItemGroup
{
	Q_OBJECT
	Q_PROPERTY(QPointF pos READ pos WRITE setPos)
	Q_PROPERTY(qreal scale READ scale WRITE setScale)

	QPainterPath path;
	bool pathSet;
public:
	ClippedGroup(QGraphicsItem* parent =0);
	virtual ~ClippedGroup();
	void setShape(const QPainterPath& p);

	virtual QRectF boundingRect() const;
	virtual QPainterPath shape() const;
};

struct Zone
{
	int value;
	bool is_clip;
	QString caption;
	std::unique_ptr<ClippedGroup> pImage;
	QPainterPath path;

	Zone();

	void bw();

//	Zone(const Target& target);
};

typedef std::shared_ptr<Zone> ZonePtr;
typedef std::unique_ptr<Zone> ZoneUPtr;
typedef Zone* ZonesItem;
typedef QVector<ZonesItem> Zones;

bool operator < (const QPointF& a, const QPointF& b);

struct Target
{
	QString name;
	std::unique_ptr<ClippedGroup> pImage;
	Zones zones;

	QMap<char, QPointF> destPoints;
	QStringList quadPoints;

	void addZone(const ZonesItem& zone);

	Target();
	~Target()
	{
		//qDeleteAll(zones);
	}

	void bw()
	{
		for(ZonesItem item: zones)
			item->bw();
	}

	QSizeF size() const;

	int getValue(const QPointF& p);

	ClippedGroup* release()
	{
		zones.clear();
		return pImage.release();
	}

private:
	QMap<QPointF, int> _pointValueCache;
};

struct DefaultTarget : public Target
{
	DefaultTarget();
};

typedef std::shared_ptr<Target> TargetPtr;

bool readXmlTag(const QDomElement& targetElement, Target& target, const ReadXmlContext& context, bool optional = true);
bool readXmlTag(const QDomElement& zoneElement, Zone& zone, const ReadXmlContext& context, bool optional = true);

#endif // TARGET_H


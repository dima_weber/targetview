#ifndef TARGETPARAMSWIDGET_H
#define TARGETPARAMSWIDGET_H

#include <QtWidgets/QWidget>

namespace Ui {
class TargetParamsWidget;
}

struct TargetParams;

class TargetParamsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TargetParamsWidget(QWidget *parent = 0);
    ~TargetParamsWidget();

    const TargetParams& params() const;
    void setParams(const TargetParams& param);

signals:
    void changed();

private:
    Ui::TargetParamsWidget *ui;
};

#endif // TARGETPARAMSWIDGET_H

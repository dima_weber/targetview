<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU" sourcelanguage="en">
<context>
    <name>AnalyzeForm</name>
    <message>
        <location filename="analyzeform.ui" line="14"/>
        <source>Dialog</source>
        <translation>Анализ</translation>
    </message>
    <message>
        <location filename="analyzeform.ui" line="59"/>
        <source>Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="analyzeform.ui" line="64"/>
        <location filename="analyzeform.cpp" line="26"/>
        <location filename="analyzeform.cpp" line="36"/>
        <source>Score</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="analyzeform.ui" line="69"/>
        <source>Color map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="analyzeform.ui" line="155"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="analyzeform.cpp" line="19"/>
        <source>Serie score</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="analyzeform.cpp" line="25"/>
        <source>Serie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="analyzeform.cpp" line="30"/>
        <source>Shots distribution per serie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="analyzeform.cpp" line="37"/>
        <source>Shots</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="analyzeform.cpp" line="56"/>
        <location filename="analyzeform.cpp" line="61"/>
        <source>Save image</source>
        <translation>Сохранить изображение</translation>
    </message>
    <message>
        <location filename="analyzeform.cpp" line="62"/>
        <source>PNG Image (*.png)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="analyzeform.cpp" line="244"/>
        <source>Shots density</source>
        <translation>Плотность выстрелов</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Мишень!</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="28"/>
        <source>&amp;File</source>
        <translation>Ф&amp;айл</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="37"/>
        <source>&amp;Session</source>
        <translation>C&amp;ессия</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="56"/>
        <source>&amp;Load</source>
        <translation>Загрузить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="61"/>
        <source>&amp;Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="66"/>
        <source>&amp;Quit</source>
        <translation>Выйти</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="71"/>
        <source>&amp;Recognize</source>
        <translation>Распознавание</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="76"/>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="81"/>
        <source>Analyze</source>
        <translation>Анализ</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="125"/>
        <source>Load</source>
        <translation>Загрузить</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Закрыть</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="135"/>
        <source>Unload</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="139"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="143"/>
        <source>Enable multi select</source>
        <translation>Мульти выбор</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="149"/>
        <source>Show bounding rect</source>
        <translation>Показать ограничивающий прямоугольник</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="152"/>
        <source>Show center</source>
        <translation>Показать центр масс (СТП)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="155"/>
        <source>Show max distance</source>
        <translation>Показать максимальное расстояние</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="158"/>
        <source>MilRad circles</source>
        <translation>тысячные доли (милрад)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="161"/>
        <source>MOA circles</source>
        <translation>Угловые минуты (МОА)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="164"/>
        <source>no circles</source>
        <translation>не показывать</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="201"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="209"/>
        <source>Save image</source>
        <translation>Сохранить изображение</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="210"/>
        <source>PNG Image (*.png)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="216"/>
        <source>Show problematic zones</source>
        <translation>Показать ошибочные зоны</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="275"/>
        <source>Всё хорошо</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="276"/>
        <source>Heeling (anticipating recoil)</source>
        <translation>Крен (ожидание отдачи)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="277"/>
        <source>Breaking wrist up</source>
        <translation>Излом запястья вверх</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="278"/>
        <source>Pushing (anticipating recoil) or No follow through</source>
        <translation>Сдавливание в ожидании отдачи или неконтроллируемое выжимание спуска</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="279"/>
        <source>Too little trigger finger</source>
        <translation>Указательный палец слишком мало просунут в скобу</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="280"/>
        <source>Tightening fingers</source>
        <translation>Черезмерное сдавливание пальцами</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="281"/>
        <source>Jerking or Slapping trigger</source>
        <translation>Рывок спуска</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="282"/>
        <source>Breaking wrist down, Pushing forward or Drooping head</source>
        <translation>Излом запястья вниз, давление впрерёд или \&quot;нырок\&quot; головой</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="283"/>
        <source>Tightening grip while pulling trigger</source>
        <translation>Черезмерное сдавливание рукоятки в момент спуска</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="284"/>
        <source>Thumbing (squeezing thumb) or Too much trigger finger</source>
        <translation>ерезмерное сдавливание большим пальцем, или указательный палец далеко просунут в спусковую скобу</translation>
    </message>
    <message>
        <source>Serie &apos;%1&apos;</source>
        <translation type="vanished">Серия &quot;%1&quot;</translation>
    </message>
    <message>
        <source>  Serie label: %1</source>
        <translation type="vanished">Имя серии</translation>
    </message>
    <message>
        <source>     distance: %1</source>
        <translation type="obsolete">Рассто</translation>
    </message>
    <message>
        <source> Shots number: %1</source>
        <translation type="vanished">Количество выстрелов</translation>
    </message>
    <message>
        <source>        Width: %1</source>
        <translation type="vanished">Ширина</translation>
    </message>
    <message>
        <source>       height: %1</source>
        <translation type="vanished">Высота</translation>
    </message>
    <message>
        <source> max distance: %1</source>
        <translation type="vanished">максимальное расстояние</translation>
    </message>
    <message>
        <source>  mass center: %1, %2</source>
        <translation type="vanished">координаты СТП</translation>
    </message>
    <message>
        <source> point of impact shift: %1</source>
        <translation type="vanished">Смещение СТП</translation>
    </message>
    <message>
        <source>standard derivation: %1</source>
        <translation type="vanished">стандартное отклонение</translation>
    </message>
</context>
<context>
    <name>RecognizeForm</name>
    <message>
        <location filename="recognizeform.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="recognizeform.ui" line="54"/>
        <source>Done</source>
        <translation>Готово</translation>
    </message>
</context>
<context>
    <name>SerieFilterWidget</name>
    <message>
        <location filename="seriefilterwidget.ui" line="14"/>
        <location filename="seriefilterwidget.ui" line="17"/>
        <source>Serie filter</source>
        <translation>Фильтр</translation>
    </message>
    <message>
        <location filename="seriefilterwidget.ui" line="41"/>
        <source>Start date</source>
        <translation>дата начала</translation>
    </message>
    <message>
        <location filename="seriefilterwidget.ui" line="55"/>
        <source>End date</source>
        <translation>дата конца</translation>
    </message>
    <message>
        <location filename="seriefilterwidget.ui" line="69"/>
        <source>Caliber</source>
        <translation>калибр</translation>
    </message>
    <message>
        <location filename="seriefilterwidget.ui" line="77"/>
        <location filename="seriefilterwidget.ui" line="103"/>
        <location filename="seriefilterwidget.ui" line="124"/>
        <location filename="seriefilterwidget.ui" line="160"/>
        <source>any</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="seriefilterwidget.ui" line="82"/>
        <source>.223</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="seriefilterwidget.ui" line="87"/>
        <source>5.56x45</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="seriefilterwidget.ui" line="95"/>
        <source>Target</source>
        <translation>Мишень</translation>
    </message>
    <message>
        <location filename="seriefilterwidget.ui" line="108"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="seriefilterwidget.ui" line="116"/>
        <source>Position</source>
        <translation>Позиция</translation>
    </message>
    <message>
        <location filename="seriefilterwidget.ui" line="129"/>
        <source>staying</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="seriefilterwidget.ui" line="134"/>
        <source>sitting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="seriefilterwidget.ui" line="139"/>
        <source>playing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="seriefilterwidget.ui" line="144"/>
        <source>bench</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="seriefilterwidget.ui" line="152"/>
        <source>Scope</source>
        <translation>Прицел</translation>
    </message>
    <message>
        <location filename="seriefilterwidget.ui" line="165"/>
        <source>none</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="seriefilterwidget.ui" line="170"/>
        <source>iron sight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="seriefilterwidget.ui" line="175"/>
        <source>red dot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="seriefilterwidget.ui" line="180"/>
        <source>optic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="seriefilterwidget.ui" line="185"/>
        <source>laser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="seriefilterwidget.ui" line="193"/>
        <source>filter match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="seriefilterwidget.ui" line="200"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

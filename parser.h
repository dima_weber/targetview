#ifndef PARSER_H
#define PARSER_H

#include "session.h"

#include <QtCore/QVector>
#include <memory>


class QFile;

class SessionReader
{
public:
    SessionReader(Sessions& sessions):_sessions(sessions){}
    bool read(const QString& filePath);
protected:
    Sessions& _sessions;
    virtual bool parse(QFile&) =0;
};

#endif

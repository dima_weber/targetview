#ifndef HOLE_H
#define HOLE_H

#include "types.h"
#include "xmlhelper.h"

class Hole : public PolarCoordinate
{
public:
	Hole(const QPointF& point);
	Hole();
};

bool readXmlTag(const QDomElement& holeElement, Hole& hole, const ReadXmlContext& context, bool optional = true);
bool writeXmlTag(QDomDocument &doc, QDomElement& holeElement, const Hole& hole);


#endif // HOLE_H

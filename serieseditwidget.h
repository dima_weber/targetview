#ifndef SERIESEDITWIDGET_H
#define SERIESEDITWIDGET_H

#include "serie.h"

#include <QWidget>
class QComboBox;
class SerieEditWidget;
class SerieCollection;

class SeriesEditWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SeriesEditWidget(SerieCollection& collection, QWidget *parent = 0);
    ~SeriesEditWidget();

    void setSeries(Series* series);

private:
    SerieCollection& collection;
    Series* pSeries;
    QComboBox* pSerieSelect;
    SerieEditWidget* pSerieEdit;
};

#endif // SERIESEDITWIDGET_H

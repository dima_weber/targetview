#include "trajectorywidget.h"
#include "ui_trajectorywidget.h"
#include "units.h"
#include "types.h"
#include "ballistics/ballistics.h"

#include <qcustomplot/qcustomplot.h>
#include <QGraphicsScene>
#include <QStandardItemModel>

TrajectoryWidget::TrajectoryWidget(Solution::Ptr solution, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TrajectoryWidget),
    solution(solution)
{
    pDropScene = new QGraphicsScene(this);
    pRangeModel = new QStandardItemModel(this);

    ui->setupUi(this);

    ui->dropView->setScene(pDropScene);
    ui->rangeView->setModel(pRangeModel);

    ui->plotView->addGraph(ui->plotView->xAxis, ui->plotView->yAxis);
    ui->plotView->graph(0)->setAntialiased(true);
    pSightLine = new QCPCurve(ui->plotView->xAxis, ui->plotView->yAxis);
    pSightLine->setPen(QPen(QColor("green")));
    ui->plotView->graph(0)->setPen(QPen(QColor("blue"), 2));
    ui->plotView->addPlottable(pSightLine);
    ui->plotView->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);

}

TrajectoryWidget::~TrajectoryWidget()
{
    delete ui;
}

void TrajectoryWidget::recalculate(const BulletParams& bulletParams)
{
    solution->build(bulletParams, solution->atmosphere());
    populateView();
}

void TrajectoryWidget::recalculate(const AtmosphereParams& atmosphereParams)
{
    solution->build(solution->bullet(), atmosphereParams);
    populateView();
}

void TrajectoryWidget::init(const BulletParams& bulletParams, const AtmosphereParams& atmosphereParams, const TargetParams& targetParams)
{
    solution->build(bulletParams, atmosphereParams);
    populateView(targetParams);
}

void TrajectoryWidget::onPrint()
{
    QPrinter printer;
    QPrintDialog dialog(&printer, this);
    dialog.exec();

    const int rows = ui->rangeView->model()->rowCount();
    const int columns = ui->rangeView->model()->columnCount();
    int totalWidth = ui->rangeView->verticalHeader()->width();
    for (int c = 0; c < columns; ++c)
        totalWidth += ui->rangeView->columnWidth(c);
    int totalHeight = ui->rangeView->horizontalHeader()->height();
    for (int r = 0; r < rows; ++r)
        totalHeight += ui->rangeView->rowHeight(r);
    QTableView tempTable;
    tempTable.setAttribute(Qt::WA_DontShowOnScreen);
    tempTable.setModel(ui->rangeView->model());
    tempTable.setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    tempTable.resizeColumnsToContents();
    tempTable.resizeRowsToContents();
    tempTable.setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    tempTable.setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    QPixmap pixmap = QPixmap::grabWidget(&tempTable);
    QPainter painter;
    painter.begin(&printer);//p is my QPrinter
    printer.paintEngine()->drawPixmap(QRectF(0, 0, pixmap.width(), pixmap.height()), pixmap, QRectF(0, 0,pixmap.width(), pixmap.height()));
    painter.end();
}

void TrajectoryWidget::populateView()
{
    QList<QStandardItem*> items;

    pRangeModel->clear();
    pDropScene->clear();
    QCPGraph* pGraph = ui->plotView->graph(0);
    pGraph->clearData();

    QPainterPath dropPath;
    QPainterPath windagePath;

    QPointF dropPoint = QPointF(0, solution->bullet().sightHeight.value(Length::Unit::cm));
    QPointF windagePoint = QPointF(0, 0);

    pGraph->addData(dropPoint.x(), -dropPoint.y());
    dropPath.moveTo(dropPoint);
    windagePath.moveTo(windagePoint);

    QPen dropPen(QColor("blue"), 2);

    double minDrop=0;
    double maxDrop=0;

    Length rangeBegin = target.rangeBegin;
    Length rangeEnd = qMin(target.rangeEnd, solution->max_range());

    pRangeModel->setHorizontalHeaderLabels(QStringList() << "range\n(m)" << "flight\ntime (ms)" << "velocity\n(m/s)" << "drop\n(cm)" << "correction\n(moa)" << "windage\n(cm)" << "windage\ncorrection\n(moa)");

    Angle alpha = solution->bullet().shootingAngle;
    for (Length range=rangeBegin; range <= rangeEnd; range += target.rangeStep)
    {
        items.clear();

        double distance = range.value(Length::Unit::m);
        double drop = solution->drop(range).value(Length::Unit::cm);
        double windage = solution->windage(range).value(Length::Unit::cm);


        double x = solution->x(range).value(Length::Unit::m);
        double y = solution->y(range).value(Length::Unit::cm);

        minDrop = qMin(minDrop, -y);
        maxDrop = qMax(maxDrop, -y);

        QStandardItem* nameItem = new QStandardItem(QString::number(distance, 'f', 1));
        QStandardItem* timeItem = new QStandardItem(QString::number(solution->time(range).value(Time::Unit::ms)));
        QStandardItem* velocityItem = new QStandardItem(QString::number(solution->velocity(range).value(Length::Unit::m, Time::Unit::s), 'f', 1));
        QStandardItem* dropItem = new QStandardItem(QString::number(drop, 'f', 1));
        QStandardItem* correctionItem = new QStandardItem(QString::number(solution->vertCorr(range).value(Angle::Unit::moa), 'f', 1));
        QStandardItem* windageItem = new QStandardItem(QString::number(windage, 'f', 1));
        QStandardItem* windageCorrectionItem = new QStandardItem(QString::number(solution->horCorr(range).value(Angle::Unit::moa), 'f', 1));

        if (qAbs(drop) < target.targetHeight.value(Length::Unit::cm) / 2)
            dropItem->setData(QColor("light blue"), Qt::BackgroundColorRole);
        else
            dropItem->setData(QColor("light pink"), Qt::BackgroundColorRole);

        if (qAbs(windage) < target.targetWidth.value(Length::Unit::cm) / 2)
            windageItem->setData(QColor("light blue"), Qt::BackgroundColorRole);
        else
            windageItem->setData(QColor("light pink"), Qt::BackgroundColorRole);

        items << nameItem << timeItem << velocityItem << dropItem << correctionItem << windageItem << windageCorrectionItem;
        pRangeModel->appendRow(items);

        dropPoint = QPointF(x, y);
        dropPath.lineTo(  dropPoint );

        pGraph->addData(x, -y);

        windagePath.lineTo( QPointF(x, windage));
    }

//		pPlot->xAxis->setRange(0, rangeEnd);
//		pPlot->yAxis->setRange(minDrop, maxDrop);
    ui->plotView->rescaleAxes(true);

    int shootingHeight = 180;

    QPen penSmall(QColor("black"),1);
    QPen penLarge(QColor("black"),2);
    QPen gridPen(QColor("grey"), 1, Qt::DashLine);

    int minDropScale = (qRound(minDrop) / 10 - 1) * 10;
    int maxDropScale = (qRound(maxDrop) / 10 + 1) * 10;

    pDropScene->addLine(0, shootingHeight,rangeEnd.value(Length::Unit::m), shootingHeight, penLarge);
    pDropScene->addLine(0, -minDropScale, 0, -maxDropScale, penLarge);

    for (int i=minDropScale; i<=maxDropScale; i+=10)
    {
        bool is30 = i % 30 == 0;
        pDropScene->addLine(-3,-i,0,-i, is30?penLarge:penSmall);
        if (is30)
        {
            QGraphicsSimpleTextItem* pText = pDropScene->addSimpleText(QString::number(i));
            pText->moveBy(-5 - pText->boundingRect().width(), -i - pText->boundingRect().height()/2);

            pDropScene->addLine(0,-i, rangeEnd.value(Length::Unit::m), -i, gridPen);
        }
    }
    for (int i=10; i<=rangeEnd.value(Length::Unit::m); i+=10)
    {
        bool is50 = i % 50 == 0;
        int h = is50?5:3;

        pDropScene->addLine(i, 180, i, 180-h, is50?penLarge:penSmall);
        if (is50)
        {
            QGraphicsSimpleTextItem* pText = pDropScene->addSimpleText(QString::number(i));
            pText->moveBy(i - pText->boundingRect().height()/2, shootingHeight + 2);

            pDropScene->addLine(i,-minDropScale, i, -maxDropScale, gridPen);
        }
    }
    pDropScene->addLine(0, 0, rangeEnd.value(Length::Unit::m)*alpha.cos(),  rangeEnd.value(Length::Unit::m) * alpha.sin(), QPen(QColor("green"),2));
    QVector<double> sightX, sightY;
    sightX << 0 << rangeEnd.value(Length::Unit::m) * alpha.cos();
    sightY << 0 << -rangeEnd.value(Length::Unit::m) * alpha.sin();
    pSightLine->setData(sightX, sightY);

    pDropScene->addPath(dropPath, dropPen);

    ui->rangeView->resizeRowsToContents();
    ui->rangeView->resizeColumnsToContents();

    ui->plotView->replot();
}

void TrajectoryWidget::populateView(const TargetParams& targetParams)
{
    target = targetParams;
    populateView();
}

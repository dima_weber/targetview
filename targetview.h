#ifndef TARGETVIEW_H
#define TARGETVIEW_H

#include <QtWidgets/QGraphicsView>

class QGraphicsScene;
class QWheelEvent;
class QMouseEvent;

class TargetView : public QGraphicsView
{
	Q_OBJECT
	double _currentScale;
public:
	TargetView(QWidget* parent = 0);
	TargetView(QGraphicsScene* pScene, QWidget* parent =0 );

	void originalScale();
protected:
	virtual void wheelEvent(QWheelEvent* pEvent);
	virtual void mousePressEvent(QMouseEvent* mouseEvent);
	virtual void mouseReleaseEvent(QMouseEvent* mouseEvent);

signals:

public slots:
};

#endif // TARGETVIEW_H

#ifndef OPACITYWIDGET_H
#define OPACITYWIDGET_H

#include <QPushButton>
#include <QEvent>
#include <QGraphicsOpacityEffect>

class OpacityWidget : public QPushButton
{
    Q_OBJECT
    QGraphicsOpacityEffect* pEffect;
    double minOpacity;
    double maxOpacity;
public:
    OpacityWidget(double min, double max, const QString& text, QWidget* parent = nullptr)
        :QPushButton(text, parent), minOpacity(min), maxOpacity(max)
    {
        pEffect = new QGraphicsOpacityEffect(this);
        pEffect->setOpacity(min);
        setGraphicsEffect(pEffect);
    }

    void enterEvent(QEvent* event) override
    {
        QPushButton::enterEvent(event);
        pEffect->setOpacity(maxOpacity);
    }

    void leaveEvent(QEvent* event) override
    {
        QPushButton::leaveEvent(event);
        pEffect->setOpacity(minOpacity);
    }
};

#endif // OPACITYWIDGET_H

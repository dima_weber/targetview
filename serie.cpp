#include "serie.h"
#include "session.h"
#include "xmlhelper.h"
#include "targetbuilder.h"

#include <QtGui/QColor>
#include <QtGui/QBrush>
#include <QtGui/QPen>

#include <QtWidgets/QGraphicsItemGroup>
#include <QtWidgets/QPushButton>

#include <QtXml/QDomElement>

#include <gsl/gsl_statistics.h>

#include <functional>

Serie::Serie(SessionPtr session)
    : number(0),
        position(Serie::Position::Unknown), scope(Serie::Scope::Unknown), firestyle(Serie::Firestyle::Unknown),
        range(0), _shape(Serie::HoleShape::Hexagon)
{
    setSession(session);
    invalidateCache();
    _uuid = QUuid::createUuid();
}

Serie::~Serie()
{
}

QColor Serie::getColor() const
{
    QColor color;
    if (_color.isValid())
        color = _color;
    else
    {
        color = getSerieColor(number);
        if (color.alpha() == 255)
            color.setAlpha(200);
    }
    return color;
}

void Serie::setColor(QColor color)
{
    _color = color;
}

Serie::HoleShape Serie::getShape() const
{
    return _shape;
}

void Serie::setHoleShape(Serie::HoleShape shape)
{
    _shape = shape;
}

QString Serie::name() const
{
    return _name;
}

void Serie::setName(const QString& name)
{
    _name = name;
}

void Serie::addHole(const HolesItem& hole)
{
    _holes.mutexAppend(hole);

    invalidateCache();
}

int Serie::holesCount() const
{
    return holes().count();
}


QString Serie::positionToString(Position position)
{
    switch(position)
    {
        case Position::Standing: return "standing";
        case Position::Sitting: return "sitting";
        case Position::Laying: return "laying";
        case Position::Bench: return "bench";
        default: return "unknown";
    }
}

QString Serie::scopeToString(Serie::Scope scope)
{
    switch(scope)
    {
        case Scope::IronSight: return "iron sight";
        case Scope::Laser: return "laser";
        case Scope::Optic: return "optic";
        case Scope::RedDot: return "red dot";
        case Scope::None: return "none";
        case Scope::Unknown:
        default: return "unknown";
    }
}

QString Serie::firestyleToString(Serie::Firestyle style)
{
    switch(style)
    {
        case Serie::Firestyle::Slow: return "slow";
        case Serie::Firestyle::Rapid: return "rapid";
        case Serie::Firestyle::TargetSwitch: return "target switch";
        case Serie::Firestyle::Unknown:
        default:
            return "unknown";
    }
}

QString Serie::shapeToString(Serie::HoleShape shape)
{
    switch(shape)
    {
    case HoleShape::Cirle: return "cirlce";
    case HoleShape::Hexagon: return "hexagon";
    case HoleShape::Square: return "square";
    case HoleShape::Diamond: return "diamond";
    case HoleShape::Pentagon: return "pentagon";
    case HoleShape::Triangle: return "triangle";
    case HoleShape::CrossCirle: return "crosscircle";
    case HoleShape::PlusCirlce: return "pluscircle";
    case HoleShape::Unknown:
    default:
        return "unknown";
    }
}

Serie::Position Serie::stringToPosition(const QString& str)
{
    if (str == "standing")
        return Serie::Position::Standing;
    else if (str == "sitting")
        return Serie::Position::Sitting;
    else if (str == "laying")
        return Serie::Position::Laying;
    else if (str == "bench")
        return Serie::Position::Bench;
    else
        return Serie::Position::Unknown;
}

Serie::Scope Serie::stringToScope(const QString& str)
{
    if (str == "iron sight")
        return Serie::Scope::IronSight;
    else if (str == "laser")
        return Serie::Scope::Laser;
    else if (str == "optic")
        return Serie::Scope::Optic;
    else if (str == "red dot")
        return Serie::Scope::RedDot;
    else if (str == "none")
        return Serie::Scope::None;
    else
        return Serie::Scope::Unknown;
}

Serie::Firestyle Serie::stringToFirestyle(const QString& str)
{
    if (str == "slow")
        return Serie::Firestyle::Slow;
    else if (str == "rapid")
        return Serie::Firestyle::Rapid;
    else if (str == "target switch")
        return Serie::Firestyle::TargetSwitch;
    else
        return Serie::Firestyle::Unknown;
}

Serie::HoleShape Serie::stringToShape(const QString& str)
{
    if (str == "cirle")
        return HoleShape::Cirle;
    else if (str.compare("hexagon", Qt::CaseInsensitive) == 0)
        return HoleShape::Hexagon;
    else if (str.compare("square",Qt::CaseInsensitive) == 0)
        return HoleShape::Square;
    else if (str.compare("triangle", Qt::CaseInsensitive) == 0)
        return HoleShape::Triangle;
    else if (str.compare("pentagon", Qt::CaseInsensitive) == 0)
        return HoleShape::Pentagon;
    else if (str.compare("crosscircle", Qt::CaseInsensitive) == 0)
        return HoleShape::CrossCirle;
    else if (str.compare("pluscircle", Qt::CaseInsensitive) == 0)
        return HoleShape::PlusCirlce;
    else if (str.compare("diamond", Qt::CaseInsensitive) == 0)
        return HoleShape::Diamond;
    else
        return HoleShape::Unknown;
}

QString Serie::info()
{
    QString msg;
    msg += QString("        label: %1\n").arg(name());
    msg += QString("        range: %1 m\n")
            .arg(QString::number(range.value(Length::Unit::m), 'f', 0));
    msg += QString(" shots number: %1\n").arg(holesCount());
    msg += QString(" width x height: %1 x %2 mm\n")
            .arg(QString::number(boundingRect().width(), 'f', 1))
            .arg(QString::number(boundingRect().height(), 'f', 1));
    msg += QString(" max distance: %1 mm\n")
            .arg(QString::number(maxDistance(), 'f', 1));
    msg += QString("  mass center: (%1, %2)\n")
            .arg(QString::number(massCenter().x(), 'f', 1))
            .arg(QString::number(massCenter().y(), 'f', 1));
    msg += QString(" point of impact shift: %1 mm\n")
            .arg(QString::number(massCenter().radius(), 'f', 1));
    msg += QString("standard derivation: %1\n")
            .arg(QString::number(sd().value(Length::Unit::mm), 'f',1));
    msg += QString("  total value: %1").arg(scoreTotal());
//    msg += QString("       values:") << values;

    return msg;
}

Holes Serie::holes() const
{
    return _holes;
}

void Serie::setHoles(const Holes& holes)
{
    _holes.clear();
    for (const HolesItem& hole: holes)
        addHole(hole);
}

QDate Serie::date() const
{
    QDate ret;
    SessionPtr s = wpSession.lock();
    if (s)
        ret = s->date();
    return ret;
}

Caliber Serie::caliber() const
{
    Caliber ret(0);
    SessionPtr  s = wpSession.lock();
    if (s)
        ret = s->caliber();
    return ret;
}

QString defaultTarget = "unknown";
const QString& Serie::targetName() const
{
    SessionPtr s = wpSession.lock();
    if (s)
        return s->targetName();
    else
        return defaultTarget;
}

QString defaultShooter = "unknown";
const QString& Serie::shooter() const
{
    SessionPtr s = wpSession.lock();
    Q_ASSERT(s);
    if (s)
        return s->shooter();
    else
        return ::defaultShooter;
}

void Serie::setSession(SessionPtr p)
{
    wpSession = p;
}

PolarCoordinate Serie::massCenter() const
{
    if (!_massCenter)
        _massCenter.reset(new QPointF(calculateMassCenter()));
    return *_massCenter;
}

Length Serie::sd() const
{
    if (!_sd)
        _sd.reset(new Length(calculateSd()));

    return *_sd;
}

double Serie::maxDistance() const
{
    return dist(mostDistantPoints().first, mostDistantPoints().second);
}


std::pair<QPointF, QPointF> Serie::mostDistantPoints() const
{
    if (!_mostDistantPoints)
        _mostDistantPoints.reset(new std::pair<QPointF, QPointF>(calculateMostDistantPoints()));
    return *_mostDistantPoints;
}

double Serie::maxRadius() const
{
    if (!_maxRadius)
    {
        _maxRadius.reset(new double(calculateMaxRadius()));
    }
    return *_maxRadius;
}

QRectF Serie::boundingRect() const
{
    if (!_boundingRect)
        _boundingRect.reset(new QRectF(calculateBoundingRect()));
    return *_boundingRect;
}

int Serie::holeValue(const QPointF& coord) const
{
    return target()->getValue(coord);
}

int Serie::valueCount(int v) const
{
    if (!_values)
        _values.reset(new QMap<int, int>(calculateValues()));
    if (!_values->contains(v))
        return 0;
    return _values->value(v);
}

const QMap<int, int>& Serie::values() const
{
    if (!_values)
        _values.reset(new QMap<int, int>(calculateValues()));
    return *_values;
}

void Serie::invalidateCache()
{
    _massCenter.reset();
    _sd.reset();
    _mostDistantPoints.reset();
    _maxRadius.reset();
}

TargetPtr Serie::target() const
{
    SessionPtr s = wpSession.lock();
    if (s)
        return s->target();
    else
        return nullptr;
}

typedef double (GSL_STAT_FUNC)(const double [], const size_t, const size_t);
typedef double (GSL_WSTAT_FUNC)(const double [], const size_t, const double[], const size_t, const size_t);
typedef void (GSL_STAT_FUNC2)(double*, double*, const double[], const size_t, const size_t);

template<class T, typename F>
double gsl_wrapper(const QVector<T>& vector, F accessor, GSL_STAT_FUNC gsl_func )
{
    int len = vector.size();
    QScopedArrayPointer<double> array(new double[len]);
    for(int i=0; i<len; i++)
        array[i] = accessor(vector[i]);
    return gsl_func(array.data(), 1, len);
}

template<class T, typename F>
void gsl_wrapper(const QVector<T>& vector, F accessor, GSL_STAT_FUNC2 gsl_func2, double& r1, double& r2 )
{
    int len = vector.size();
    QScopedArrayPointer<double> array(new double[len]);
    for(int i=0; i<len; i++)
        array[i] = accessor(vector[i]);
    gsl_func2(&r1, &r2, array.data(), 1, len);
}

template<class V, class W>
double gsl_w_wrapper(const W& weight, const V& vector, GSL_WSTAT_FUNC gsl_w_func)
{
    int idx=0;
    Q_ASSERT(vector.size() == weight.size());
    int len = vector.size();
    QScopedArrayPointer<double> v(new double[len]);
    QScopedArrayPointer<double> w(new double[len]);
    idx=0;
    for(auto it=vector.begin(); it != vector.end(); it++)
        v[idx++] = *it;
    idx=0;
    for(auto it=weight.begin(); it != weight.end(); it++)
        w[idx++] = *it;
    return gsl_w_func(w.data(), 1, v.data(), 1, len);
}

int Serie::scoreTotal()
{
    int ret = 0;
    for(int v: values().keys())
        ret += v * valueCount(v);
    return ret;
}

double Serie::scoreAvg()
{
    return gsl_w_wrapper(values().values(), values().keys(), gsl_stats_wmean);
}

double Serie::scoreSd()
{
    return gsl_w_wrapper(values().values(), values().keys(), gsl_stats_wsd);
}

PolarCoordinate Serie::calculateMassCenter() const
{
    using namespace std::placeholders;

    double mean_x = gsl_wrapper(holes(), std::bind(&PolarCoordinate::x, _1), gsl_stats_mean);
    double mean_y = gsl_wrapper(holes(), std::bind(&PolarCoordinate::y, _1), gsl_stats_mean);

    return PolarCoordinate(QPointF(mean_x, mean_y));
}

Length Serie::calculateSd() const
{
    using namespace std::placeholders;

    double dispersion = 0;
    dispersion = gsl_wrapper(holes(), std::bind(&PolarCoordinate::distanceTo, _1, massCenter()), gsl_stats_sd);
    return Length(dispersion);
}

std::pair<QPointF, QPointF> Serie::calculateMostDistantPoints() const
{
    std::pair<QPointF, QPointF>	ret;
    Holes vec = holes();
    double mx=0;
    int count = vec.size();
    for (int i=1; i<count; i++)
    {
        const QPointF& a = vec[i]->coord();
        for (int j=0; j<i; j++)
        {
            const QPointF& b = vec[j]->coord();
            double r = dist(a, b);
            if (r > mx)
            {
                mx = r;
                ret.first = a;
                ret.second = b;
            }
        }
    }
    return ret;
}

double Serie::calculateMaxRadius() const
{
    using namespace std::placeholders;

    double r=0;
    r = gsl_wrapper(holes(), std::bind(&PolarCoordinate::distanceTo, _1, massCenter()), gsl_stats_max);
    return r;
}

QMap<int, int> Serie::calculateValues() const
{
    QMap<int, int> ret;
    for(HolesItem& hole: holes())
    {
        QPointF coord = hole->coord();
        int value = holeValue(coord);
        ret[value]++;
    }

    return ret;
}

QRectF Serie::calculateBoundingRect() const
{
    using namespace  std::placeholders;

    QRectF rect;
    double left, right, bottom, top;
    gsl_wrapper(holes(), std::bind(&PolarCoordinate::x, _1), gsl_stats_minmax, left, right);
    gsl_wrapper(holes(), std::bind(&PolarCoordinate::y, _1), gsl_stats_minmax, top, bottom);
    rect.setLeft(left);
    rect.setRight(right);
    rect.setTop(top);
    rect.setBottom(bottom);
    return rect;
}

bool readXmlTag(const QDomElement& serieElement, Serie& serie, const ReadXmlContext& context, bool optional)
{
    bool ok;
    QString strPosition;
    QString strScope;
    QString strShape;
    QString strFirestyle;
    QColor color;
    QString name;
    ProcessFunction<HolesItem> func = std::bind(&Serie::addHole, &serie, std::placeholders::_1);
    ok =     readChildTag(serieElement, "range", serie.range, context, false)
          && readChildTag(serieElement, "name", name, context, true)
          && readChildTag(serieElement, "comment", serie.comment, context, true)
          && readChildTag(serieElement, "position", strPosition, context, true)
          && readChildTag(serieElement, "scope", strScope, context, true)
          && readChildTag(serieElement, "firestyle", strFirestyle, context, true)
          && readChildTag(serieElement, "shape", strShape, context, true)
          && readChildTag(serieElement, "color", color, context, false)
          && readChildArray(serieElement, "holes", "hole", func, context, false)
             ;

    if (ok || optional)
    {
        serie.setColor(color);
        serie.setName(name);
        serie.position = Serie::stringToPosition(strPosition);
        serie.scope = Serie::stringToScope(strScope);
        serie.firestyle = Serie::stringToFirestyle(strFirestyle);
        serie.setHoleShape(Serie::stringToShape(strShape));

        serie.setUuid( serieElement.attribute("uuid"));
        serie.number = serieElement.attribute("number").toInt();
    }

    return ok || optional;
}

bool writeXmlTag(QDomDocument& doc, QDomElement& serieElement, const Serie& serie)
{
    serieElement.setAttribute("uuid", serie.uuid());
    serieElement.setAttribute("number", serie.number);

    QString position = Serie::positionToString(serie.position);
    QString scope = Serie::scopeToString(serie.scope);
    QString firestyle = Serie::firestyleToString(serie.firestyle);
    QString shape = Serie::shapeToString(serie.getShape());
    bool ok =
           writeChildTag(doc, serieElement, "position", position)
        && writeChildTag(doc, serieElement, "scope", scope)
        && writeChildTag(doc, serieElement, "firestyle", firestyle)
        && writeChildTag(doc, serieElement, "shape", shape)
        && writeChildTag(doc, serieElement, "range", serie.range)
        && writeChildTag(doc, serieElement, "name", serie.name())
        && writeChildTag(doc, serieElement, "comment", serie.comment)
        && writeChildArray(doc, serieElement, "holes", "hole", serie.holes())
        && writeChildTag(doc, serieElement, "color", serie.getColor())
           ;

    return ok;
}

#include "sessioneditwidget.h"
#include "seriecollection.h"
#include "serieseditwidget.h"
#include "session.h"

#include <QtWidgets/QFormLayout>
#include <QtWidgets/QBoxLayout>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLineEdit>

SessionEditWidget::SessionEditWidget(SerieCollection& collection, QWidget *parent) :
	QWidget(parent),
	collection(collection),
	pSession(nullptr)
{
	QBoxLayout* pTopLayout = new QVBoxLayout(this);
	QFormLayout* pLayout = new QFormLayout();


	pDateEdit = new QDateEdit(this);
	pDateEdit->setCalendarPopup(true);

	QHBoxLayout* pCaliberLayout = new QHBoxLayout();
	QPushButton* pAddCaliberButton = new QPushButton("+", this);

	pCaliberSelect = new QComboBox(this);
	pCaliberSelect->setEditable(true);
	pCaliberSelect->addItems(collection.availableValues(SerieCollection::FilterType::Caliber));
	pCaliberLayout->addWidget(pCaliberSelect);
	pCaliberLayout->addWidget(pAddCaliberButton);

	connect (pAddCaliberButton, &QPushButton::clicked, [&]()
	{
		QDialog dialog(this);
		QFormLayout* pLayout = new QFormLayout(&dialog);
		QLineEdit* pName = new QLineEdit(&dialog);
		QLineEdit* pValue = new QLineEdit(&dialog);
		QComboBox* pUnit = new QComboBox(&dialog);
		pUnit->addItem("mm", (int)Length::Unit::mm);
		pUnit->addItem("inch", (int)Length::Unit::inch);
		QPushButton* pOk = new QPushButton(tr("ok"), &dialog);
		QPushButton* pCancel = new QPushButton(tr("cancel"), &dialog);
		pLayout->addRow(tr("name"), pName);
		pLayout->addRow(tr("value"), pValue);
		pLayout->addRow(tr("unit"), pUnit);
		pLayout->addRow(pOk, pCancel);
		connect (pOk, SIGNAL(clicked(bool)), &dialog, SLOT(accept()));
		connect (pCancel, SIGNAL(clicked(bool)), &dialog, SLOT(reject()));
		if (dialog.exec() == QDialog::Accepted)
		{
			Caliber cal (pName->text(), pValue->text().toDouble(), static_cast<Length::Unit>(pUnit->currentData().toInt()));
			pCaliberSelect->addItem(pName->text());
			pCaliberSelect->setCurrentText(pName->text());
		}
	});

	pTargetSelect = new QComboBox(this);
	pTargetSelect->addItems(collection.availableValues(SerieCollection::FilterType::Target));

	pShooterSelect = new QComboBox(this);
	pShooterSelect->addItems(collection.availableValues(SerieCollection::FilterType::Shooter));
	pShooterSelect->setEditable(true);

	pLayout->addRow(tr("Date"), pDateEdit);
	pLayout->addRow(SerieCollection::filterTypeToString(SerieCollection::FilterType::Caliber), pCaliberLayout);
	pLayout->addRow(SerieCollection::filterTypeToString(SerieCollection::FilterType::Target), pTargetSelect);
	pLayout->addRow(SerieCollection::filterTypeToString(SerieCollection::FilterType::Shooter), pShooterSelect);


	connect (pTargetSelect, &QComboBox::currentTextChanged, [&](const QString& s)
	{
		if (pSession)
			pSession->setTargetName(s);
	});

	connect (pShooterSelect, &QComboBox::currentTextChanged, [&](const QString& s)
	{
		if (pSession)
			pSession->setShooter(s);
	});

	connect (pCaliberSelect, &QComboBox::currentTextChanged, [&](const QString& cal)
	{
		if (pSession)
			pSession->setCaliber(cal);
	});

	connect (pDateEdit, &QDateEdit::dateChanged, [&](QDate date)
	{
		if (pSession)
			pSession->setDate(date);
	});

	pSeriesEdit = new SeriesEditWidget(collection, this);

	pTopLayout->addLayout(pLayout);
	pTopLayout->addWidget(pSeriesEdit);
}

SessionEditWidget::~SessionEditWidget()
{
}

void SessionEditWidget::setSession(SessionPtr pSession)
{
	this->pSession = pSession;
	if (pSession)
	{
		pDateEdit->setDate(pSession->date());
		pCaliberSelect->setCurrentText(pSession->caliber().name());
		pTargetSelect->setCurrentText(pSession->targetName());
		pShooterSelect->setCurrentText(pSession->shooter());

		pSeriesEdit->setSeries(&pSession->series);
	}
}

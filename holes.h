#ifndef HOLES_H
#define HOLES_H

#include "types.h"
#include "xmlhelper.h"

#include <QVector>

class QDomElement;

class Hole: public XmlSerializable
{
    PolarCoordinate& hole;
public:
    Hole(PolarCoordinate& a)
        : XmlSerializable("hole"),
          hole(a)
    {}
    virtual bool readXml(const QDomElement& element);
    virtual bool writeXml(QDomElement& element);
};


#endif // HOLES_H

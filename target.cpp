#include "target.h"
#include "xmlhelper.h"
#include "types.h"

#include <QtWidgets/QGraphicsTextItem>
#include <QtWidgets/QGraphicsItemGroup>
#include <QtWidgets/QGraphicsPathItem>

#include <QtGui/QFont>
#include <QtGui/QPen>
#include <QtGui/QColor>


#include <QDomElement>

Zone::Zone()
    :value(0), pImage(new ClippedGroup())
{}

void Zone::bw()
{
    QPen pen(QColor("black"), 2);
    QBrush brush(QColor("white"));
    for (QGraphicsItem* pItem: pImage->childItems())
    {
        QAbstractGraphicsShapeItem* p = dynamic_cast<QAbstractGraphicsShapeItem*>(pItem);
        if (p)
        {
            p->setPen(pen);
            p->setBrush(brush);
        }
    }
}

//Zone::Zone(const Target& target)
//	:Zone()
//{
//	target.pImage->addToGroup(pImage.get());
//}

ClippedGroup::ClippedGroup(QGraphicsItem* parent)
    :QGraphicsItemGroup(parent), pathSet(false)
{}

ClippedGroup::~ClippedGroup()
{
}

void ClippedGroup::setShape(const QPainterPath& p)
{
    path = p;
    pathSet = true;
    setFlag(ItemClipsChildrenToShape, true);
}

QRectF ClippedGroup::boundingRect() const
{
    if (pathSet)
        return path.boundingRect();
    else
        return QGraphicsItemGroup::boundingRect();

}

QPainterPath ClippedGroup::shape() const
{
    if (pathSet)
        return path;
    else
        return QGraphicsItemGroup::shape();
}

bool readXmlTag(const QDomElement& zoneElement, Zone& zone, const ReadXmlContext& context, bool optional)
{
    bool ok = true;
    if (!zoneElement.isNull())
    {
        readChildTag(zoneElement, "value", zone.value, context);

        QDomElement curveElement = zoneElement.firstChildElement("curve");
        if (!curveElement.isNull())
        {
            double border =1;
            QColor fillColor;
            QColor borderColor;

            readChildTag(curveElement, "border", border, context);
            if (!context.color.isValid())
                readChildTag(curveElement, "color", fillColor, context);
            else
                fillColor = context.color;
            readChildTag(curveElement, "bordercolor", borderColor, context);

            QPainterPath outerPath;
            QPainterPath innerPath;
            QString type = curveElement.attribute("type");
            if (type == "ring")
            {
                QPointF point;
                Length r;
                Length R;

                if (!readXmlTag(curveElement, point, context))
                    qWarning() << "ring center point is missing";

                readChildTag(curveElement, "innerradius", r, context);
                readChildTag(curveElement, "outerradius", R, context);

                outerPath.addEllipse(point.x()-R.value(Length::Unit::mm),point.y()-R.value(Length::Unit::mm),2*R.value(Length::Unit::mm),2*R.value(Length::Unit::mm));
                innerPath.addEllipse(point.x()-r.value(Length::Unit::mm),point.y()-r.value(Length::Unit::mm),2*r.value(Length::Unit::mm),2*r.value(Length::Unit::mm));
                zone.path = outerPath.subtracted(innerPath);
            }
            else if (type =="polygon")
            {
                QPolygonF poly;
                if (readChildTag(curveElement, "points", poly, context))
                {
                    zone.path.addPolygon(poly);
                }
                else
                    qWarning() << "points are missing";
            }
            else if (type == "doubleperimiter")
            {
                QPolygonF poly;

                if (readChildTag(curveElement, "innerperimiter", poly, context))
                {
                    innerPath.addPolygon(poly);
                }
                poly.clear();
                if (readChildTag(curveElement, "outerperimiter", poly, context))
                {
                    outerPath.addPolygon(poly);
                }
                zone.path = outerPath.subtracted(innerPath);
            }
            else
            {
                ok = false;
                qWarning() << "unknown curve type";
            }

            QString clip = zoneElement.attribute("clip", "false");
            zone.is_clip = clip == "true";
            if (!zone.is_clip)
            {
                QGraphicsPathItem* pCurve = new QGraphicsPathItem(zone.pImage.get());
                pCurve->setZValue(zone.value);
                pCurve->setPath(zone.path);
                pCurve->setPen(QPen(borderColor, border));
                pCurve->setBrush(fillColor);

                //zone.pImage->setShape(zone.path);
                zone.pImage->addToGroup(pCurve);
            }
        }

        QDomElement titleElement = zoneElement.firstChildElement("title");
        if (!titleElement.isNull())
        {
            QPointF point(0,0);
            Angle rotation;
            FontSize fontSize(16, "pt");
            QColor color(0,0,0);

            readChildTag(titleElement, "caption", zone.caption, context);
            readChildTag(titleElement, "position", point, context);
            readChildTag(titleElement, "rotation", rotation, context);
            readChildTag(titleElement, "fontsize", fontSize, context);
            readChildTag(titleElement, "color", color, context);

            QString visible = titleElement.attribute("visible", "yes");
            if (visible == "yes")
            {
                QGraphicsSimpleTextItem* pCaption = new QGraphicsSimpleTextItem(zone.caption, zone.pImage.get());
                zone.pImage->addToGroup(pCaption);
                QFont font = pCaption->font();
                QPen captionPen(color);
                QBrush captionBrush(color);
                pCaption->setFont(fontSize.setFontSize(font));
                QRectF captionRect = pCaption->boundingRect();
                pCaption->setTransformOriginPoint(captionRect.width()/2, captionRect.height()/2);
                pCaption->setPen(captionPen);
                pCaption->setBrush(captionBrush);
                pCaption->setRotation(-rotation.value(Angle::Unit::deg));
                pCaption->moveBy(-captionRect.width()/2, -captionRect.height()/2);
                pCaption->moveBy(point.x(), point.y());
                pCaption->setZValue(100);
            }
        }
    }

    return ok || optional;
}

bool readXmlTag(const QDomElement& targetElement, Target& target, const ReadXmlContext& context, bool optional)
{
    bool ok = false;
    if (!targetElement.isNull())
    {
        ok = readChildTag(targetElement, "name", target.name, context);

        if (ok)
        {
            ProcessFunction<ZonesItem> func = std::bind(&Target::addZone, &target, std::placeholders::_1);
            readChildArray(targetElement, "zones", "zone", func, context);
        }

        QDomElement recognitionElement = targetElement.firstChildElement("recognition");
        ok = !recognitionElement.isNull();
        if (ok)
        {
            QDomElement recognitionPointsElement = recognitionElement.firstChildElement("points");
            QDomElement recognitionQuadsElement = recognitionElement.firstChildElement("quads");

            ok = !recognitionPointsElement.isNull();
            if (ok)
            {
                QDomNodeList pointNodes = recognitionPointsElement.elementsByTagName("point");
                for (int idx=0; ok && idx < pointNodes.size(); idx++)
                {
                    QPointF point;
                    char letter;
                    QDomElement pointElement = pointNodes.at(idx).toElement();
                    ok =    readXmlTag(pointElement, point, context)
                         && pointElement.hasAttribute("name");
                    if (ok)
                    {
                        letter = pointElement.attribute("name", "A")[0].toLatin1();
                        target.destPoints[letter] = point;
                    }
                }
            }

            ok = !recognitionQuadsElement.isNull();
            if (ok)
            {
                QDomNodeList quadNodes = recognitionQuadsElement.elementsByTagName("quad");
                for (int idx=0; ok && idx < quadNodes.size(); idx++)
                {
                    QString quad;
                    QDomElement quadElement = quadNodes.at(idx).toElement();
                    ok = readXmlTag(quadElement, quad, context);
                    if (ok)
                    {
                        ok = quad.length() == 4;
                        target.quadPoints << quad;
                    }
                }
            }
        }
    }

    return ok || optional;
}

inline bool qHash(const QPointF& a)
{
    return qHash(a.x()) + qHash(a.y());
}

inline bool operator < (const QPointF& a, const QPointF& b)
{
    return (a.x() < b.x()) || (a.x() == b.x() && a.y() < b.y());
}

int Target::getValue(const QPointF &p)
{
    if (_pointValueCache.contains(p))
        return _pointValueCache[p];

    int val = 0;
    for(const ZonesItem& zone: zones)
    {
        if (zone->value > val && zone->path.contains(p))
            val = zone->value;
    }
    _pointValueCache[p] = val;
    return val;
}

void Target::addZone(const ZonesItem& zone)
{
    if (zone->is_clip)
    {
        pImage->setShape(zone->path);
    }
    else
    {
        pImage->addToGroup(zone->pImage.get());
        zones.append(zone);
        _pointValueCache.clear();
    }
}

Target::Target()
    :pImage(new ClippedGroup())
{

}

QSizeF Target::size() const
{
    if (pImage)
        return pImage->boundingRect().size();
    else
        return QSize();
}

DefaultTarget::DefaultTarget()
{
    static QPixmap pixmap(":/panda.png");
    static QPixmap scaledPixmap =  pixmap.scaled(500, 500, Qt::KeepAspectRatio);
    pImage->addToGroup(new QGraphicsPixmapItem(scaledPixmap));
}

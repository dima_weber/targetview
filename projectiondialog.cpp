#include "projectiondialog.h"
#include "ui_projectiondialog.h"
#include "targetbuilder.h"
#include "atmosphereparamswidget.h"
#include "bulletpropertieswidget.h"
#include "targetparamswidget.h"
#include "ballistics/ballistics.h"
#include "types.h"
#include "units_converter.h"

#include <QGridLayout>
#include <QBoxLayout>
#include <QFormLayout>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QSlider>
#include <QPropertyAnimation>
#include <QGraphicsTextItem>
#include <QTableView>
#include <QGroupBox>
#include <QComboBox>
#include <QSpinBox>
#include <QSplitter>
#include <QScreen>

ProjectionDialog::ProjectionDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProjectionDialog),
    customDpi(0),
    sceneDist(3, Length::Unit::m),
    sceneWth(7.5, Length::Unit::m)
{
    ui->setupUi(this);

    pScene = new QGraphicsScene(this);
    pTargetModel = new TargetModel(this);

    QWidget* pLeftSide = new QWidget(this);
    QWidget* pRightSide =  new QWidget;
    QSplitter* pSplit = new QSplitter(this);

    QGridLayout* pViewLayout = ui->pViewLayout;
    QHBoxLayout* pLayout = new QHBoxLayout(this);
    QVBoxLayout* pControlsLayout = ui->pControlsLayout;
    QBoxLayout* pTrajectoryControlsLayout = ui->pTrajectoryControlsLayout;

    ui->pView->setScene(pScene);

    ui->pTargetsView->setModel(pTargetModel);
    ui->pTargetsView->setItemDelegateForColumn(1, new TargetSelectDelegate(this));
    ui->pTargetsView->setItemDelegateForColumn(2, new DistanceEditDelegate(1, 500, 10, "m", this));
    ui->pTargetsView->setItemDelegateForColumn(3, new DistanceEditDelegate(1, 10, 1, "m", this));
    ui->pTargetsView->setItemDelegateForColumn(4, new DistanceEditDelegate(0, 20, 1, "m/s", this));
    ui->pTargetsView->setItemDelegateForColumn(5, new ColorSelectDelegate(QColor::colorNames(), this));

    ui->pTargetsView->resizeColumnsToContents();
    ui->pTargetsView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->pTargetsView->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

    BulletParams bulletParams;
    bulletParams.bc = 0.26;
    bulletParams.muzzleVelocity.setValue(896, Length::Unit::m, Time::Unit::s);
    bulletParams.zero_range.setValue(50, Length::Unit::m);
    bulletParams.sightHeight.setValue(65, Length::Unit::mm);
    bulletParams.shootingAngle.setValue(0, Angle::Unit::deg);
    bulletParams.dragFunction = G1;

    AtmosphereParams atmosphereParams;
    atmosphereParams.windSpeed.setValue(5, Length::Unit::m, Time::Unit::s);
    atmosphereParams.windAngle.setValue(90, Angle::Unit::deg);
    atmosphereParams.altitude.setValue(120, Length::Unit::m);
    atmosphereParams.pressure.setValue(1016, Pressure::Unit::hpa); //30.09;
    atmosphereParams.temperature.setValue(6, Temperature::Unit::c);
    atmosphereParams.humidity = 0.75;

    // range
    TargetParams targetParams;
    targetParams.rangeBegin.setValue(50, Length::Unit::m);
    targetParams.rangeEnd.setValue(500, Length::Unit::m);
    targetParams.rangeStep.setValue(50,Length::Unit::m);
    targetParams.targetHeight.setValue(170, Length::Unit::cm);
    targetParams.targetWidth.setValue(60, Length::Unit::cm);

    ui->pAtmWidget->setParams(atmosphereParams);
    ui->pBulWidget->setParams(bulletParams);
    ui->pTarWidget->setParams(targetParams);

    ui->pDpiSpin->setValue(dpi());

    pLayout->setMargin(1);
    pSplit->addWidget(pLeftSide);
    pSplit->addWidget(pRightSide);
    pLeftSide->setLayout(pViewLayout);
    pRightSide->setLayout(pControlsLayout);
    pLayout->addWidget(pSplit);
    pSplit->setHandleWidth(4);
    pSplit->setCollapsible(1, true);
    pSplit->setCollapsible(0, false);

    ui->pShowTrajectory->setLayout(pTrajectoryControlsLayout);
//    pRangeControlsLayout->addStretch();

    connect(pTargetModel, SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)), this, SLOT(render()));
    connect(ui->pAtmWidget, &AtmosphereParamsWidget::changed, this, &ProjectionDialog::renderTrajectory);
    connect(ui->pBulWidget, &BulletPropertiesWidget::changed, this, &ProjectionDialog::renderTrajectory);
    connect(ui->pTarWidget, &TargetParamsWidget::changed, this, &ProjectionDialog::render);


    addTarget("idpa", Length(300, Length::Unit::m), Length(1, Length::Unit::m), Speed(1), QColor("olive"));
    addTarget("ipsc", Length(235, Length::Unit::m), Length(2, Length::Unit::m), Speed(1), QColor("olive"));
    addTarget("ipsc mini", Length(50, Length::Unit::m), Length(3, Length::Unit::m), Speed(1), QColor("olive"));
    addTarget("idpa", Length(100, Length::Unit::m), Length(4, Length::Unit::m), Speed(1), QColor("olive"));
    addTarget("idpa", Length(337, Length::Unit::m), Length(5, Length::Unit::m), Speed(1), QColor("olive"));

    render();
//    setWindowState(Qt::WindowFullScreen);
}

ProjectionDialog::~ProjectionDialog()
{
    delete ui;
}

double ProjectionDialog::rescaleMmAtRangeToPxAtRealRange(const Length& distance, const Length& size) const
{
    return size.toAngle(distance).toLength(sceneDistance()).value(Length::Unit::px);
}

void ProjectionDialog::renderTargets()
{
    QMap<int, TargetPos>& target_ranges = pTargetModel->targets();

    for (TargetPos& pos: target_ranges)
        if (pos.pAnima)
            pos.curTime.setValue(pos.pAnima->currentTime(), Time::Unit::ms);

    removeSceneItems(targetItems);

    for (TargetPos& target_pos: target_ranges)
    {
        if (target_pos.pAnima)
        {
            target_pos.pAnima->stop();
            delete target_pos.pAnima;
            target_pos.pAnima = nullptr;
        }

        double scale = sceneDistance() / target_pos.distance * dpm();
        TargetPtr target = TargetBuilder::ref().target(target_pos.name, scale, rangeCorrectedColor(target_pos.distance, target_pos.color));
        QGraphicsItemGroup* pTargetImg = target->release();
        pTargetImg->setZValue((max_range() - target_pos.distance).value(Length::Unit::m));
        target_pos.pImg = pTargetImg;
        pTargetImg->setVisible(target_pos.hidden != TargetPos::Hidden);
        pScene->addItem(pTargetImg);

        targetItems << pTargetImg;

        double target_x = - rescaleMmAtRangeToPxAtRealRange(target_pos.distance, sceneWidth()) * viewHorzPosition();
        double target_y = y_coord(target_x) - pTargetImg->boundingRect().height()/2 + 1;
        double target_offset = rescaleMmAtRangeToPxAtRealRange(target_pos.distance, target_pos.offset);
        pTargetImg->setPos(target_x + target_offset, target_y);

        double targetMargin = pTargetImg->boundingRect().width()/2;

        target_pos.pAnima = new QPropertyAnimation(dynamic_cast<QObject*>(pTargetImg), "pos", this);
        if (target_pos.speed != Speed(0))
        {
            target_pos.pAnima->setKeyValueAt(0.0, QPointF(target_x + targetMargin,target_y));
            target_pos.pAnima->setKeyValueAt(1.0, QPointF(target_x + targetMargin,target_y));
            target_pos.pAnima->setKeyValueAt(0.5, QPointF(target_x + rescaleMmAtRangeToPxAtRealRange(target_pos.distance, sceneWidth()) - targetMargin, target_y));
            target_pos.pAnima->setDuration(2 * static_cast<int>(sceneWidth().value(Length::Unit::mm) / target_pos.speed.value(Length::Unit::mm, Time::Unit::ms)));
            target_pos.pAnima->setLoopCount(-1);

            target_pos.pAnima->start();
            target_pos.pAnima->setCurrentTime(target_pos.curTime.value(Time::Unit::ms));
        }
        if (target_pos.hidden != TargetPos::Running)
            target_pos.pAnima->pause();
    }
}

void ProjectionDialog::renderWalls()
{
    removeSceneItems(wallItems);

    QPointF rightWall_a (x_min() + rescaleMmAtRangeToPxAtRealRange(min_range(), sceneWidth()), y_min());
    QPointF rightWall_A (x_min() + rescaleMmAtRangeToPxAtRealRange(min_range(), sceneWidth()), y_min() - rescaleMmAtRangeToPxAtRealRange(min_range(), wallHeight()));
    QPointF rightWall_B (x_max() + rescaleMmAtRangeToPxAtRealRange(max_range(), sceneWidth()), y_max() - rescaleMmAtRangeToPxAtRealRange(max_range(), wallHeight()));
    QPointF rightWall_b (x_max() + rescaleMmAtRangeToPxAtRealRange(max_range(), sceneWidth()), y_max());

    QPointF leftWall_a (x_min(), y_min());
    QPointF leftWall_A (x_min(), y_min() - rescaleMmAtRangeToPxAtRealRange(min_range(), wallHeight()));
    QPointF leftWall_b (x_max(), y_max());
    QPointF leftWall_B (x_max(), y_max() - rescaleMmAtRangeToPxAtRealRange(max_range(), wallHeight()));

    QPolygonF wall;
    wall << rightWall_a
              << rightWall_A
              << rightWall_B
              << rightWall_b;

    QPen wallPen(floorColor());
    QLinearGradient wallGradient;
    wallGradient.setStart(rightWall_A);
    wallGradient.setFinalStop(rightWall_B);
    wallGradient.setColorAt(0, wallColor());
    wallGradient.setColorAt(1.0, rangeCorrectedColor(max_range(), wallColor()));
    wallItems << pScene->addPolygon(wall, wallPen, QBrush(wallGradient));

    wall.clear();
    wall << leftWall_a
              << leftWall_A
              << leftWall_B
              << leftWall_b;

    wallGradient.setStart(leftWall_A);
    wallGradient.setFinalStop(leftWall_B);
    wallItems << pScene->addPolygon(wall, wallPen, QBrush(wallGradient));

}

void ProjectionDialog::renderFloor()
{
    removeSceneItems(floorItems);

    const Length step = ui->pTarWidget->params().rangeStep;
    bool odd = false;
    for (Length range =min_range(); range<=max_range(); range+=step)
    {
        double vsize = rescaleMmAtRangeToPxAtRealRange(range, sceneWidth());

        double x1 =   vsize * (1 - viewHorzPosition());
        double x2 = - vsize * viewHorzPosition();
        double y = y_coord(x2);

        floorItems << pScene->addLine(x1,y, x2, y, QPen(rangeCorrectedColor(range, floorColor())));
        QGraphicsTextItem* pText = pScene->addText(QString::number(range.value(Length::Unit::m)));
        //TODO: use transfor:  QGraphicsItem::setTransform(const QTransform &matrix, bool combine = false), QTransform::quadToQuad
        if (odd)
            pText->setPos(x2 - pText->boundingRect().width(), y - pText->boundingRect().height()/2);
        else
            pText->setPos(x1 + 10, y - pText->boundingRect().height()/2);
        odd = !odd;
        floorItems << pText;
    }

    for (Length x(0); x<=sceneWidth(); x += Length(1, Length::Unit::m))
    {
        double off1 =   rescaleMmAtRangeToPxAtRealRange(min_range(), x);
        double off2 =   rescaleMmAtRangeToPxAtRealRange(max_range(), x);
        QLinearGradient grad;
        grad.setStart(x_min() + off1, 0);
        grad.setFinalStop(x_max() + off2, -viewVertPosition());
        grad.setColorAt(0, floorColor());
        grad.setColorAt(1.0, rangeCorrectedColor(max_range(), floorColor()));

        floorItems << pScene->addLine(x_min() + off1, 0, x_max()+off2, -viewVertPosition(), QPen(QBrush(grad), 1));
    }
}

void ProjectionDialog::removeSceneItems(QList<QGraphicsItem*>& itemList)
{
    for (QGraphicsItem* pItem: itemList)
    {
        pScene->removeItem(pItem);
        delete pItem;
    }
    itemList.clear();
}

void ProjectionDialog::setCustomDpi(double dpi)
{
    customDpi = dpi;
    render();
}

void ProjectionDialog::setVertCorr(double moa)
{
    vertCorr.setValue(moa, Angle::Unit::moa);
    renderTrajectory();
}

void ProjectionDialog::setSceneDistance(double meters)
{
    sceneDist.setValue(meters, Length::Unit::m);
    render();
}

void ProjectionDialog::setSceneWidth(double meters)
{
    sceneWth.setValue(meters, Length::Unit::m);
    render();
}

void ProjectionDialog::addTarget(const QString& name, const Length& distance, const Length& offset, const Speed& speed, const QColor& color)
{
    pTargetModel->addTarget(name, distance, offset, speed, color);
}

QColor ProjectionDialog::rangeCorrectedColor(Length range, const QColor &c) const
{
    int k = 100 + 20 * static_cast<int>((range - min_range()) / (max_range()-min_range()));
    return c.light(k);
}

QColor ProjectionDialog::floorColor() const
{
    return QColor("gray");
}

QColor ProjectionDialog::wallColor() const
{
    return QColor("grey");
}

QColor ProjectionDialog::trajectoryColor(bool above_ground) const
{
    if (above_ground)
        return QColor("light blue");
    else
        return QColor("red");
}

Length ProjectionDialog::wallHeight() const
{
    return Length(750, Length::Unit::mm);
}

Length ProjectionDialog::min_range() const
{
    return ui->pTarWidget->params().rangeBegin;
}

Length ProjectionDialog::max_range() const
{
    return ui->pTarWidget->params().rangeEnd;
}

double ProjectionDialog::x_min() const
{
    return - rescaleMmAtRangeToPxAtRealRange(min_range(), sceneWidth()) * viewHorzPosition();
}

double ProjectionDialog::y_min() const
{
    return 0;
}

double ProjectionDialog::x_max() const
{
    return - rescaleMmAtRangeToPxAtRealRange(max_range(), sceneWidth()) * viewHorzPosition();
}

double ProjectionDialog::y_max() const
{
    return -viewVertPosition();
}

double ProjectionDialog::y_coord(double x)
{
    return y_min() + (x-x_min())*(y_max()-y_min()) / (x_max()-x_min());
}

double ProjectionDialog::viewVertPosition() const
{
    return ui->pVertSlider->value();
}

double ProjectionDialog::viewHorzPosition() const
{
    return ui->pHorzSlider->value() / 100.0;
}

Length ProjectionDialog::trajectoryPosition() const
{
    return sceneWidth() * .5;
}

Length ProjectionDialog::weaponHeight() const
{
    return Length(375, Length::Unit::mm);
}

Angle ProjectionDialog::trajectoryVertCorrection() const
{
    return vertCorr;
}

void ProjectionDialog::render()
{
    if (customDpi < 1)
        setDpi(qApp->primaryScreen()->physicalDotsPerInch());
    else
        setDpi(customDpi);

    renderWalls();
    renderFloor();
    renderTargets();
    renderTrajectory();
}

void ProjectionDialog::renderTrajectory()
{
    removeSceneItems(trajectoryItems);

    if (ui->pShowTrajectory->isChecked())
    {

        Solution solution;
        solution.build(ui->pBulWidget->params(), ui->pAtmWidget->params());

        for (Length range =ui->pTarWidget->params().rangeBegin;range<=ui->pTarWidget->params().rangeEnd; range+=Length(2, Length::Unit::m))
        {
            Length drop = solution.drop(range);
            double drop_x = rescaleMmAtRangeToPxAtRealRange(range, trajectoryPosition());
            double shoot_height = rescaleMmAtRangeToPxAtRealRange(range, weaponHeight() + drop);
            shoot_height += rescaleMmAtRangeToPxAtRealRange(range, range * ui->pBulWidget->params().shootingAngle.tg());
            shoot_height += rescaleMmAtRangeToPxAtRealRange(range, trajectoryVertCorrection().toLength(range));
            double vsize = rescaleMmAtRangeToPxAtRealRange(range, sceneWidth());
            double x2 = - vsize * viewHorzPosition();
            double y = y_coord(x2);
            double ellipseRadius = Angle(2, Angle::Unit::moa).toLength(sceneDistance()).value(Length::Unit::px);
            QGraphicsItem* pBullet = pScene->addEllipse(x2 + drop_x - ellipseRadius, y - shoot_height - ellipseRadius, 2*ellipseRadius, 2*ellipseRadius, rangeCorrectedColor(range, trajectoryColor(shoot_height>0)));
            pBullet->setZValue((max_range() - range).value(Length::Unit::m) + 0.01);
            trajectoryItems.append(pBullet);
        }
    }
}

Length ProjectionDialog::sceneWidth() const
{
    return sceneWth;
}

Length ProjectionDialog::sceneDistance() const
{
    return sceneDist;
}

TargetModel::TargetModel(QObject* parent)
    :QAbstractTableModel(parent)
{
}

int TargetModel::rowCount(const QModelIndex& /*parent*/) const
{
    int cnt = _targets.size();
    return cnt;
}

int TargetModel::columnCount(const QModelIndex& /*parent*/) const
{
    return 6;
}

QVariant TargetModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();

    const TargetPos& pos = _targets[index.row()];
    if (role == Qt::DisplayRole)
    {
        switch (index.column())
        {
            case 0: switch (pos.hidden)
                {
                    case TargetPos::Hidden:   return "hidden";
                    case TargetPos::Paused: return "paused";
                    case TargetPos::Running: return "running";
                }
            case 1: return pos.name;
            case 2: return QString("%1 m").arg(pos.distance.value(Length::Unit::m));
            case 3: return QString("%1 m").arg(pos.offset.value(Length::Unit::m));
            case 4: return QString("%1 m/s").arg(pos.speed.value(Length::Unit::m, Time::Unit::s));
            case 5: return pos.color.name();
            default:
                break;
        }
    }
    if (role == Qt::EditRole)
    {
        switch (index.column())
        {
            case 0: return QVariant();
            case 1: return pos.name;
            case 2: return pos.distance.value(Length::Unit::m);
            case 3: return pos.offset.value(Length::Unit::m);
            case 4: return pos.speed.value(Length::Unit::m, Time::Unit::s);
            case 5: return pos.color;
            default:
                break;
        }
    }
    if (role == Qt::CheckStateRole)
    {
        if (index.column() == 0)
        {
            switch (pos.hidden)
            {
                case TargetPos::Hidden:   return Qt::Unchecked;
                case TargetPos::Paused: return Qt::PartiallyChecked;
                case TargetPos::Running: return Qt::Checked;
            }
        }
    }
    if (role == Qt::DecorationRole)
    {
        if (index.column() == 5)
            return pos.color;
    }

    return QVariant();
}

QVariant TargetModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if (orientation == Qt::Horizontal)
            switch (section)
            {
                case 0:  return "animate";
                case 1:  return "target name";
                case 2:  return "distance";
                case 3:  return "offset";
                case 4:  return "speed";
                case 5:  return "color";
            }
        else
            return section+1;
    }

    return QVariant();
}

bool TargetModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
    if (!index.isValid())
        return false;

    TargetPos& pos = _targets[index.row()];
    if (role == Qt::CheckStateRole)
        if (index.column() == 0)
        {
            switch (value.toInt())
            {
                case Qt::Unchecked:  pos.hidden = TargetPos::Hidden; break;
                case Qt::PartiallyChecked: pos.hidden = TargetPos::Paused; break;
                case Qt::Checked: pos.hidden = TargetPos::Running; break;
            }
            emit dataChanged(index, index );
            return true;
        }
    if (role == Qt::EditRole)
    {
        switch(index.column())
        {
            case 1: pos.name = value.toString(); break;
            case 2: pos.distance.setValue(value.toInt(), Length::Unit::m); break;
            case 3: pos.offset.setValue(value.toInt(), Length::Unit::m); break;
            case 4: pos.speed.setValue(value.toInt(), Length::Unit::m, Time::Unit::s); break;
            case 5: pos.color = qvariant_cast<QColor>(value);
        }
        emit dataChanged(index, index );
        return true;
    }
    return false;
}

Qt::ItemFlags TargetModel::flags(const QModelIndex& index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;
    if (index.column() == 0)
        return Qt::ItemIsEnabled | Qt::ItemIsUserCheckable | Qt::ItemIsUserTristate;

    return Qt::ItemIsEnabled | Qt::ItemIsEditable;
}

void TargetModel::addTarget(const QString& name, const Length& distance, const Length& offset, const Speed& speed, const QColor& color)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    TargetPos t = {name, distance, offset, speed, color, nullptr, nullptr, offset / speed, TargetPos::Running};
    _targets.insert(rowCount(), t);
    endInsertRows();
}

TargetSelectDelegate::TargetSelectDelegate(QObject* parent)
    :QStyledItemDelegate(parent)
{
}

QWidget* TargetSelectDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& /*option*/, const QModelIndex& /*index*/) const
{
    QComboBox* pBox = new QComboBox(parent);
    pBox->setFrame(false);
    pBox->addItems(TargetBuilder::ref().availableTargets());

    return pBox;
}

void TargetSelectDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
    QString text = index.model()->data(index, Qt::EditRole).toString();
    QComboBox* pBox = dynamic_cast<QComboBox*>(editor);
    pBox->setCurrentText(text);
}

void TargetSelectDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
    QComboBox* pBox = dynamic_cast<QComboBox*>(editor);
    model->setData(index, pBox->currentText(), Qt::EditRole);
}

void TargetSelectDelegate::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& /*index*/) const
{
    editor->setGeometry(option.rect);
}


DistanceEditDelegate::DistanceEditDelegate(int min, int max, int step, const QString& suff, QObject* parent)
    :QStyledItemDelegate(parent), _min(min), _max(max), _step(step), _suff(suff)
{
}

QWidget*DistanceEditDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& /*option*/, const QModelIndex& /*index*/) const
{
    QSpinBox* pBox = new QSpinBox(parent);
    pBox->setFrame(false);
    pBox->setRange(_min, _max);
    pBox->setSingleStep(_step);
    pBox->setSuffix(" " + _suff);
    return pBox;
}

void DistanceEditDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
    int val = index.model()->data(index, Qt::EditRole).toInt();
    QSpinBox* pBox = dynamic_cast<QSpinBox*>(editor);
    pBox->setValue(val);
}

void DistanceEditDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
    QSpinBox* pBox = dynamic_cast<QSpinBox*>(editor);
    model->setData(index, pBox->value(), Qt::EditRole);
}

void DistanceEditDelegate::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& /*index*/) const
{
    editor->setGeometry(option.rect);
}

ColorSelectDelegate::ColorSelectDelegate(const QStringList& colors, QObject* parent)
    :QStyledItemDelegate(parent), colorNames(colors)
{
}

QWidget*ColorSelectDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& /*option*/, const QModelIndex& /*index*/) const
{
    QComboBox* pBox = new QComboBox(parent);
    pBox->setFrame(false);
    for (int i = 0; i < colorNames.size(); ++i)
    {
            QColor color(colorNames[i]);
            pBox->insertItem(i, colorNames[i]);
            pBox->setItemData(i, color, Qt::DecorationRole);
    }
    return pBox;
}

void ColorSelectDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
    QComboBox* pBox = dynamic_cast<QComboBox*>(editor);
    QColor color = qvariant_cast<QColor>(index.model()->data(index, Qt::DecorationRole));
    int idx = pBox->findData(color, Qt::DecorationRole);
    pBox->setCurrentIndex(idx);
}

void ColorSelectDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
    QComboBox* pBox = dynamic_cast<QComboBox*>(editor);
    QColor color = qvariant_cast<QColor>(pBox->itemData(pBox->currentIndex(), Qt::DecorationRole));
    model->setData(index, color, Qt::EditRole);
}

void ColorSelectDelegate::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& /*index*/) const
{
    editor->setGeometry(option.rect);
}

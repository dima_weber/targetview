 #include "targetview.h"

#include <QtWidgets/QGraphicsScene>

#include <QtGui/QWheelEvent>
#include <QtGui/QMouseEvent>

TargetView::TargetView(QWidget* parent)
	:QGraphicsView(parent), _currentScale(1)
{

}

TargetView::TargetView(QGraphicsScene *pScene, QWidget *parent)
	:QGraphicsView(pScene, parent), _currentScale(1)
{
	viewport()->setCursor(Qt::CrossCursor);
}

void TargetView::originalScale()
{
	scale (1/_currentScale, 1/_currentScale);
	_currentScale = 1;
}

void TargetView::wheelEvent(QWheelEvent *pEvent)
{
	if(pEvent->modifiers() & Qt::ControlModifier)
	{
		QGraphicsView::wheelEvent(pEvent);
	}
	else
	{
		if(pEvent->delta() > 0)
		{
			scale(1.2, 1.2);
			_currentScale *= 1.2;
		}
		else
		{
			scale (1/1.2, 1/1.2);
			_currentScale /= 1.2;
		}
	}
}

void TargetView::mousePressEvent(QMouseEvent* mouseEvent)
{
	QGraphicsView::mousePressEvent(mouseEvent);
	viewport()->setCursor(Qt::ClosedHandCursor);
}

void TargetView::mouseReleaseEvent(QMouseEvent* mouseEvent)
{
	QGraphicsView::mouseReleaseEvent(mouseEvent);
	viewport()->setCursor(Qt::CrossCursor);
}

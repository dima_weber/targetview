#ifndef TRAJECTORYWIDGET_H
#define TRAJECTORYWIDGET_H

#include <QtWidgets/QWidget>
#include <ballistics/ballistics.h>
#include "types.h"

namespace Ui {
class TrajectoryWidget;
}

class QStandardItemModel;
class QGraphicsScene;
class QCPCurve;

class TrajectoryWidget : public QWidget
{
	Q_OBJECT

public:
	explicit TrajectoryWidget(Solution::Ptr solution, QWidget *parent = 0);
	~TrajectoryWidget();

public slots:
	void populateView();
	void populateView(const TargetParams& targetParams);
	void recalculate(const BulletParams& bulletParams);
	void recalculate(const AtmosphereParams& atmosphereParams);
	void init(const BulletParams&, const AtmosphereParams&, const TargetParams&);
	void onPrint();

private:
	Ui::TrajectoryWidget *ui;

	QGraphicsScene* pDropScene;
	QStandardItemModel* pRangeModel;
	Solution::Ptr solution;
	QCPCurve* pSightLine;

	TargetParams target;
};

#endif // TRAJECTORYWIDGET_H

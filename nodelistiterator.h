#ifndef NODELISTITERATOR_H
#define NODELISTITERATOR_H

#include <QtXml/QDomElement>
#include <memory>
#include <exception>

#define NODELIST_ITERATOR_DO_RANGE_CHECK

class NodeListIterator : public std::iterator<std::bidirectional_iterator_tag, QDomNode, int>
{
    QDomNodeList* nodeList;
    int idx;
public:

    static NodeListIterator begin(QDomNodeList& lst)
    {
        NodeListIterator iter(lst);
        return iter;
    }
    static NodeListIterator end(QDomNodeList& lst)
    {
        NodeListIterator iter(lst);
        iter.idx = lst.size();
        return iter;
    }

    NodeListIterator(QDomNodeList& list)
        :nodeList(&list), idx(0)
    {}

    inline bool isValid() const
    {
        return idx >= 0 && idx < nodeList->size();
    }

    inline NodeListIterator& operator++()
    {
        ++idx;
        return *this;
    }
    inline  NodeListIterator operator++(int)
    {
        NodeListIterator ret (*this);

        idx++;
        return ret;
    }

    inline NodeListIterator& operator--()
    {
        ++idx;
        return *this;
    }

    inline NodeListIterator operator--(int)
    {
        NodeListIterator ret (*this);

        idx--;
        return ret;
    }

    inline bool operator == (const NodeListIterator& other ) const
    {
        return idx == other.idx && nodeList == other.nodeList;
    }
    inline bool operator != (const NodeListIterator& other ) const
    {
        return idx != other.idx || nodeList != other.nodeList;
    }

    inline QDomNode operator*()
    {
#ifdef NODELIST_ITERATOR_DO_RANGE_CHECK
        if (!isValid())
            throw std::out_of_range("invalid iterator");
#endif
        return nodeList->at(idx);
    }
};

class DomNodeList : public QDomNodeList
{
public:
    DomNodeList(const QDomNodeList& other)
        :QDomNodeList(other)
    {}
    typedef NodeListIterator iterator;
    NodeListIterator begin() { return NodeListIterator::begin(*this); }
    NodeListIterator end() { return NodeListIterator::end(*this); }
};

#endif // NODELISTITERATOR_H


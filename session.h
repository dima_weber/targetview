#ifndef SESSION_H
#define SESSION_H

#include <QtCore/QDate>
#include <QtCore/QMap>

#include "serie.h"
#include "target.h"

class Session : public std::enable_shared_from_this<Session>, public Serie
{
public:
    enum class SaveFormat {Csv, Xml};
    double dpm;
    Series series;

    void selectSerie(SeriePtr p);
    void unselectSerie(SeriePtr p);

    virtual Holes holes() const override;

    Session();
    virtual ~Session();

    virtual QColor getColor() const override;
    void clear();
    static bool save(Sessions sessions, const QString& filePath, SaveFormat format) ;
    void setVisible(bool show);

    virtual QString name() const override;

    QDate date() const override;
    Caliber caliber() const override;
    const QString& targetName() const override;
    const QString& shooter() const override;

    void setCaliber(const Caliber& caliber);
    void setDate(const QDate& date);
    void setTargetName(const QString& targetName);
    void setShooter(const QString& shooter);

    TargetPtr target() const override;

    void addSerie(const SeriePtr& serie);
    void removeSerie(SeriePtr serie);

private:
    QVector<SeriePtr> selectedSeries;

    QDate _date;
    Caliber _caliber;
    QString _targetName;
    QString _shooter;
    mutable TargetPtr _targetCache;

};

bool readXmlTag(const QDomElement& sessionElement, Session& session, const ReadXmlContext& context, bool optional =true);
bool writeXmlTag(QDomElement& sessionElement, const Session& session);

#endif // SESSION_H


#ifndef BULLETPROPERTIESWIDGET_H
#define BULLETPROPERTIESWIDGET_H

#include <QtWidgets/QWidget>

struct BulletParams;

namespace Ui {
class BulletPropertiesWidget;
}

class BulletPropertiesWidget : public QWidget
{
    Q_OBJECT

public:
    explicit BulletPropertiesWidget(QWidget *parent = 0);
    ~BulletPropertiesWidget();

    const BulletParams& params() const;
    void setParams(const BulletParams& param);

signals:
    void changed();

private:
    Ui::BulletPropertiesWidget *ui;
};

#endif // BULLETPROPERTIESWIDGET_H

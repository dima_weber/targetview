#ifndef ACCESSOR_H
#define ACCESSOR_H

#include <memory>

template <class T>
struct Accessor
{
	T t;
	Accessor(){}
	inline const T& val() const
	{
		return t;
	}
	inline T& ref()
	{
		return t;
	}
};

template <class T>
struct Accessor<T*>
{
	T* p;
	Accessor(T* copy = nullptr)
		:p(copy?copy:new T())
	{
	}
	inline T* val() const
	{
		return p;
	}
	inline T& ref()
	{
		return *p;
	}
};

template <class T>
struct Accessor<std::shared_ptr<T>>
{
	std::shared_ptr<T> s;
	Accessor(const std::shared_ptr<T>& copy = nullptr)
		:s(copy?copy:std::make_shared<T>())
	{
	}

	inline const std::shared_ptr<T>& val() const
	{
		return s;
	}
	inline T& ref()
	{
		if (s)
			return *s;
		else
			throw std::runtime_error("null pointer");
	}
};

#endif // ACCESSOR_H


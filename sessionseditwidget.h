#ifndef SESSIONSEDITWIDGET_H
#define SESSIONSEDITWIDGET_H

#include "session.h"
#include "seriecollection.h"

#include <QWidget>

class SessionEditWidget;
class QListWidget;
class SerieCollection;

class SessionsEditWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SessionsEditWidget(Sessions& sessions, QWidget *parent = 0);
    ~SessionsEditWidget();

private:
    Sessions& sessions;
    SerieCollection collection;
    QListWidget* pSessionList;
    SessionEditWidget* pSessionEdit;
};

#endif // SESSIONSEDITWIDGET_H

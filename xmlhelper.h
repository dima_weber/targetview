#ifndef XMLHELPER_H
#define XMLHELPER_H

#include "accessor.h"
#include "nodelistiterator.h"
#include "types.h"

#include <QtConcurrent>
#include <QScriptEngine>

#include <functional>

#define USE_ITERATOR_FOR_READ_XML

class Length;
class Caliber;
class QString;
class FontSize;
class Angle;
class QPointF;
class QPolygonF;
class QColor;
class QDate;
class PolarCoordinate;

struct ReadXmlContext
{
    bool is_threaded;
    double dpm;
    double scale;
    QColor color;
    std::unique_ptr<QScriptEngine> pScriptEngine;

    ReadXmlContext()
        :is_threaded(false), dpm(1), scale(1), pScriptEngine(new QScriptEngine)
    {}

    ReadXmlContext clone() const
    {
        ReadXmlContext context;
        context.is_threaded = is_threaded;
        context.dpm = dpm;
        context.scale = scale;
        return context;
    }

    ReadXmlContext& set_threaded(bool v){is_threaded = v; return *this;}

    ReadXmlContext(const ReadXmlContext& other)
        :is_threaded(other.is_threaded), dpm(other.dpm), scale(other.scale), pScriptEngine(new QScriptEngine)
    {
    }
};

template<class T>
bool readChildTag(const QDomElement& e, const QString& tag, T& value, const ReadXmlContext& context, bool optional=true)
{
    bool ok;
    QDomElement childE = e.firstChildElement(tag);
    ok = !childE.isNull();
    if (ok)
    {
        ok = readXmlTag(childE, value, context, optional);
    }
    return ok;
}

template<class T>
bool readChildTag(const QDomElement& e, const QString& tag, T* value, const ReadXmlContext& context, bool optional=true)
{
    return readChildTag(e, tag, *value, optional);
}

template<class T>
bool readChildTag(const QDomElement& e, const QString& tag, std::shared_ptr<T> value, const ReadXmlContext& context, bool optional=true)
{
    return readChildTag(e, tag, *value, optional);
}

template<class T>
bool readChildArray(const QDomElement& parentE, const QString& tagName, const QString& childTagname, MutexVector<T>& array, const ReadXmlContext& context, bool optional = true)
{
    bool ok;
    QDomElement arrayElement = parentE.firstChildElement(tagName);
    ok = !arrayElement.isNull();
    DomNodeList childList = arrayElement.elementsByTagName(childTagname);

    auto mapFunction = [optional, &context, &ok, &array](QDomNode node)
    {
        QDomElement childElement = node.toElement();
        Accessor<T> item;
        ok = readXmlTag(childElement, item.ref(), context.is_threaded?context:context.clone().set_threaded(true), optional);
        array.mutexAppend(item.val());
    };

    if (!context.is_threaded)
    {
        QtConcurrent::blockingMap(std::begin(childList), std::end(childList), mapFunction);
    }
    else
    {
#ifdef USE_ITERATOR_FOR_READ_XML
        std::for_each (std::begin(childList), std::end(childList), mapFunction);
#else
        for(int idx=0; ok && idx < childList.size(); ++idx)
            mapFunction(childList.item(idx));
#endif
    }

    return ok;
}

template<typename T>
using ProcessFunction = std::function<void(T)>;

template<class T>
bool readChildArray(const QDomElement& parentE, const QString& tagName, const QString& childTagname, ProcessFunction<T>& func, const ReadXmlContext& context, bool optional = true)
{
    bool ok;
    QDomElement arrayElement = parentE.firstChildElement(tagName);
    ok = !arrayElement.isNull();
    DomNodeList childList = arrayElement.elementsByTagName(childTagname);

    auto mapFunction = [optional, &context, &ok, func](QDomNode node)
    {
        QDomElement childElement = node.toElement();
        Accessor<T> item;
        ok = readXmlTag(childElement, item.ref(), context.is_threaded?context:context.clone().set_threaded(true), optional);
        func(item.val());
    };

    if (!context.is_threaded)
    {
        QtConcurrent::blockingMap(std::begin(childList), std::end(childList), mapFunction);
    }
    else
    {
        std::for_each (std::begin(childList), std::end(childList), mapFunction);
    }

    return ok;
}

template<class T>
bool writeChildTag(QDomDocument& doc, QDomElement& parentE, const QString& tag, std::shared_ptr<T> value)
{
    return writeChildTag(doc, parentE, tag, *value);
}

template <class T>
bool writeChildTag(QDomDocument& doc, QDomElement& parentE, const QString& tag, T* value)
{
    if (value)
        return writeChildTag(doc, parentE, tag, *value);
    else
        return false;
}

template <class T>
bool writeChildTag(QDomDocument& doc, QDomElement& parentE, const QString& tag, const T& value)
{
    bool ok;
    QDomElement element = doc.createElement(tag);
    ok = writeXmlTag(doc, element, value);
    parentE.appendChild(element);
    return ok;
}

template<class T>
bool writeChildArray(QDomDocument& doc, QDomElement& parentE, const QString& tagName, const QString& childTagname, const QVector<T>& array)
{
    QDomElement arrayElement = doc.createElement(tagName);
    bool ok;
    for(const T& t: array)
    {
        ok = writeChildTag(doc, arrayElement, childTagname, t);
        if (!ok)
            break;
    }
    parentE.appendChild(arrayElement);
    return ok;
}

template <class T>
bool readXmlTag(const QDomElement& tElement, T& t, const ReadXmlContext& /*context*/, bool optional = true)
{
    Q_UNUSED(tElement)
    Q_UNUSED(t)
    Q_UNUSED(optional)
    throw std::runtime_error("you must implement readXmlTag for this type");
    return false;
}

template <class T>
bool writeXmlTag(QDomDocument &doc, QDomElement& tElement, const T& t)
{
    Q_UNUSED(tElement)
    Q_UNUSED(t)
    Q_UNUSED(doc)
    throw std::runtime_error("you must implement writeXmlTag for this type");
    return false;
}

bool readXmlTag(const QDomElement& intElement,       int& value, const ReadXmlContext& context, bool optional=true);
bool writeXmlTag(QDomDocument &doc, QDomElement& intElement, int value);

bool readXmlTag(const QDomElement& doubleElement, double& value, const ReadXmlContext& context, bool optional=true);
bool writeXmlTag(QDomDocument &doc, QDomElement& doubleElement, double value);

bool readXmlTag (const QDomElement& lengthElement,  Length& value, const ReadXmlContext& context, bool optional=true);
bool writeXmlTag(QDomDocument &doc, QDomElement& lengthElement, const Length& value);

bool readXmlTag (const QDomElement& caliberElement,  Caliber& caliber, const ReadXmlContext& context, bool optional=true);
bool writeXmlTag(QDomDocument &doc, QDomElement& caliberElement, const Caliber& value);

bool readXmlTag (const QDomElement& stringElement, QString& value, const ReadXmlContext& context,  bool optional=true);
bool writeXmlTag(QDomDocument &doc, QDomElement& stringElement, const QString& value);

bool readXmlTag(const QDomElement& sizeElement,  FontSize& fontSize, const ReadXmlContext& context, bool optional=true);

bool readXmlTag(const QDomElement& angleElement,    Angle& value, const ReadXmlContext& context,  bool optional=true);
bool writeXmlTag(QDomDocument& doc, QDomElement& angleElement, const Angle& angle);

bool  readXmlTag(const QDomElement& pointElement,  QPointF& value, const ReadXmlContext& context,  bool optional=true);
bool writeXmlTag(QDomDocument& doc, QDomElement& pointElement, const QPointF& point);

bool  readXmlTag(const QDomElement& polarElement, PolarCoordinate& coord, const ReadXmlContext& context, bool optional = true);
bool writeXmlTag(QDomDocument &doc, QDomElement &polarElement, const PolarCoordinate &coord);

bool readXmlTag(const QDomElement& polygonElement, QPolygonF& poly, const ReadXmlContext& context, bool optional=true);

bool readXmlTag(const QDomElement& colorElenent,   QColor& color, const ReadXmlContext& context, bool optional=true);
bool writeXmlTag(QDomDocument &doc, QDomElement &colorElement, const QColor& color);

bool  readXmlTag(const QDomElement& dateElement,     QDate& date, const  ReadXmlContext& context,  bool optional=true);
bool writeXmlTag(QDomDocument &doc, QDomElement& dateElement, const QDate& date);
#endif // XMLHELPER_H


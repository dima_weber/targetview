#ifndef GUISERIE_H
#define GUISERIE_H

#include <QtCore/QObject>

#include "serie.h"

class QGraphicsScene;
class QGraphicsItemGroup;
class QPushButton;
class GuiSerie;
typedef std::shared_ptr<GuiSerie> GuiSeriePtr;
typedef std::weak_ptr<GuiSerie> GuiSerieWPtr;

class GuiSerie : public QObject
{
    Q_OBJECT
public:
    enum class CircleType {None, MOA, MilRad};
    GuiSerie(QGraphicsScene& scene, SeriePtr serie, QWidget* parent = nullptr);

    virtual ~GuiSerie();

    QGraphicsItem* guiMassCenter();
    QGraphicsItem* guiBounds();
    QGraphicsItem* guiMaxDistance();
    QGraphicsItem* guiMoaCircles();
    QGraphicsItem* guiMilradCircles();
    QGraphicsItem* guiHoles();

    QPushButton* pButton;

    void addGuiSerie(GuiSeriePtr spGuiSerie);
    void setVisible(bool show);

public slots:
    void showMassCenter(bool show = true);
    void showBoundingRect(bool show = true);
    void showMaxDistance(bool show = true);
    void showCircle(CircleType);

    void invalidate();

    SerieWPtr serie();
private:
    std::unique_ptr<QPen> _seriePenCache;
    std::unique_ptr<QPen> _holePenCache;
    std::unique_ptr<QPen> _textPenCache;
    std::unique_ptr<QBrush> _holeBrushCache;

    QVector<GuiSerieWPtr> subGuis;

    QGraphicsScene& scene;
    SerieWPtr wpSerie;
    QGraphicsItemGroup* pSerieGui;

    std::unique_ptr<QGraphicsItem> _pGuiMassCenter;
    std::unique_ptr<QGraphicsItem> _pGuiBounds;
    std::unique_ptr<QGraphicsItem> _pGuiMaxDistance;
    std::unique_ptr<QGraphicsItem> _pGuiMoaCircles;
    std::unique_ptr<QGraphicsItem> _pGuiMilradCircles;
    std::unique_ptr<QGraphicsItem> _pGuiHoles;

    const QPen& seriePen();
    const QPen& holePen();
    const QBrush& holeBrush();
    const QPen& textPen();

    QGraphicsItem* buildGuiMassCenter();
    QGraphicsItem* buildGuiBounds();
    QGraphicsItem* buildGuiMaxDistance();
    QGraphicsItem* buildGuiCircles(int radius);
    QGraphicsItem* buildGuiHoles();

    QGraphicsItem* buildHole(const HolePtr& hole, Length caliber, Serie::HoleShape shape);

    void clear();
};

#endif // GUISERIE_H

#ifndef SERIEEDITWIDGET_H
#define SERIEEDITWIDGET_H

#include "serie.h"

#include <QWidget>

class SerieCollection;
class QComboBox;
class QSpinBox;
class QLineEdit;
class QTextEdit;

class SerieEditWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SerieEditWidget(SerieCollection& collection, QWidget *parent = 0);
    ~SerieEditWidget();

public slots:
    void setSerie(SeriePtr serie);

private:
    SerieCollection& collection;
    SeriePtr serie;

    QComboBox* pPositionSelect;
    QComboBox* pScopeSelect;
    QComboBox* pFirestyleSelect;
    QLineEdit* pColorEdit;
    QLineEdit* pNameEdit;
    QSpinBox*  pNumberEdit;
    QSpinBox*  pRangeEdit;
    QComboBox* pRangeUnit;
    QTextEdit* pCommentEdit;
};

#endif // SERIEEDITWIDGET_H

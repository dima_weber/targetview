#ifndef SERIEFORTFILTERMODEL_H
#define SERIEFORTFILTERMODEL_H

#include <QSortFilterProxyModel>

class SerieSortFilterModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    SerieSortFilterModel(QObject* parent = nullptr);

protected:
    bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const override;
};

#endif // SERIEFORTFILTERMODEL_H

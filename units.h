#ifndef UNITS_H
#define UNITS_H

#include "units_converter.h"

#include <gsl/gsl_math.h>
#include <gsl/gsl_sf_trig.h>

#include <cstring>

#define CMP_PRECISION 0.000002

class Length;
class Angle;

class Angle
{
public:
    static double DegToMoa(double deg){return deg*60;}
    static double DegToRad(double deg){return deg*M_PI/180;}
    static double MoaToDeg(double moa){return moa/60;}
    static double MoaToRad(double moa){return moa/60*M_PI/180;}
    static double RadToDeg(double rad){return rad*180/M_PI;}
    static double RadToMoa(double rad){return rad*60*180/M_PI;}
    static double RadToMil(double rad){return rad * 1000; } //* 954.92;}
    static double MilToRad(double mil){return mil / 1000; }// 954.92;}

    enum class Unit {rad, deg, moa, mil};
    void setValue(double a, Unit unit)
    {
        switch (unit)
        {
            case Unit::deg: _a = DegToRad(a); break;
            case Unit::rad: _a = a; break;
            case Unit::moa: _a = MoaToRad(a); break;
            case Unit::mil: _a = MilToRad(a); break;
        }
    }
    double value (Unit unit) const
    {
        switch (unit)
        {
            case Unit::deg: return RadToDeg(_a);
            case Unit::rad: return _a;
            case Unit::moa: return RadToMoa(_a);
            case Unit::mil: return RadToMil(_a);
        }
        return 0;
    }
    double rawValue() const
    {
        return _a;
    }
    void setRawValue(double a)
    {
        _a = a;
    }

    explicit Angle(double a = 0, Unit unit = Unit::rad)
    {
        setValue(a, unit);
    }

    double sin () const
    {
        return gsl_sf_sin(_a);
    }

    double cos () const
    {
        return gsl_sf_cos(_a);
    }

    double tg() const
    {
        return sin() / cos();
    }

    bool operator==(const Angle& a) const
    {
        return gsl_fcmp(_a, a._a, 0.000001) == 0;
    }
    bool operator < (const Angle& a) const
    {
        return gsl_fcmp(_a, a._a, 0.000001) < 0;
    }
    bool operator > (const Angle& a) const
    {
        return gsl_fcmp(_a, a._a, 0.000001) > 0;
    }
    static const char* unitToString(Unit u)
    {
        static const char* moa = "moa";
        static const char* mil = "mil";
        static const char* deg = "deg";
        static const char* rad = "rad";
        switch (u)
        {
            case Unit::moa: return moa;
            case Unit::mil: return mil;
            case Unit::deg: return deg;
            case Unit::rad: return rad;
            default: return "unknown";
        }
    }
    static Unit stringToUnit(const char* str)
    {
        for (int i = 0; i <= static_cast<int>(Unit::mil); ++i)
        {
            Unit u = static_cast<Unit>(i);
            if (strcmp(unitToString(u), str) == 0) return u;
        }
        return static_cast<Unit>(0);
    }
    Angle operator+ (const Angle& a) const
    {
        return Angle(_a + a._a, Angle::Unit::rad);
    }
    Angle& operator+= (const Angle& a)
    {
        _a += a._a;
        return *this;
    }
    Angle operator / (double a) const
    {
        return Angle(_a / a, Angle::Unit::rad);
    }
    Angle& operator /= (double a)
    {
        _a /= a;
        return *this;
    }
    Length toLength(const Length& atDistance) const;
private:
    double _a; // rad
};

class Time
{
public:
    enum class Unit {ms, s, m, h};
    explicit Time(double t=0, Unit unit = Unit::ms)
    {
        setValue(t, unit);
    }

    void setValue (double t, Unit unit)
    {
        switch (unit)
        {
            case Unit::h:  _ms = t * 60 * 60 * 1000; break;
            case Unit::m:  _ms = t * 60 * 1000; break;
            case Unit::s:  _ms = t * 1000; break;
            case Unit::ms: _ms = t; break;
            default: _ms = 1;
        }
    }
    double value(Unit unit) const
    {
        switch (unit)
        {
            case Unit::h:  return _ms / 60 / 60 / 1000;
            case Unit::m:  return _ms / 60 / 1000;
            case Unit::s:  return _ms / 1000;
            case Unit::ms: return _ms;
        }
        return 0;
    }
    void setRawValue(double t)
    {
        _ms = t;
    }
    double rawValue() const
    {
        return _ms;
    }

    Time& operator += (const Time& t)
    {
        _ms += t._ms;
        return *this;
    }
    Time operator *(double a) const
    {
        return Time(a * _ms);
    }
    Time operator + (const Time& other) const
    {
        return Time (_ms + other._ms);
    }
    Time operator - (const Time& other) const
    {
        return Time (_ms - other._ms);
    }
private:
    double _ms; // s
};

class Temperature
{
public:
    enum class Unit {c, f, k};
    explicit Temperature (double t =0, Unit unit=Unit::c)
    {
        setValue(t, unit);
    }
    void setValue(double t, Unit unit)
    {
        switch (unit)
        {
            case Unit::c: _t = t; break;
            case Unit::f: _t = t-32/1.8; break;
            case Unit::k: _t = t - 273.1; break;
        }
    }
    double value(Unit unit) const
    {
        switch (unit)
        {
            case Unit::c: return _t;
            case Unit::f: return _t * 1.8 + 32;
            case Unit::k: return _t + 273.1;
        }
        return 0;
    }
    void setRawValue(double t)
    {
        _t = t;
    }
    double rawValue() const
    {
        return _t;
    }


    bool operator==(const Temperature& t) const
    {
        return gsl_fcmp(_t, t._t, 0.0001) == 0;
    }
    Temperature operator - (const Temperature& other) const
    {
        return Temperature(_t - other._t);
    }
    Temperature operator + (const Temperature& other) const
    {
        return Temperature(_t + other._t);
    }
    double operator / (const Temperature& other) const
    {
        return _t / other._t;
    }
private:
    double _t; // c
};

class Length
{
#define mm_in_in 25.4
#define mm_in_yd 914.4
#define mm_in_ft 304.8
#define mm_in_m 1000
#define mm_in_mi 1609344
#define mm_in_km 1000000
#define mm_in_cm 10

    double _dpm;
    double _l; // mm
public:

    enum class Unit {px, mm, cm, inch, ft, yard, m, km, mi};
    static const char* unitToString(Unit u)
    {
        static const char* px = "px";
        static const char* mm = "mm";
        static const char* cm = "cm";
        static const char* inch = "inch";
        static const char* ft = "ft";
        static const char* yard = "yard";
        static const char* m = "m";
        static const char* km = "km";
        static const char* mi = "mi";
        switch (u)
        {
            case Unit::px: return px;
            case Unit::mm: return mm;
            case Unit::cm: return cm;
            case Unit::inch: return inch;
            case Unit::ft: return ft;
            case Unit::yard: return yard;
            case Unit::m: return m;
            case Unit::km: return km;
            case Unit::mi: return mi;
            default: return "unknown";
        }
    }
    static Unit stringToUnit(const char* str)
    {
        for (int i = 0; i <= static_cast<int>(Unit::mi); ++i)
        {
            Unit u = static_cast<Unit>(i);
            if (strcmp(unitToString(u), str) == 0) return u;
        }
        return static_cast<Unit>(0);
    }
    explicit Length(double rawValue = 0)
        :_dpm(1)
    {
        setRawValue(rawValue);
    }

    explicit Length(double length, Unit u, double dotPerMm=0)
    {
        setValue(length, u, dotPerMm);
    }
    void setValue(double a, Unit u, double dotPerMm=0)
    {
        _dpm = dotPerMm;
        switch(u)
        {
            case Unit::px: _l = a / dpm(); break;
            case Unit::mm: _l = a; break;
            case Unit::cm: _l = a * mm_in_cm; break;
            case Unit::ft: _l = a * mm_in_ft; break;
            case Unit::inch: _l = a * mm_in_in; break;
            case Unit::yard: _l = a * mm_in_yd; break;
            case Unit::m: _l = a * mm_in_m; break;
            case Unit::km: _l = a * mm_in_km; break;
            case Unit::mi: _l = a * mm_in_mi; break;
            default: _l = 1;
        }
    }
    double value(Unit u) const
    {
        switch (u)
        {
            case Unit::px: return _l * dpm();
            case Unit::mm: return _l;
            case Unit::cm: return _l/mm_in_cm;
            case Unit::ft:return _l/mm_in_ft;
            case Unit::inch: return _l/mm_in_in;
            case Unit::yard:return _l/mm_in_yd;
            case Unit::m:return _l/ mm_in_m;
            case Unit::km: return _l / mm_in_km;
            case Unit::mi: return _l / mm_in_mi;
        }
        return 0;
    }
    double rawValue() const
    {
        return _l;
    }
    void setRawValue(double l)
    {
        _l = l;
    }

    void setDpm(double dpm)
    {
        _dpm = dpm;
    }
    double dpm() const
    {
        if (gsl_fcmp(_dpm + 1, 1, .0000000001 ) != 0)
            return _dpm;
        else
            return ::dpm();
    }
    Angle toAngle(const Length& atDistance) const
    {
        return Angle(value(Length::Unit::mm)/atDistance.value(Length::Unit::m), Angle::Unit::mil);
    }
    Length operator+(const Length& other) const
    {
        return Length(_l + other._l);
    }
    Length operator- (const Length& other) const
    {
        return Length(_l - other._l);
    }
    Length& operator += (const Length& other)
    {
        _l += other._l;
        return *this;
    }
    Length& operator -= (const Length& other)
    {
        _l -= other._l;
        return *this;
    }
    double operator / (const Length& other) const
    {
        return _l / other._l;
    }
    Length operator* (double a) const
    {
        return Length(_l * a);
    }
    Length operator / (double a) const
    {
        return Length(_l / a);
    }
    Length operator -() const
    {
        return Length(-_l);
    }

    bool operator == (const Length& other) const
    {
        return gsl_fcmp(_l, other._l, CMP_PRECISION) == 0;
   }
    bool operator != (const Length&  other) const
    {
        return gsl_fcmp(_l, other._l, CMP_PRECISION) != 0;
    }
    bool operator < (const Length& other) const
    {
        return gsl_fcmp(_l, other._l, CMP_PRECISION) < 0;
    }
    bool operator > (const Length& other) const
    {
        return gsl_fcmp(_l, other._l, CMP_PRECISION) > 0;
    }
    bool operator <= (const Length& other) const
    {
        return gsl_fcmp(_l, other._l, CMP_PRECISION) <= 0;
    }
    bool operator >= (const Length& other) const
    {
        return gsl_fcmp(_l, other._l, CMP_PRECISION) >= 0;
    }
};

class Speed
{
public:
    explicit Speed (double v =0, Length::Unit distance=Length::Unit::m, Time::Unit time = Time::Unit::s)
    {
        setValue(v, distance, time);
    }

    void setValue(double v, Length::Unit distance, Time::Unit time)
    {
        Length d (v, distance);
        Time t (1, time);
        _v = d.value(Length::Unit::m) / t.value(Time::Unit::s);
    }

    double value(Length::Unit distance, Time::Unit time ) const
    {
        double coeff = Length(1, distance).value(Length::Unit::m)  / Time(1, time).value(Time::Unit::s);
        return _v / coeff;
    }

    double rawValue() const
    {
        return _v;
    }
    void setRawValue(double v)
    {
        _v = v;
    }

    void setOrts(const Speed& x, const Speed& y)
    {
        setRawValue( gsl_sf_hypot(x.rawValue(), y.rawValue()));
    }

    bool operator == (const Speed& s) const
    {
        return gsl_fcmp(_v, s._v, 0.001) == 0;
    }
    bool operator != (const Speed& s) const
    {
        return gsl_fcmp(_v, s._v, 0.001) != 0;
    }

    Speed operator *(double a) const
    {
        return Speed(_v * a);
    }
    Speed operator - () const
    {
        return Speed(-_v);
    }
    double operator / (const Speed& other) const
    {
        return _v / other._v;
    }
    Speed operator + (const Speed& other) const
    {
        return Speed(_v + other._v);
    }
    Speed& operator += (const Speed& other)
    {
        _v += other._v;
        return *this;
    }
    bool operator < (const Speed& other) const
    {
        return gsl_fcmp(_v, other._v, CMP_PRECISION) < 0;
    }
    bool operator > (const Speed& other) const
    {
        return gsl_fcmp(_v, other._v, CMP_PRECISION) > 0;
    }
    bool operator <= (const Speed& other) const
    {
        return gsl_fcmp(_v, other._v, CMP_PRECISION) <= 0;
    }
    bool operator >= (const Speed& other) const
    {
        return gsl_fcmp(_v, other._v, CMP_PRECISION) >= 0;
    }
private:
    double _v; // m/s
};

class Acceleration
{
    Speed _a; // per s
public:
    explicit Acceleration(const Speed& s = Speed(), Time::Unit time = Time::Unit::s)

    {
        _a.setRawValue(s.rawValue() / Time(1, time).value(Time::Unit::s));
    }
    Speed operator * (const Time& t) const
    {
        return _a * t.value(Time::Unit::s);
    }
    Acceleration operator * (double a) const
    {
        return Acceleration (_a * a);
    }
    Acceleration operator + (const Acceleration& other) const
    {
        return Acceleration(_a + other._a);
    }
};

inline
Length operator* (const Speed& speed, const Time& time)
{
    return Length(speed.value(Length::Unit::m, Time::Unit::s) * time.value(Time::Unit::s), Length::Unit::m);
}

inline
Time operator / (const Length& length, const Speed& speed)
{
    return Time(length.value(Length::Unit::m) / speed.value(Length::Unit::m, Time::Unit::s), Time::Unit::s);
}

class Pressure
{
#define pa_in_hpa 100.0
#define pa_in_kpa 1000.0
#define pa_in_mmhg 133.322368
#define pa_in_inchhg 3386.38816
#define pa_in_atm 101325

public :
    enum class Unit {pa, hpa, kpa, mmhg, inchhg, atm};
    static const char* unitToString(Unit u)
    {
        static const char* pa = "pa";
        static const char* hpa = "hpa";
        static const char* kpa = "kpa";
        static const char* mmhg = "mmhg";
        static const char* inchhg = "inchhg";
        static const char* atm = "atm";
        switch (u)
        {
            case Unit::pa: return pa;
            case Unit::hpa: return hpa;
            case Unit::kpa: return kpa;
            case Unit::mmhg: return mmhg;
            case Unit::inchhg: return inchhg;
            case Unit::atm: return atm;
            default: return "unknown";
        }
    }
    static Unit stringToUnit(const char* str)
    {
        for (int i = 0; i <= static_cast<int>(Unit::atm); ++i)
        {
            Unit u = static_cast<Unit>(i);
            if (strcmp(unitToString(u), str) == 0) return u;
        }
        return static_cast<Unit>(0);
    }

    explicit Pressure (double p=0, Unit unit = Unit::pa)
    {
        setValue(p, unit);
    }

    void setValue(double p, Unit unit = Unit::pa)
    {
        switch (unit)
        {
            case Unit::pa: _p = p; break;
            case Unit::hpa: _p = p * pa_in_hpa; break;
            case Unit::kpa: _p = p * pa_in_kpa; break;
            case Unit::mmhg: _p = p * pa_in_mmhg; break;
            case Unit::inchhg: _p = p * pa_in_inchhg; break;
            case Unit::atm: _p = p * pa_in_atm; break;
        }
    }

    double value (Unit unit=Unit::pa) const
    {
        switch (unit)
        {
            case Unit::pa: return _p;
            case Unit::hpa: return _p / pa_in_hpa;
            case Unit::kpa: return _p / pa_in_kpa;
            case Unit::mmhg: return _p / pa_in_mmhg;
            case Unit::inchhg: return _p / pa_in_inchhg;
            case Unit::atm: return _p / pa_in_atm;
        }
        return 0;
    }

    bool operator==(const Pressure& p) const
    {
        return gsl_fcmp(_p, p._p, 0.00001) == 0;
    }
    Pressure operator*(double a) const
    {
        return Pressure(_p * a, Unit::pa);
    }
    Pressure operator - (const Pressure& other) const
    {
        return Pressure(_p - other._p);
    }
    double operator / (const Pressure& other) const
    {
        return _p / other._p;
    }
private:
    double _p;
};

#endif // UNITS_H

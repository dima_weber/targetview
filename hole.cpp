#include "hole.h"

Hole::Hole()
{

}


Hole::Hole(const QPointF& point)
    :PolarCoordinate(point)
{

}

bool readXmlTag(const QDomElement& holeElement, Hole& hole, const ReadXmlContext& context, bool optional )
{
    PolarCoordinate coord;
    bool ok =readXmlTag(holeElement, coord, context, optional);
    if (ok)
        hole = Hole(coord);
    return ok;
}

bool writeXmlTag(QDomDocument &doc, QDomElement& holeElement,  const Hole& hole)
{
    return writeXmlTag(doc, holeElement, static_cast<PolarCoordinate>(hole));
}

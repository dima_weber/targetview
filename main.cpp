#include "mainwindow.h"
#include <QtWidgets/QApplication>
#include <QtCore/QTranslator>
#include <QtCore/QLibraryInfo>
#include <qdebug.h>
#include <QScreen>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTranslator qtTranslator;
    qtTranslator.load("ru.qm");
    a.installTranslator(&qtTranslator);

    QApplication::addLibraryPath("platfroms");
    QApplication::addLibraryPath(".");

#ifndef Q_OS_LINUX
    a.setWindowIcon(QIcon(":/icon.png"));
#endif
    for (auto screen: qApp->screens())
    {
        qDebug() << "Name: " << screen->name();
        qDebug() << "\tsize" << screen->size();
        qDebug() << "\tdevicePixelRation" << screen->devicePixelRatio();
        qDebug() << "\tlogicalDotPerInch" << screen->logicalDotsPerInch();
        qDebug() << "\tlogicalDotPerInchX" << screen->logicalDotsPerInchX();
        qDebug() << "\tlogicalDotPerInchY" << screen->logicalDotsPerInchY();
        qDebug() << "\tphysicalDotsPerInch" << screen->physicalDotsPerInch();
        qDebug() << "\tphysicalDotsPerInchX" << screen->physicalDotsPerInchX();
        qDebug() << "\tphysicalDotsPerInchY" << screen->physicalDotsPerInchY();
        qDebug() << "\tphysicalSize" << screen->physicalSize();

        QSizeF size = screen->size();
        QSizeF rsize = screen->physicalSize();
        double diag = qSqrt(size.width()*size.width() + size.height()*size.height());
        double rdiag = qSqrt(rsize.width()*rsize.width() + rsize.height()*rsize.height());

        qDebug() << "dpm: " << diag / rdiag;
        qDebug() << "dpi:" << diag * 25.4 / rdiag;
    }

    MainWindow w;
    w.show();

    return a.exec();
}

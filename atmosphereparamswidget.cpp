#include "atmosphereparamswidget.h"
#include "ui_atmosphereparamswidget.h"
#include "ballistics/ballistics.h"

#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QUrlQuery>
#include <QJsonDocument>
#include <QtMath>

AtmosphereParamsWidget::AtmosphereParamsWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::AtmosphereParamsWidget),
	manager(this)
{
	ui->setupUi(this);
	connect (ui->temperatureInput, SIGNAL(valueChanged(double)), SIGNAL(changed()));
	connect (ui->humidityInput, SIGNAL(valueChanged(int)), SIGNAL(changed()));
	connect (ui->pressureInput, SIGNAL(valueChanged(int)), SIGNAL(changed()));
	connect (ui->altitudeInput, SIGNAL(valueChanged(int)), SIGNAL(changed()));
	connect (ui->windDirectionInput, SIGNAL(valueChanged(int)), SIGNAL(changed()));
	connect (ui->windSpeedInput, SIGNAL(valueChanged(int)), SIGNAL(changed()));

	connect (&manager, SIGNAL(finished(QNetworkReply*)), SLOT(onInternetReply(QNetworkReply*)));

	connect (ui->cityInput, &QLineEdit::textChanged, [this](const QString& cityName)
	{
		ui->getFromInternetButton->setEnabled(!cityName.simplified().isEmpty());
	});
}

AtmosphereParamsWidget::~AtmosphereParamsWidget()
{
	delete ui;
}

const AtmosphereParams& AtmosphereParamsWidget::params() const
{
	static AtmosphereParams param;
	param.altitude.setValue(ui->altitudeInput->value(), Length::Unit::m);
	param.humidity = ui->humidityInput->value() / 100.0;
	param.pressure.setValue(ui->pressureInput->value(), Pressure::Unit::hpa);
	param.temperature.setValue( ui->temperatureInput->value(), Temperature::Unit::c);
	param.windAngle.setValue( ui->windDirectionInput->value(), Angle::Unit::deg);
	param.windSpeed.setValue(ui->windSpeedInput->value(), Length::Unit::m, Time::Unit::s);

	return param;
}

void AtmosphereParamsWidget::setParams(const AtmosphereParams& param)
{
	blockSignals(true);
	ui->altitudeInput->setValue(param.altitude.value(Length::Unit::m));
	ui->pressureInput->setValue(param.pressure.value(Pressure::Unit::hpa));
	ui->humidityInput->setValue(param.humidity * 100);
	ui->temperatureInput->setValue(param.temperature.value(Temperature::Unit::c));
	ui->windDirectionInput->setValue(param.windAngle.value(Angle::Unit::deg));
	ui->windSpeedInput->setValue(param.windSpeed.value(Length::Unit::m, Time::Unit::s));
	blockSignals(false);
	//    emit changed();
}

void AtmosphereParamsWidget::onGetFromInternet()
{
	QUrl url("http://api.openweathermap.org/data/2.5/weather");
	QUrlQuery query;
	query.addQueryItem("q", ui->cityInput->text());
	query.addQueryItem("APPID", "a8da34ed6b980aaafe296b86e597e9c6");
	query.addQueryItem("units", "metric");
	url.setQuery(query);
	QNetworkRequest request(url);
	QNetworkReply* pReply = manager.get(request);
	pReply->ignoreSslErrors();
}

void AtmosphereParamsWidget::onInternetReply(QNetworkReply* pReply)
{
	QByteArray answer = pReply->readAll();

	QJsonDocument doc;
	QJsonParseError error;
	doc = QJsonDocument::fromJson(answer, &error);

	QVariantMap jsonMap = doc.toVariant().toMap();
	if (jsonMap.contains("cod"))
	{
		int code = jsonMap["cod"].toInt();
		qDebug() << code;
		if (code == 200)
		{
			blockSignals(true);
			if (jsonMap.contains("main"))
			{
				QVariantMap mainMap = jsonMap["main"].toMap();
				ui->temperatureInput->setValue(mainMap["temp"].toDouble());
				ui->humidityInput->setValue(mainMap["humidity"].toDouble());
				if (mainMap.contains("sea_level"))
				{
					double ground_pressure = mainMap["ground_level"].toDouble();
					double sea_pressure = mainMap["sea_level"].toDouble();
					double h = 18.4 * qLn(sea_pressure/ground_pressure) / qLn(10);
					ui->pressureInput->setValue(sea_pressure);
					ui->altitudeInput->setValue(h);
				}
				else
					ui->pressureInput->setValue(mainMap["pressure"].toDouble());
			}
			if (jsonMap.contains("wind"))
			{
				QVariantMap windMap = jsonMap["wind"].toMap();
				ui->windSpeedInput->setValue(windMap["speed"].toDouble());
				ui->windDirectionInput->setValue(windMap["deg"].toDouble());
			}
			blockSignals(false);
			emit changed();
		}
	}

	pReply->deleteLater();
}

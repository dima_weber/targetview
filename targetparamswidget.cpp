#include "targetparamswidget.h"
#include "ui_targetparamswidget.h"
#include "types.h"

TargetParamsWidget::TargetParamsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TargetParamsWidget)
{
    ui->setupUi(this);
    connect (ui->beginInput, SIGNAL(valueChanged(int)), SIGNAL(changed()));
    connect (ui->endInput, SIGNAL(valueChanged(int)), SIGNAL(changed()));
    connect (ui->stepInput, SIGNAL(valueChanged(int)), SIGNAL(changed()));
    connect (ui->widthInput, SIGNAL(valueChanged(int)), SIGNAL(changed()));
    connect (ui->heightInput, SIGNAL(valueChanged(int)), SIGNAL(changed()));
}

TargetParamsWidget::~TargetParamsWidget()
{
    delete ui;
}

const TargetParams& TargetParamsWidget::params() const
{
    static TargetParams param;
    param.rangeBegin.setValue(ui->beginInput->value(), Length::Unit::m);
    param.rangeEnd.setValue(ui->endInput->value(), Length::Unit::m);
    param.rangeStep.setValue(ui->stepInput->value(), Length::Unit::m);
    param.targetHeight.setValue(ui->heightInput->value(), Length::Unit::cm);
    param.targetWidth.setValue(ui->widthInput->value(), Length::Unit::cm);

    return param;
}

void TargetParamsWidget::setParams(const TargetParams& param)
{
    blockSignals(true);
    ui->endInput->setValue(param.rangeEnd.value(Length::Unit::m));
    ui->beginInput->setValue(param.rangeBegin.value(Length::Unit::m));
    ui->stepInput->setValue(param.rangeStep.value(Length::Unit::m));
    ui->widthInput->setValue(param.targetWidth.value(Length::Unit::cm));
    ui->heightInput->setValue(param.targetHeight.value(Length::Unit::cm));
    blockSignals(false);
//    emit changed();
}

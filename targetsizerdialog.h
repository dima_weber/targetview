#ifndef TARGETSIZERDIALOG_H
#define TARGETSIZERDIALOG_H

#include <QtWidgets/QDialog>

namespace Ui {
class TargetSizerDialog;
}

class TargetSizerDialog : public QDialog
{
	Q_OBJECT

public:
	explicit TargetSizerDialog(QWidget *parent = 0);
	~TargetSizerDialog();

	void setTargets(const QStringList& targets);
	int getFullSettings();
	int getRangeOnly();

	int count() const;
	double realRange() const;
	double minRange() const;
	double maxRange() const;
	double stepRange() const;
	QString targetName() const;
	bool newLine() const;
	bool showOnDisplay() const;
	double scale() const;
	int scopeScale() const;

private:
	Ui::TargetSizerDialog *ui;
};

#endif // TARGETSIZERDIALOG_H

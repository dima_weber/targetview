#include "units_converter.h"
#include <iostream>

double _dpi=72;
const double mm_per_inch = 25.4;

void setDpi(double dpi)
{
    _dpi = dpi;
}

double dpi()
{
    return _dpi;
}

double dpm ()
{
  return dpi() / mm_per_inch;
}




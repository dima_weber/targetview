#ifndef SERIEFILTERWIDGET_H
#define SERIEFILTERWIDGET_H

#include <QtWidgets/QGroupBox>
#include <QDate>

#include "seriecollection.h"

namespace Ui {
class SerieFilterWidget;
}

class SerieFilterWidget : public QGroupBox
{
    Q_OBJECT

public:
    explicit SerieFilterWidget(QWidget *parent = 0);
    ~SerieFilterWidget();

    void setSerieCollection(SerieCollection* collection);

    void setMatchCount(int i);

    void addStringFilter(SerieCollection::FilterType type);
    void addDateFilter();


private:

    Ui::SerieFilterWidget *ui;
    SerieCollection* collection;
};

#endif // SERIEFILTERWIDGET_H

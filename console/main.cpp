#include "ballistics/ballistics.h"
#include "units.h"

#include "optionparser.h"

#include <cstring>
#include <iostream>
#include <iomanip>
#include <algorithm>

//TODO: Use FastCGI

struct Arg: public option::Arg
{
  static void printError(const char* msg1, const option::Option& opt, const char* msg2)
  {
    fprintf(stderr, "%s", msg1);
    fwrite(opt.name, opt.namelen, 1, stderr);
    fprintf(stderr, "%s", msg2);
  }

  static option::ArgStatus Unknown(const option::Option& option, bool msg)
  {
    if (msg) printError("Unknown option '", option, "'\n");
    return option::ARG_ILLEGAL;
  }

  static option::ArgStatus Required(const option::Option& option, bool msg)
  {
    if (option.arg != 0)
      return option::ARG_OK;

    if (msg) printError("Option '", option, "' requires an argument\n");
    return option::ARG_ILLEGAL;
  }

  static option::ArgStatus NonEmpty(const option::Option& option, bool msg)
  {
    if (option.arg != 0 && option.arg[0] != 0)
      return option::ARG_OK;

    if (msg) printError("Option '", option, "' requires a non-empty argument\n");
    return option::ARG_ILLEGAL;
  }

  static option::ArgStatus Numeric(const option::Option& option, bool msg)
  {
    char* endptr = 0;
    if (option.arg != 0 && strtol(option.arg, &endptr, 10)){};
    if (endptr != option.arg && *endptr == 0)
      return option::ARG_OK;

    if (msg) printError("Option '", option, "' requires a numeric argument\n");
    return option::ARG_ILLEGAL;
  }

  static option::ArgStatus Decimal(const option::Option& option, bool msg)
  {
    char* endptr = 0;
    if (option.arg != 0)
    {
        strtod(option.arg, &endptr);
        if (endptr != option.arg && *endptr == 0)
            return option::ARG_OK;
    }

    if (msg) printError("Option '", option, "' requires a numeric argument\n");
    return option::ARG_ILLEGAL;
  }

  static option::ArgStatus DragFunction(const option::Option& option, bool msg)
  {
      if (!option.arg)
          return option::ARG_ILLEGAL;

      if (strlen(option.arg) == 2 && option.arg[0] == 'G' && option.arg[1] >= '1' && option.arg[1] <= '8')
          return option::ARG_OK;

      if (msg) printError("Option '", option, "' requires an argument G1 - G8\n");
      return option::ARG_ILLEGAL;
  }
};

enum optionIndex{Unknown, Help, ImperialMetric, RangeUnit, DropUnit, SightHeightUnit, PressureUnit, RangeTo, RangeFrom, RangeStep, Bc, MuzzleVelocity, ZeroRange,
                 SightHeight, ShootingAngle, DragFunction, WindSpeed, WindAngle, Altitude, Pressure, Temperature, Humidity,
                ClickCost, ClickCostUnit};
enum optionType{Imperial, Metric};

const option::Descriptor usage[] = {
    {Unknown, 0,"","", Arg::Unknown, "Usage: console [options]\n\n"
                                            "Options:"},
    {Help, 0, "", "help", Arg::None, "\t--help \tPrint usage and exit."},
    {ImperialMetric, Imperial, "", "imperial", Arg::None, "\t--imperial"},
    {ImperialMetric, Metric, "", "metric", Arg::None, "\t--metric"},
    {RangeUnit, 0, "", "range-unit", Arg::Required, "\t--range-unit \tDistanse unit [default: m/yd]"},
    {DropUnit, 0, "", "drop-unit", Arg::Required, "\t--drop-unit \tDrop unit [default: cm/in]"},
    {SightHeightUnit, 0, "", "sight-height-unit", Arg::Required, "\t--sight-height-unit \tSight height unit [default: mm/in]"},
    {PressureUnit, 0, "", "pressure-unit", Arg::Required, "\t--pressure-unit \tPressure unit [default: mmhg/inchhg]"},
    {RangeTo, 0, "", "range-to", Arg::Numeric, "\t--range-to \tMaximal distanse to print table to [default: 20000]"},
    {RangeFrom, 0, "", "range-from", Arg::Numeric, "\t--range-from \tMinimal distanse to print table to [default: 25]"},
    {RangeStep, 0, "", "range-step", Arg::Numeric, "\t--range-step \tDistanse increment to print table [default: 25]"},
    {Bc, 0, "", "bc", Arg::Decimal, "\t--bc \tBallistic coefficient\t[default: 0.28]"},
    {MuzzleVelocity, 0, "", "muzzle-velocity", Arg::Numeric, "\t--muzzle-velocity [default 981]"},
    {ZeroRange, 0, "", "zero-range", Arg::Numeric, "\t--zero-range\t[default: 50]"},
    {SightHeight, 0, "", "sight-height", Arg::Decimal, "\t--sight-height\t[default 85]"},
    {ShootingAngle, 0, "", "shooting-angle", Arg::Numeric, "\t--shooting-angle"},
    {DragFunction, 0, "", "drag-function", Arg::DragFunction, "\t--drag-function"},
    {WindSpeed, 0, "", "wind-speed", Arg::Numeric, "\t--wind-speed"},
    {WindAngle, 0, "", "wind-angle", Arg::Numeric, "\t--wind-angle"},
    {Altitude, 0, "", "altitude", Arg::Numeric, "\t--altitude"},
    {Pressure, 0, "", "pressure", Arg::Decimal, "\t--pressure"},
    {Temperature, 0, "", "temperature", Arg::Numeric, "\t--temperature"},
    {Humidity, 0, "", "humidity", Arg::Numeric, "\t--humidity"},
    {ClickCost, 0, "", "click-cost", Arg::Decimal, "\t--click-cost"},
    {ClickCostUnit, 0, "", "click-cost-unit", Arg::Required, "\t--click-cost-unit"},
    {0,0,0,0,0,0}
};

int main(int argc, char** argv)
{
    double delta = 25;
    double maxR = 200000;
    double minR = delta;

    Angle clickCost (0);

    BulletParams bulletParams;
    bulletParams.bc = 0.28;
    bulletParams.muzzleVelocity.setValue(951, Length::Unit::m, Time::Unit::s);
    bulletParams.zero_range.setValue(50, Length::Unit::m);
    bulletParams.sightHeight.setValue(85, Length::Unit::mm);
    bulletParams.shootingAngle.setValue(0, Angle::Unit::deg);
    bulletParams.dragFunction = G1;

    AtmosphereParams atmosphereParams;
    atmosphereParams.windSpeed.setValue(5, Length::Unit::m, Time::Unit::s);
    atmosphereParams.windAngle.setValue(90, Angle::Unit::deg);
    atmosphereParams.altitude.setValue(120, Length::Unit::m);
    atmosphereParams.pressure.setValue(750.1, Pressure::Unit::mmhg);
    atmosphereParams.temperature.setValue(15, Temperature::Unit::c);
    atmosphereParams.humidity = 0.75;

    argc -= (argc>0);
    argv += (argc>0);
    option::Stats stats(usage, argc, argv);
    option::Option options[stats.options_max];
    option::Option buffer[stats.buffer_max];
    option::Parser parser(usage, argc, argv, options, buffer);

    if (parser.error())
        return 1;

    if (options[Help] )
    {
        int columns = getenv("COLUMNS")?atoi(getenv("COLUMNS")) : 80;
        option::printUsage(fwrite, stdout, usage, columns);
        return 0;
    }

    Length::Unit rangeUnits = Length::Unit::m;
    Length::Unit sightHeightUnit = Length::Unit::mm;
    Length::Unit bulletSpeedLengthUnit = Length::Unit::m;
    Length::Unit dropUnit = Length::Unit::cm;
    Length::Unit windSpeedLengthUnit = Length::Unit::m;
    Time::Unit   windSpeedTimeUnit = Time::Unit::s;
    Length::Unit altitudeUnit = Length::Unit::m;
    Temperature::Unit temperatureUnit = Temperature::Unit::c;
    Angle::Unit corrOutput = Angle::Unit::mil;
    Pressure::Unit pressureUnit = Pressure::Unit::mmhg;
    Angle::Unit clickCostUnit = Angle::Unit::mil;

    if (options[ImperialMetric].last()->type() == Imperial)
    {
        rangeUnits = Length::Unit::yard;
        sightHeightUnit = Length::Unit::inch;
        bulletSpeedLengthUnit = Length::Unit::ft;
        dropUnit = Length::Unit::inch;
        altitudeUnit = Length::Unit::ft;
        temperatureUnit = Temperature::Unit::f;
        corrOutput = Angle::Unit::moa;
        clickCostUnit = Angle::Unit::moa;
        pressureUnit = Pressure::Unit::inchhg;
    }

    // parse units first, then values
    for (int i=0; i<parser.optionsCount(); i++)
    {
        option::Option& opt = buffer[i];
        switch (opt.index())
        {
            case RangeUnit: rangeUnits = Length::stringToUnit(opt.arg); break;
            case DropUnit: dropUnit = Length::stringToUnit(opt.arg); break;
            case SightHeightUnit: sightHeightUnit = Length::stringToUnit(opt.arg); break;
            case ClickCostUnit: clickCostUnit = Angle::stringToUnit(opt.arg); break;
            case PressureUnit: pressureUnit = Pressure::stringToUnit(opt.arg); break;
        }
    }

    for (int i=0; i<parser.optionsCount(); i++)
    {
        option::Option& opt = buffer[i];
        switch (opt.index())
        {
            case Help: break;
            case RangeTo: maxR = std::stod(opt.arg); break;
            case RangeFrom: minR = std::stod(opt.arg); break;
            case RangeStep: delta = std::stod(opt.arg); break;
            case Bc: bulletParams.bc = std::stod(opt.arg); break;
            case MuzzleVelocity: bulletParams.muzzleVelocity.setValue(std::stod(opt.arg), bulletSpeedLengthUnit, Time::Unit::s); break;
            case ZeroRange: bulletParams.zero_range.setValue(std::stod(opt.arg), rangeUnits); break;
            case SightHeight: bulletParams.sightHeight.setValue(std::stod(opt.arg), sightHeightUnit); break;
            case ShootingAngle: bulletParams.shootingAngle.setValue(std::stod(opt.arg), Angle::Unit::deg); break;
            case DragFunction: bulletParams.dragFunction = G1 + opt.arg[1] - '1'; break;
            case WindSpeed: atmosphereParams.windSpeed.setValue(std::stod(opt.arg), windSpeedLengthUnit, windSpeedTimeUnit); break;
            case WindAngle: atmosphereParams.windAngle.setValue(std::stod(opt.arg), Angle::Unit::deg); break;
            case Altitude: atmosphereParams.altitude.setValue(std::stod(opt.arg), altitudeUnit); break;
            case Pressure: atmosphereParams.pressure.setValue(std::stod(opt.arg), pressureUnit); break;
            case Temperature: atmosphereParams.temperature.setValue(std::stod(opt.arg), temperatureUnit); break;
            case Humidity: atmosphereParams.humidity = std::stod(opt.arg); break;
            case ClickCost: clickCost.setValue(std::stod(opt.arg), clickCostUnit); break;
        }
    }

    Solution::Ptr solution = std::make_shared<Solution>();
    solution->build(bulletParams, atmosphereParams);

    double m = std::min(maxR, solution->max_range().value(rangeUnits));

    using namespace std;

    const size_t col_width = 9;

    char headerName[20];
    cout << fixed;

    cout << "|";
    cout << setw(col_width) << setfill('-') << "+";
    cout << setw(col_width) << "+";
    if (clickCost.value(clickCostUnit) > 0)
        cout << setw(col_width) << "+";
    cout << setw(col_width) << "|";
    cout << setfill(' ');
    cout << endl;

    cout << "|";
    cout << setw(col_width) << "dist |";
    cout << setw(col_width) << "drop |";
    cout << setw(col_width) << "corr |";
    if (clickCost.value(clickCostUnit) > 0)
        cout << setw(col_width) << "click |";
    cout << endl;

    cout << "|";
    sprintf(headerName, "%s |", Length::unitToString(rangeUnits));
    cout << setw(col_width) << headerName;
    sprintf(headerName, "%s |", Length::unitToString(dropUnit));
    cout << setw(col_width) << headerName;
    sprintf(headerName, "%s |", Angle::unitToString(corrOutput));
    cout << setw(col_width) << headerName;
    if (clickCost.value(clickCostUnit) > 0)
        cout << setw(col_width) << " |";
    cout << endl;

    cout << "|";
    cout << setw(col_width) << setfill('-') << "+";
    cout << setw(col_width) << "+";
    if (clickCost.value(clickCostUnit) > 0)
        cout << setw(col_width) << "+";
    cout << setw(col_width) << "|";
    cout << setfill(' ');
    cout << endl;

    for (double i=minR; i<=m; i+=delta)
    {
        Length range(i, rangeUnits);
        Length drop = solution->drop(range);

        sprintf(headerName, "%.0f |", i);
        cout << "|";
        cout << setw(col_width) << headerName;
        sprintf(headerName, "%.1f |", drop.value(dropUnit));
        cout << setw(col_width) << headerName;
        double corrValue = solution->vertCorr(range).value(corrOutput);
        if (i>0)
            sprintf(headerName, "%.1f |", corrValue);
        else
            strcpy (headerName, " - |");
        cout << setw(col_width) << headerName;
        if (clickCost.value(clickCostUnit) > 0)
        {
            int cnt = round(fabs(corrValue) / clickCost.value(corrOutput));
            if (i>0 && cnt > 0)
                sprintf(headerName, "%s %d |", corrValue<0?"up":"down", cnt);
            else
                strcpy (headerName, " - |");
            cout << setw(col_width) << headerName;
        }

        sprintf(headerName, "%.1f |", solution->x(range).value(Length::Unit::m));
        cout << setw(col_width) << headerName;

        sprintf(headerName, "%.1f |", solution->y(range).value(Length::Unit::cm));
        cout << setw(col_width) << headerName;

        cout << endl;
    }
    cout << "|";
    cout << setw(col_width) << setfill('-') << "+";
    cout << setw(col_width) << "+";
    if (clickCost.value(clickCostUnit) > 0)
        cout << setw(col_width) << "+";
    cout << setw(col_width) << "|";
    cout << setfill(' ');
    cout << endl;

    return 0;
}

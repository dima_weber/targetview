QT -= gui core
TARGET = trajectoryTable
TEMPLATE =app
CONFIG += c++11 silent

INCLUDEPATH += ../ballistics \
                ..

HEADERS += ../units.h \
           ../ballistics/ballistics.h \
           ../cpp-lru-cache/include/lrucache.hpp

SOURCES += ../ballistics/ballistics.cpp \
            main.cpp \
    ../units_converter.cpp

!win32 {
    LIBS += -lgsl -lgslcblas
}

OBJECTS_DIR = .obj
UI_DIR = .ui
MOC_DIR = .moc

DESTDIR = ../bin


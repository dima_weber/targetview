#include "serieseditwidget.h"

#include "serieeditwidget.h"

#include<QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>

#include <QDebug>

SeriesEditWidget::SeriesEditWidget(SerieCollection& collection, QWidget *parent) :
    QWidget(parent),
    collection(collection),
    pSeries(nullptr)
{

    QFormLayout* pTopLayout = new QFormLayout(this);
    pSerieSelect = new QComboBox(this);
    pSerieEdit = new SerieEditWidget(collection, this);
    connect (pSerieSelect, &QComboBox::currentTextChanged, [&](const QString& uuid)
    {
        auto findSerieByUid = [uuid](const SeriePtr& serie) -> bool
        {
            return serie->uuid() == uuid;
        };

        auto serie = std::find_if(pSeries->begin(), pSeries->end(), findSerieByUid);
        if (serie != pSeries->end())
            pSerieEdit->setSerie(*serie);
    });

    pTopLayout->addRow(tr("serie"), pSerieSelect);
    pTopLayout->addRow(nullptr, pSerieEdit);
}

SeriesEditWidget::~SeriesEditWidget()
{
}

void SeriesEditWidget::setSeries(Series* series)
{
    pSeries = series;
    pSerieSelect->clear();
    for(SeriePtr serie: *series)
    {
        pSerieSelect->addItem(serie->uuid());
    }
}

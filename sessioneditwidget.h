#ifndef SESSIONEDITWIDGET_H
#define SESSIONEDITWIDGET_H

#include "session.h"

#include <QWidget>

class QDateEdit;
class QComboBox;
class SerieCollection;
class SeriesEditWidget;

class SessionEditWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SessionEditWidget(SerieCollection& collection, QWidget *parent = 0);
    ~SessionEditWidget();

    void setSession(SessionPtr pSession);

private:
    SerieCollection& collection;
    SessionPtr pSession;

    QDateEdit* pDateEdit;
    QComboBox* pCaliberSelect;
    QComboBox* pTargetSelect;
    QComboBox* pShooterSelect;
    SeriesEditWidget* pSeriesEdit;
};

#endif // SESSIONEDITWIDGET_H

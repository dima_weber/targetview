#ifndef SERIECOLLECTION_H
#define SERIECOLLECTION_H

#include "serie.h"
#include "session.h"

#include <QtCore/QObject>
#include <QtCore/qglobal.h>
#include <QtCore/QVector>
#include <QtCore/QDate>
#include <QtCore/QString>


class SerieCollection : public QObject
{
    Q_OBJECT
public:
    SerieCollection(const Sessions& sessions);

    enum class FilterType {StartDate, EndDate, Position, Scope, Caliber, Target, Firestyle, Range, Shooter};

    static QString filterTypeToString(FilterType type);
    static const QVector<FilterType>& enumerateStringFilters();

    SerieCollection& setFilter(FilterType type, const QString& value = QString());
    QStringList availableValues(FilterType type) const;

    enum class SortOrder {None, Asc, Desc};
    enum class SortCriteria{None, Date, SerieNumber, SerieName};
    struct SortOption
    {
        SortCriteria criteria;
        SortOrder order;
    };

    SerieCollection& setSort(SortCriteria criteria = SortCriteria::None, SortOrder order = SortOrder::Asc);

    QDate minDate() const;
    QDate maxDate() const;

    QMap<QString, QVector<SeriePtr> > filter() const;

    QString getSerieFiled(SeriePtr serie, FilterType type) const;
signals:
    void filterChanged();

private:
    SerieCollection& setStartDateFilter(const QString& date = QString());
    SerieCollection& setEndDateFilter(const QString& date = QString());
    SerieCollection& setStartDateFilter(const QDate& date = QDate());
    SerieCollection& setEndDateFilter(const QDate& date = QDate());


    QVector<SeriePtr> data;
    QDate startDate;
    QDate endDate;
    QMap<FilterType, QString> stringFilters;
    QVector<SortOption> sortOption;
};

#endif // SERIECOLLECTION_H

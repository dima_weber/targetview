#include "recognizeform.h"
#include "ui_recognizeform.h"
#include "types.h"
#include "session.h"
#include "serie.h"

#include <QtWidgets/QGraphicsEllipseItem>
#include <QtWidgets/QGraphicsObject>
#include <QtWidgets/QGraphicsSceneMouseEvent>
#include <QtWidgets/QGraphicsPixmapItem>
#include <QtWidgets/QLabel>
#include <QtWidgets/QBoxLayout>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFileDialog>

#include <QtGui/QKeyEvent>
#include <QtGui/QFocusEvent>
#include <QtGui/QPainter>

#include <QtCore/QDebug>

RecognizeForm::RecognizeForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RecognizeForm),
    pImageScene(new InputScene(this)),
    pRecognizedScene(new QGraphicsScene(this)),
    pBgImage(nullptr),
    pRecognizedGroup(nullptr),
    pRecognizedTarget(nullptr),
    targetLoaded(false),
    lastUsedSerie(0)
{
    ui->setupUi(this);
    setLayout(ui->verticalLayout);

    ui->pImageView->setScene(pImageScene.get());
    ui->pRecognizedView->setScene(pRecognizedScene.get());

    ui->pImageView->setDragMode(QGraphicsView::ScrollHandDrag);
    ui->pImageView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->pImageView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->pImageView->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    ui->pImageView->viewport()->setCursor(Qt::CrossCursor);

    ui->pImageView->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    ui->pImageView->setMinimumSize(600, 600);

    ui->pRecognizedView->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    ui->pRecognizedView->setMinimumSize(600, 600);
    ui->pRecognizedView->setSceneRect(-280,-280, 560, 560);


    pCalibrationNetSelect = new QComboBox(ui->pImageView);
    pCalibrationNetSelect->move(2,2);
    connect (pCalibrationNetSelect, SIGNAL(currentIndexChanged(QString)), SLOT(onLoadCalibrationNet(QString)));

    connect(pImageScene.get(), SIGNAL(doubleClick(QPointF)), SLOT(onImageDoubleClick(QPointF)));

    auto addHudItem = [](QWidget* pHud, QWidget* pItem)
    {
        pItem->setAttribute(Qt::WA_TranslucentBackground, true);
        pItem->setAttribute(Qt::WA_TransparentForMouseEvents, true);
        QFont font = pItem->font();
        font.setPointSize(14);
        font.setBold(true);
        pItem->setFont(font);
        QPalette pal = pItem->palette();
        pal.setColor(QPalette::Text, QColor("white"));
        pItem->setPalette(pal);
        pHud->layout()->addWidget(pItem);
    };

    pHud = new QWidget(ui->pImageView);
    pHud->setVisible(false);
    pHud->setAttribute(Qt::WA_TransparentForMouseEvents, true);
    pHudLayout = new QVBoxLayout(pHud);
    pSerieNumberLabel = new QLabel(pHud);
    pMouseCartesianPositionLabel = new QLabel(pHud);
    pMousePolarPositionLabel = new QLabel(pHud);
    pScaleLabel = new QLabel(pHud);

    addHudItem(pHud, pSerieNumberLabel);
    addHudItem(pHud, pMouseCartesianPositionLabel);
    addHudItem(pHud, pMousePolarPositionLabel);
    addHudItem(pHud, pScaleLabel);

    pScaleLabel->setText("Scale: XXX.XX%");
    pMouseCartesianPositionLabel->setText("X: xxx   Y: yyy");
    pMousePolarPositionLabel->setText("R: rrr A: aaa");
    pSerieNumberLabel->setText("Serie: sss");

    pHud->adjustSize();
    pHud->move(ui->pImageView->width() - pHud->width(),
               ui->pImageView->height() - pHud->height());


    QPushButton* pLoadButton = new QPushButton(tr("Load Image"), ui->pImageView);
    connect (pLoadButton, &QPushButton::clicked, [=]()
    {
        QString fileName = QFileDialog::getOpenFileName(this, tr("Opem image file"), ".", tr("Image Files (*.png *.jpg)"));
        if (!fileName.isEmpty())
        {
            QPixmap pixmap (fileName);
            pBgImage->setPixmap(pixmap);
//            ui->pImageView->update();
        }
    });
    pLoadButton->move(100, 2);

    QPushButton* pRotateButton = new QPushButton(tr("Rotate"), ui->pImageView);
    connect (pRotateButton, &QPushButton::clicked, [=]()
    {
        if (pBgImage)
        {
            pBgImage->setRotation(pBgImage->rotation() + 90);
        }
    });
    pRotateButton->move(200, 2);

    setTargetSource();
}

RecognizeForm::~RecognizeForm()
{
    delete ui;
}

void RecognizeForm::buildSource(QMap<char, QPointF> destPoints)
{
    if (!pBgImage)
    {
        QPixmap pixmap("./image.png");
        pBgImage = pImageScene->addPixmap(pixmap);
        pBgImage->setOffset(-pixmap.width()/2, -pixmap.height()/2);
    }

    int width = pBgImage->pixmap().width();
    int height = pBgImage->pixmap().height();

    for (CalibrationNode* pItem: calibrationNodes)
        delete pItem;

    for (Edge* pEdge: calibrationEdges)
        delete pEdge;

    calibrationNodes.clear();
    calibrationEdges.clear();

    for (char pointName: destPoints.keys())
    {
        QPointF p = destPoints[pointName];
        p.setX(p.x() * width / 500);
        p.setY(p.y() * height / 500);
        CalibrationNode* pItem = new CalibrationNode(p, pointName, this);
        pImageScene->addItem(pItem);
        calibrationNodes[pointName] = pItem;
    }

    for (RecognizeQuad& quad: quads)
    {
        int cnt = quad.points.size();
        for (int i=1; i<= cnt; i++)
        {
            char pointA = quad.points[i % cnt];
            char pointB = quad.points[i - 1];
            Edge* pEdge = new Edge(calibrationNodes[pointA], calibrationNodes [pointB]);
            calibrationEdges << pEdge;
            pImageScene->addItem(pEdge);

            quad.srcPoly << calibrationNodes[pointA]->pos();
        }
    }
}

void RecognizeForm::buildDestination()
{
    if (pRecognizedGroup)
        delete pRecognizedGroup;

    pRecognizedGroup = new QGraphicsItemGroup();
    pRecognizedGroup->setZValue(200);
    pRecognizedScene->addItem(pRecognizedGroup);
}

void RecognizeForm::setTargetSource()
{
    QStringList availTargets = TargetBuilder::ref().availableTargets();
    targetLoaded = false;
    pCalibrationNetSelect->clear();
    pCalibrationNetSelect->addItems(availTargets);
    targetLoaded = true;

    onLoadCalibrationNet(availTargets[0]);
}

void RecognizeForm::onMarkerMove()
{
    // We don't know which mark moved,
    //  so have to recalculate all matrices
    //  and adjust all recognized points
    if (targetLoaded)
    {
        for (RecognizeQuad& quad: quads)
        {
            quad.srcPoly.clear();
            for (char letter: quad.points)
                quad.srcPoly << calibrationNodes[letter]->pos();
            QTransform::quadToQuad(quad.srcPoly, quad.destPoly, quad.transformMatrix);
        }
    }

    adjustRecognizedPositions();
}

void RecognizeForm::onHitMove(HitNode* pImageNode)
{
    for(Hit& hit: hits)
    {
        if (hit.pImageHitNode == pImageNode)
        {
            hit.imagePos = pImageNode->pos();
            adjustRecognizedPositions();
            break;
        }
    }
}

void RecognizeForm::onHitChangeSerie(HitNode* pImageNode, int id)
{
    for(Hit& hit: hits)
    {
        if (hit.pImageHitNode == pImageNode)
        {
            if (id == -2)
                lastUsedSerie = hit.serie+1;
            else if (id == -1)
                lastUsedSerie = hit.serie-1;
            else if (id >= 0)
                lastUsedSerie = id;
            lastUsedSerie = hit.setSerie(lastUsedSerie);
        }
    }
}

void RecognizeForm::onImageDoubleClick(const QPointF& pos)
{
    Hit hit;

    hit.imagePos = pos;
    hit.pImageHitNode = new HitNode(pos);
    hit.pImageHitNode->setForm(this);
    pImageScene->addItem(hit.pImageHitNode);
    hit.pImageHitNode->setScale( calibrationNodes['A']->scale());
    hit.pRecognizedHitNode = addRecognizedHole(pos, hit.recognizedPos);

    hit.pImageHitNode->setFocus();
    hit.setSerie(lastUsedSerie);
    hits.append(hit);
}

void RecognizeForm::onLoadCalibrationNet(const QString &targetName)
{
    if (!targetLoaded)
        return;

    targetLoaded = false;
    currentTargetName = targetName;
    currentTarget = TargetBuilder::ref().target(targetName);
//	if (pRecognizedTarget)
//		pRecognizedScene->removeItem(pRecognizedTarget);

    pRecognizedTarget = currentTarget->pImage.get();
    pRecognizedScene->addItem(pRecognizedTarget);

//    for (Hit& hit: hits)
//    {
//        delete hit.pImageHitNode;
//        delete hit.pRecognizedHitNode;
//    }
//    hits.clear();

    quads.clear();

    for (QString pts: currentTarget->quadPoints)
    {
        RecognizeQuad quad;
        for(int i=0; i<pts.length(); i++)
        {
            char letter = pts[i].toUpper().toLatin1();
            quad.points.append(letter);
            quad.srcPoly << currentTarget->destPoints[letter];
            quad.destPoly << currentTarget->destPoints[letter];
        }
        quads.append(quad);
    }

    buildDestination();
    buildSource(currentTarget->destPoints);

    targetLoaded = true;
    onMarkerMove();

}

void RecognizeForm::onDone()
{
    SessionPtr session = std::make_shared<Session>();
    session->setDate(QDate::currentDate());
    session->setCaliber(Caliber(".223 Rem", .223, Length::Unit::inch));
    session->setTargetName (currentTargetName);
    session->setShooter("me");
    Sessions sess;
    sess[session->name()] = session;

    QMap <QString, SeriePtr> temporaryMap;
    for (Hit& hit: hits)
    {
        QString serieName = QString::number(hit.serie);
        SeriePtr serie;
        if (!temporaryMap.contains(serieName))
        {
            serie =  std::make_shared<Serie>(session);
            serie->position = Serie::Position::Standing;
            serie->scope = Serie::Scope::RedDot;
            serie->setName(serieName);
            serie->range.setValue(10, Length::Unit::m);
            serie->number = hit.serie;
            serie->firestyle = Serie::Firestyle::Slow;
            serie->setColor(hit.pRecognizedHitNode->brush().color());
            temporaryMap[serieName] = serie;
        }
        else
            serie = temporaryMap[serieName];
        QPointF p = hit.recognizedPos;
        Accessor<HolesItem> item;
        item.ref() = Hole(p);
        serie->addHole( item.val());
    }
    for (SeriePtr serie: temporaryMap.values())
        session->addSerie(serie);

    Session::save(sess, "data/file.xml", Session::SaveFormat::Xml);

    accept();
}

HitNode* RecognizeForm::addRecognizedHole(const QPointF& point, QPointF& recognizedPos)
{
    bool recognized = false;
    for (RecognizeQuad& quad: quads)
    {
        if (quad.srcPoly.containsPoint(point, Qt::OddEvenFill))
        {
            recognizedPos = quad.transformMatrix.map(point);
            recognized = true;
            break;
        }
    }
    if (!recognized && quads.size() > 0)
    {
        recognizedPos = quads[0].transformMatrix.map(point);
    }

    return new HitNode(recognizedPos, false, pRecognizedGroup);
}

void RecognizeForm::adjustRecognizedPositions()
{
    if (!hits.isEmpty())
    {
        delete pRecognizedGroup;
        pRecognizedGroup = new QGraphicsItemGroup();
        pRecognizedScene->addItem(pRecognizedGroup);

        for(Hit& hit: hits)
        {
            hit.pRecognizedHitNode = addRecognizedHole(hit.imagePos, hit.recognizedPos);
            hit.setSerie(hit.serie);
        }
    }
}

Edge::Edge(CalibrationNode *sourceNode, CalibrationNode *destNode)
    : arrowSize(10)
{
    setAcceptedMouseButtons(0);
    source = sourceNode;
    dest = destNode;
    source->addEdge(this);
    dest->addEdge(this);
    adjust();
}

CalibrationNode *Edge::sourceNode() const
{
    return source;
}

CalibrationNode *Edge::destNode() const
{
    return dest;
}

void Edge::adjust()
{
    if (!source || !dest)
        return;

    QLineF line(mapFromItem(source, 0, 0), mapFromItem(dest, 0, 0));
    qreal length = line.length();

    prepareGeometryChange();

    if (length > qreal(20.)) {
        QPointF edgeOffset((line.dx() * 10) / length, (line.dy() * 10) / length);
        sourcePoint = line.p1() + edgeOffset;
        destPoint = line.p2() - edgeOffset;
    } else {
        sourcePoint = destPoint = line.p1();
    }
}

QRectF Edge::boundingRect() const
{
    if (!source || !dest)
        return QRectF();

    qreal penWidth = 1;
    qreal extra = (penWidth + arrowSize) / 2.0;

    return QRectF(sourcePoint, QSizeF(destPoint.x() - sourcePoint.x(),
                                      destPoint.y() - sourcePoint.y()))
        .normalized()
        .adjusted(-extra, -extra, extra, extra);
}

void Edge::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    if (!source || !dest)
        return;

    QLineF line(sourcePoint, destPoint);
    if (qFuzzyCompare(line.length(), qreal(0.)))
        return;

    // Draw the line itself
    painter->setPen(QPen(Qt::blue, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    painter->drawLine(line);
}

CalibrationNode::CalibrationNode(char label, RecognizeForm* form)
    :QGraphicsEllipseItem( ), pForm(form)
{
    QPen pen(QBrush("darkgreen"), 2);
    QBrush brush("green");

    setPen(pen);
    setBrush(brush);
    int radius = 8;
    setRect(-radius, -radius, 2*radius, 2*radius);
    setFlag(ItemIsMovable);
    setFlag(ItemSendsGeometryChanges);
    setCacheMode(DeviceCoordinateCache);
    setFlag(QGraphicsItem::ItemIgnoresTransformations, true);

    QGraphicsSimpleTextItem* pLabel = new QGraphicsSimpleTextItem(QString("%1").arg(label), this);
    QFont font = pLabel->font();
    font.setPixelSize(14);
    pLabel->setFont(font);
    pLabel->setPen(QPen("white"));
    pLabel->setBrush(QBrush("white"));
    QRectF textRect = pLabel->boundingRect();
    pLabel->moveBy(-textRect.width()/2, -textRect.height()/2);

    setCursor(Qt::SizeAllCursor);
}

CalibrationNode::CalibrationNode(const QPointF& coord, char label, RecognizeForm* form)
    :CalibrationNode(label, form)
{
    moveBy(coord.x(), coord.y());
}

void CalibrationNode::addEdge(Edge* edge)
{
    edgeList << edge;
    edge->adjust();
}

QVariant CalibrationNode::itemChange(GraphicsItemChange change, const QVariant &value)
{
    switch (change) {
    case ItemPositionHasChanged:
        for (Edge* edge: edgeList)
            edge->adjust();
        pForm->onMarkerMove();
        break;
    default:
        break;
    };

    return QGraphicsItem::itemChange(change, value);
}

InputScene::InputScene(QObject* parent)
    :QGraphicsScene(parent)
{

}

void InputScene::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* mouseEvent)
{
    QGraphicsScene::mouseDoubleClickEvent(mouseEvent);
    emit doubleClick(mouseEvent->scenePos());
}


HitNode::HitNode(const QPointF& point, bool movable, QGraphicsItem* parent)
    :QGraphicsEllipseItem( parent), pForm(nullptr)
{
    setColor(1);
    QPen pen(QBrush("white"), 2);
    setPen(pen);
    int holeRadius = 5;
    setRect(-holeRadius, -holeRadius, 2*holeRadius, 2*holeRadius);
    if (movable)
    {
        setFlag(ItemIsFocusable);
        setFlag(ItemIsMovable);
        setFlag(ItemSendsGeometryChanges);
        setCacheMode(DeviceCoordinateCache);
        setFlag(QGraphicsItem::ItemIgnoresTransformations, true);
    }

    moveBy(point.x(), point.y());

    QGraphicsItemGroup* pGrp = dynamic_cast<QGraphicsItemGroup*>(parent);
    if (pGrp)
    {
        pGrp->addToGroup(this);
    }
}

void HitNode::setColor(QColor color)
{
    setBrush(QBrush(color));
}

void HitNode::setForm(RecognizeForm* p)
{
    pForm = p;
}

void HitNode::keyPressEvent(QKeyEvent* event)
{
    if (pForm)
    {
        if (event->key() >= Qt::Key_0 && event->key() <= Qt::Key_9)
            pForm->onHitChangeSerie(this, int(event->key() - Qt::Key_0));
        if (event->key() >= Qt::Key_A && event->key() <= Qt::Key_F)
            pForm->onHitChangeSerie(this, int(event->key() - Qt::Key_A + 10));
        if (event->key() == Qt::Key_Minus)
            pForm->onHitChangeSerie(this, -1);
        if (event->key() == Qt::Key_Plus)
            pForm->onHitChangeSerie(this, -2);
    }
    QGraphicsEllipseItem::keyPressEvent(event);
}

void HitNode::focusInEvent(QFocusEvent* event)
{
    QPen p = pen();
    p.setColor("yellow");
    setPen(p);
    QGraphicsEllipseItem::focusInEvent(event);
}

void HitNode::focusOutEvent(QFocusEvent* event)
{
    QPen p = pen();
    p.setColor("white");
    setPen(p);
    QGraphicsEllipseItem::focusOutEvent(event);
}

QVariant HitNode::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant& value)
{
    switch (change) {
    case ItemPositionHasChanged:
        if (pForm)
            pForm->onHitMove(this);
        break;
    default:
        break;
    };

    return QGraphicsEllipseItem::itemChange(change, value);
}

void HitNode::contextMenuEvent(QGraphicsSceneContextMenuEvent* event)
{
    if (pForm)
        pForm->onHitChangeSerie(this);
    QGraphicsEllipseItem::contextMenuEvent(event);
}

int Hit::setSerie(int id)
{
    if (id >= 0  && id <= 16)
    {
        serie = id;
        QColor serieColor = getSerieColor(id);

        pImageHitNode->setColor(serieColor);
        pRecognizedHitNode->setColor(serieColor);
    }
    return serie;
}

#ifndef TARGETBUILDER_H
#define TARGETBUILDER_H

#include <QString>
#include <QDomDocument>
#include <QMap>

#include "target.h"

class TargetBuilder
{
    QString xmlFileName;
    QDomDocument xmlDocument;
    QMap<QString, QDomElement>  targetElements;
    TargetBuilder();
    bool _isLoaded;
public:
    bool loadXml(const QString& fileName);
    bool loaded() const;
    static TargetBuilder& ref();
    QStringList availableTargets();
    TargetPtr target(const QString& name, double scale = 1, QColor color = QColor());
};

#endif // TARGETBUILDER_H

#ifndef RECOGNIZEFORM_H
#define RECOGNIZEFORM_H

#include "targetbuilder.h"

#include <QtWidgets/QDialog>
#include <QtWidgets/QGraphicsEllipseItem>
#include <QtWidgets/QGraphicsItem>
#include <QtWidgets/QGraphicsScene>
#include <QtCore/QList>

#include <memory>

namespace Ui {
class RecognizeForm;
}

class Edge;
class CalibrationNode;
class RecognizeForm;
class QGraphicsSceneContextMenuEvent;
class QKeyEvent;
class QFocusEvent;
class QBoxLayout;
class QLabel;
class QComboBox;

class CalibrationNode : public QGraphicsEllipseItem
{
public:
	CalibrationNode(char label, RecognizeForm* form );
	CalibrationNode(const QPointF& coord, char label, RecognizeForm* form);

	void addEdge(Edge* edge);

	enum { Type = UserType + 1 };
	int type() const Q_DECL_OVERRIDE { return Type; }

protected:
	virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value) Q_DECL_OVERRIDE;

private:
	QList<Edge*> edgeList;
	QPointF newPos;
	RecognizeForm* pForm;
};


class HitNode : public QGraphicsEllipseItem
{
	int colorId;
	RecognizeForm* pForm;
public:
	HitNode(const QPointF& point, bool movable = true, QGraphicsItem* parent = nullptr);
	void setColor(QColor color);
	void setForm(RecognizeForm* p);

protected:
	virtual void keyPressEvent(QKeyEvent* event);
	virtual void focusInEvent(QFocusEvent* event);
	virtual void focusOutEvent(QFocusEvent* event);
	QVariant itemChange(GraphicsItemChange change, const QVariant &value) Q_DECL_OVERRIDE;
	virtual void contextMenuEvent(QGraphicsSceneContextMenuEvent* event);
};

class Edge : public QGraphicsItem
{
public:
	Edge(CalibrationNode *sourceNode, CalibrationNode *destNode);

	CalibrationNode *sourceNode() const;
	CalibrationNode *destNode() const;

	void adjust();

	enum { Type = UserType + 2 };
	int type() const Q_DECL_OVERRIDE { return Type; }

protected:
	QRectF boundingRect() const Q_DECL_OVERRIDE;
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;

private:
	CalibrationNode *source, *dest;

	QPointF sourcePoint;
	QPointF destPoint;
	qreal arrowSize;
};


class InputScene: public QGraphicsScene
{
	Q_OBJECT
public:
	explicit InputScene( QObject* parent =0);
	virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent* mouseEvent);
signals:
	void doubleClick(const QPointF&);
};

struct Hit
{
	HitNode* pImageHitNode;
	HitNode* pRecognizedHitNode;
	QPointF  recognizedPos;
	QPointF  imagePos;
	int serie;

	int setSerie(int id);
};

class RecognizeForm : public QDialog
{
	Q_OBJECT

public:
	explicit RecognizeForm(QWidget *parent = 0);
	~RecognizeForm();

public slots:
	void onMarkerMove();
	void onHitMove(HitNode*pImageNode);
	void onHitChangeSerie(HitNode*, int id=0);
	void onImageDoubleClick(const QPointF& pos);
	void onLoadCalibrationNet(const QString& targetName);

	void onDone();
private:
	Ui::RecognizeForm *ui;
	std::unique_ptr<InputScene> pImageScene;
	std::unique_ptr<QGraphicsScene> pRecognizedScene;

	QWidget* pHud;
	QLabel* pSerieNumberLabel;
	QLabel* pMouseCartesianPositionLabel;
	QLabel* pMousePolarPositionLabel;
	QLabel* pScaleLabel;
	QComboBox* pCalibrationNetSelect;
	QBoxLayout* pHudLayout;
	QGraphicsPixmapItem* pBgImage;

	QVector<Hit> hits;
	QGraphicsItemGroup* pRecognizedGroup;
	QGraphicsItemGroup* pRecognizedTarget;

	HitNode* addRecognizedHole(const QPointF&, QPointF& recognizedPos );
	void adjustRecognizedPositions();

	QMap<char, CalibrationNode*> calibrationNodes;
	QVector<Edge*> calibrationEdges;

	void buildSource(QMap<char, QPointF> destPoints);
	void buildDestination();
	void setTargetSource();

	struct RecognizeQuad
	{
		QVector<char> points;
		QPolygonF srcPoly;
		QPolygonF destPoly;
		QTransform transformMatrix;
	};

	QVector<RecognizeQuad> quads;

	bool targetLoaded;
	int lastUsedSerie;
	QString currentTargetName;
	TargetPtr currentTarget;
};

#endif // RECOGNIZEFORM_H

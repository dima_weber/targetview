#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "parser.h"
#include "serie.h"
#include "session.h"
#include "types.h"
#include "guiserie.h"
#include "target.h"

#include <QtWidgets/QMainWindow>
#include <QtCore/QVector>
#include <QtGui/QColor>
#include <QtCore/QtMath>

#include <QtCore/QDate>
#include <QtGui/QStandardItem>
#include <QtWidgets/QPushButton>

#include <memory>

namespace Ui {
class MainWindow;
}

class QGraphicsScene;
class QGraphicsView;
class QBoxLayout;
class QCheckBox;
class QRadioButton;
class QButtonGroup;
class QTreeView;
class QFileSystemModel;
class QFileSystemWatcher;
class QStandardItemModel;
class QGroupBox;
class QSortFilterProxyModel;
class QPrinter;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    const QSizeF targetSize = QSizeF(500,500);
    const QSizeF targetHeadSize = QSizeF(230,180);
    const QSizeF targetShoulderSize = QSizeF(135, 320);
    const QPointF targetCenter = QPointF(250, 250);
    const int TargetCircleRadiusStep = 50;

    QGraphicsItemGroup* drawReticle(QGraphicsScene* pScene, const Length& real_range, double reticleScale, bool showGrid = false, bool showTails = false);
    void multiPagePrint(QGraphicsScene* pScene, QPrinter& printer);

protected slots:
    void drawTarget(const QString& targetName = QString());
    void buidProblematicZonesGroup(const Target& target);
    void onSessionViewClick(const QModelIndex& index);
    void onSessionViewDoubleClick(const QModelIndex& index);

public slots:
    bool onLoad(const QString& filePath = QStringLiteral("data/Holes.csv"));
    void onSerieChange(Serie* pSession);
    void onSerieItemChange(QStandardItem* pItem);

    bool onUnload();
    bool loadTargets(const QString& filePath = QStringLiteral("data/targets.xml"));

    void onBoundingRect();
    void onMaxDistance();
    void onCenter();
    void onCircles();

    void onRecognize();
    void onAnalyze();
    void onEdit();
    void onTargetSize();
    void onCalibration();
    void onCalculator();
    void onAnimation();
    void onPerspective();

signals:
    void targetsUpdated();

private:
    Ui::MainWindow *ui;
    Sessions sessions; // TODO: make it singletone
    Serie* pCurrentSerie;
    QGraphicsScene* pScene;
    QGraphicsView* pView;
    TargetPtr pCurrentTarget;
    QGraphicsItemGroup* pTargetGroup;
    QGraphicsItemGroup* pProblematicZonesGroup;
    QPushButton* pLoadButton;
    QWidget* pCommandPanel;
    QGroupBox* pSeriesSettings;
    QPushButton* pSaveImageButton;
    QPushButton* pShowZonesButton;
    QBoxLayout* pCommandPanelLayout;
    QPushButton* pUnloadButton;
    QButtonGroup* pSeriesButtonGroup;
    QCheckBox* pEnableMultiSelect;
    QCheckBox* pShowRect;
    QCheckBox* pShowMaxDist;
    QCheckBox* pShowCenter;
    QRadioButton* pMilRadCircles;
    QRadioButton* pMoaCircles;
    QRadioButton* pNoneCirlces;
    QFileSystemModel* pFsModel;
    QTreeView* pFsView;

    QString targetsXmlPath;
    QFileSystemWatcher* pTargetXmlWatcher;

    QStandardItemModel* pSessionModel;
    QSortFilterProxyModel* pSortSessionModel;
    QTreeView* pSessionsView;

    QMap<QString, GuiSeriePtr> uuidGuiMap;
};

class SerieItem : public QStandardItem
{
public:
    GuiSerieWPtr wpSerie;
    SerieItem(GuiSeriePtr pSerie, const QString& title, bool session = false)
        :QStandardItem(title), wpSerie(pSerie), session(session)
    {}
    bool session;
    void toggle()
    {
        GuiSeriePtr spSerie = wpSerie.lock();
        if (spSerie)
            spSerie->pButton->toggle();
    }
};
#endif // MAINWINDOW_H

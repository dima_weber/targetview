#include "targetsizerdialog.h"
#include "ui_targetsizerdialog.h"

TargetSizerDialog::TargetSizerDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::TargetSizerDialog)
{
	ui->setupUi(this);

	setLayout(ui->verticalLayout);
}

TargetSizerDialog::~TargetSizerDialog()
{
	delete ui;
}

void TargetSizerDialog::setTargets(const QStringList& targets)
{
	ui->targetNameInput->clear();
	ui->targetNameInput->addItems(targets);
}

int TargetSizerDialog::getFullSettings()
{
	ui->maxRangeInput->setVisible(true);
	ui->minRangeInput->setVisible(true);
	ui->stepRangeInput->setVisible(true);
	ui->targetNameInput->setVisible(true);
	ui->countInput->setVisible(true);
	ui->newLineInput->setVisible(true);
	ui->label_2->setVisible(true);
	ui->label_3->setVisible(true);
	ui->label_4->setVisible(true);
	ui->label_5->setVisible(true);
	ui->label_6->setVisible(true);
	return exec();
}

int TargetSizerDialog::getRangeOnly()
{
	ui->maxRangeInput->setVisible(false);
	ui->minRangeInput->setVisible(false);
	ui->stepRangeInput->setVisible(false);
	ui->targetNameInput->setVisible(false);
	ui->countInput->setVisible(false);
	ui->newLineInput->setVisible(false);
	ui->label_2->setVisible(false);
	ui->label_3->setVisible(false);
	ui->label_4->setVisible(false);
	ui->label_5->setVisible(false);
	ui->label_6->setVisible(false);
	return exec();
}

int TargetSizerDialog::count() const
{
	return ui->countInput->value();
}

double TargetSizerDialog::realRange() const
{
	return ui->realRangeInput->value();
}

double TargetSizerDialog::minRange() const
{
	return ui->minRangeInput->value();
}

double TargetSizerDialog::maxRange() const
{
	return ui->maxRangeInput->value();
}

double TargetSizerDialog::stepRange() const
{
	return ui->stepRangeInput->value();
}

QString TargetSizerDialog::targetName() const
{
	return ui->targetNameInput->currentText();
}

bool TargetSizerDialog::newLine() const
{
	return ui->newLineInput->isChecked();
}

bool TargetSizerDialog::showOnDisplay() const
{
	return ui->showOnDisplay->isChecked();
}

double TargetSizerDialog::scale() const
{
	double reticleScale = ui->reticleScaleInput->value();
	double scopeScale = ui->scopeScaleInput->value();
	return reticleScale / scopeScale;
}

int TargetSizerDialog::scopeScale() const
{
	return ui->scopeScaleInput->value();
}

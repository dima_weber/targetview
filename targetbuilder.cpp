#include "targetbuilder.h"
#include "xmlhelper.h"

#include <QDebug>
#include <QFile>

TargetBuilder::TargetBuilder()
    :_isLoaded(false)
{
}

bool TargetBuilder::loadXml(const QString& fileName)
{
    xmlFileName = fileName;
    QFile file(fileName);
    bool ok;
    ok = file.open(QFile::ReadOnly);

    if (!ok)
    {
        qWarning() << "fail to open file" << fileName;
        return false;
    }

    QString errMsg;
    int errLine;
    int errCol;
    ok = xmlDocument.setContent(&file, &errMsg, &errLine, &errCol);
    if (!ok)
    {
        qWarning() << "Fail to parse XML: " << errMsg << errLine << errCol;
        return false;
    }

    QDomElement root = xmlDocument.documentElement();
    ok = root.tagName() == "targetshow";
    if (!ok)
    {
        qWarning() << "not targetshow xml file";
        return false;
    }

    QDomElement targetsElement = root.firstChildElement("targets");
    ok = !targetsElement.isNull();

    if (!ok)
    {
        qWarning() << "no targets found";
        return false;
    }

    ok = false;
    QDomNodeList targetList = targetsElement.elementsByTagName("target");
    for (int i=0; i<targetList.count(); i++)
    {
        QDomElement targetElement = targetList.at(i).toElement();
        if (targetElement.isNull())
        {
            qWarning() << "broken target element";
        }
        else
        {
            QDomElement nameElement = targetElement.firstChildElement("name");
            QString targetName = "unnamed";
            if (nameElement.isNull())
                qWarning() << "no target name could be found. set to 'unnamed'";
            else
                targetName = nameElement.text();

            if (targetElements.contains(targetName))
                qWarning() << "target" << targetName << "already exists. Will be owerwritten";

            targetElements[targetName] = targetElement;
            ok |=  true;
        }
    }
    _isLoaded = true;
    return ok;
}

bool TargetBuilder::loaded() const
{
    return _isLoaded;
}

TargetBuilder& TargetBuilder::ref()
{
    static TargetBuilder builder;
    return builder;
}

QStringList TargetBuilder::availableTargets()
{
    return targetElements.keys();
}

TargetPtr TargetBuilder::target(const QString& name, double scale, QColor color)
{
    TargetPtr spTarget  = std::make_shared<Target>();
    bool ok = targetElements.contains(name);
    if (ok)
    {
        ReadXmlContext context;
        context.scale = scale;
        context.color = color;
        context.set_threaded(true);
        ok = readXmlTag(targetElements[name], *spTarget, context, false);
    }
    if (!ok)
        spTarget = std::make_shared<DefaultTarget>();
    return spTarget;
}

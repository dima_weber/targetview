#include "seriesortfiltermodel.h"
#include <QStandardItem>
#include "mainwindow.h"

SerieSortFilterModel::SerieSortFilterModel(QObject* parent)
	:QSortFilterProxyModel(parent)
{

}

bool SerieSortFilterModel::lessThan(const QModelIndex& source_left, const QModelIndex& source_right) const
{
	QStandardItemModel* pSourceModel = dynamic_cast<QStandardItemModel*>(sourceModel());
	if (!pSourceModel)
		return false;

	QStandardItem* pLeftItem = pSourceModel->itemFromIndex(source_left);
	QStandardItem* pRightItem = pSourceModel->itemFromIndex(source_right);

	SerieItem* pLeftSerieItem = dynamic_cast<SerieItem*>(pLeftItem);
	SerieItem* pRightSerieItem = dynamic_cast<SerieItem*>(pRightItem);

	if (pLeftSerieItem && pRightSerieItem)
	{
		SeriePtr pSerieLeft = pLeftSerieItem->wpSerie.lock()->serie().lock();
		SeriePtr pSerieRight = pRightSerieItem->wpSerie.lock()->serie().lock();
		return (pSerieLeft->date() < pSerieRight->date())
				|| (pSerieLeft->date() == pSerieRight->date() && pSerieLeft->number > pSerieRight->number);
	}
	return false;
}

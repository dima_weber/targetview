#include "seriecollection.h"
#include "session.h"

#include <QtConcurrent/QtConcurrentFilter>

SerieCollection::SerieCollection(const Sessions& sessions)
{
    for (SessionsItem session: sessions.values())
        for (SeriesItem serie: session->series)
            data.append(serie);
}

QString SerieCollection::filterTypeToString(SerieCollection::FilterType type)
{
    switch (type)
    {
        case SerieCollection::FilterType::Caliber: return tr("Caliber");
        case SerieCollection::FilterType::Firestyle: return tr("Firestyle");
        case SerieCollection::FilterType::Position: return tr("Position");
        case SerieCollection::FilterType::Range: return tr("Range");
        case SerieCollection::FilterType::Scope: return tr("Scope");
        case SerieCollection::FilterType::Shooter: return tr("Shooter");
        case SerieCollection::FilterType::Target: return tr("Target");
        case SerieCollection::FilterType::StartDate: return tr("Start date");
        case SerieCollection::FilterType::EndDate: return tr("End date");
        default: return QString();
    }

}

const QVector<SerieCollection::FilterType>& SerieCollection::enumerateStringFilters()
{
    static const QVector<FilterType> stringFilters = {
                    FilterType::Position,
                    FilterType::Scope,
                    FilterType::Caliber,
                    FilterType::Target,
                    FilterType::Firestyle,
                    FilterType::Range,
                    FilterType::Shooter
    };
    return stringFilters;
}

QString SerieCollection::getSerieFiled(SeriePtr serie, SerieCollection::FilterType type) const
{
    switch (type)
    {
        case FilterType::Target: return serie->targetName();
        case FilterType::Caliber: return serie->caliber().name();
        case FilterType::Position: return Serie::positionToString(serie->position);
        case FilterType::Scope: return Serie::scopeToString(serie->scope);
        case FilterType::Firestyle: return Serie::firestyleToString(serie->firestyle);
        case FilterType::Range: return QString::number(serie->range.value(Length::Unit::m));
        case FilterType::Shooter: return serie->shooter();
        default: return QString();
    }
}

QStringList SerieCollection::availableValues(SerieCollection::FilterType type) const
{
    QStringList lst;
    QString str;
    for(SeriePtr serie: data)
    {
        str = getSerieFiled(serie, type);
        if (!lst.contains(str))
            lst << str;
    }
    return lst;
}

SerieCollection& SerieCollection::setStartDateFilter(const QString& date)
{
    if (!date.isEmpty())
    {
        startDate = QDate::fromString(date);
    }
    else
        startDate = QDate();
    return *this;
}

SerieCollection& SerieCollection::setEndDateFilter(const QString& date)
{
    if (!date.isEmpty())
    {
        endDate = QDate::fromString(date);
    }
    else
        endDate = QDate();
    return *this;
}

SerieCollection&SerieCollection::setStartDateFilter(const QDate& date)
{
    startDate = date;
    return *this;
}

SerieCollection&SerieCollection::setEndDateFilter(const QDate& date)
{
    endDate = date;
    return *this;
}

SerieCollection& SerieCollection::setFilter(SerieCollection::FilterType type, const QString& value)
{
    if (SerieCollection::enumerateStringFilters().contains(type))
        stringFilters[type] = value;
    else if (type == FilterType::StartDate)
        setStartDateFilter(value);
    else if (type == FilterType::EndDate)
        setEndDateFilter(value);

    emit filterChanged();
    return *this;
}

SerieCollection& SerieCollection::setSort(SerieCollection::SortCriteria criteria, SerieCollection::SortOrder order)
{
    if (criteria == SortCriteria::None)
        sortOption.clear();
    else
    {
        SortOption opt;
        opt.criteria = criteria;
        opt.order = order;

        sortOption << opt;
    }
    return *this;
}

QDate SerieCollection::minDate() const
{
    QDate date = QDate::currentDate();
    for(SeriePtr serie: data)
        date = qMin(serie->date(), date);
    return date;
}

QDate SerieCollection::maxDate() const
{
    QDate date = QDate();
    for(SeriePtr serie: data)
        date = qMax(serie->date(), date);
    return date;
}


QMap<QString, QVector<SeriePtr>> SerieCollection::filter() const
{
    QMap<QString, QVector<SeriePtr>> filtered;

    auto checkStringFilter = [=](FilterType type, const QString& compareValue) -> bool
    {
        QString v = stringFilters[type];
        return     v.isEmpty()
                || v == "any"
                || v == "split"
                || v == compareValue;
    };

    auto filterFunc = [=](SeriePtr serie) -> bool
    {
        bool retValue = true;
        for(FilterType type: enumerateStringFilters())
        {
            retValue &= checkStringFilter(type, getSerieFiled(serie, type));
            if (!retValue)
                break;
        }
        return   retValue
              && (endDate.isNull() || serie->date() <= endDate)
              && (startDate.isNull() || startDate <= serie->date())
                ;
    };

    auto res = QtConcurrent::blockingFiltered(data, filterFunc);

    auto compare = [=](const SeriePtr& a, const SeriePtr& b) -> bool
    {
        for (SortOption opt: sortOption)
        {
            switch(opt.criteria)
            {
                case SortCriteria::Date:
                    if (opt.order == SortOrder::Asc)
		    {
                        if (a->date() < b->date())
                            return true;
                        if (a->date() > b->date())
                            return false;
		    }
		    else
		    {
			if (a->date() < b->date())
				return false;
			if (a->date() > b-> date())
				return true;
		    }
                    break;
                case SortCriteria::SerieName:
                    if (a->name() < b->name())
                        return true;
                    if (a->name() > b->name())
                        return false;
                    break;
                case SortCriteria::SerieNumber:
                    if (a->number < b->number)
                        return true;
                    if (a->number > b->number)
                        return false;
                    break;
                case SortCriteria::None:
                    break;
            }
        }
        return false;
    };

    if (!sortOption.isEmpty())
        std::sort(res.begin(), res.end(), compare);


    filtered["all"] = res;
    for (FilterType type: enumerateStringFilters())
    {
        if (stringFilters[type] == "split")
        {
            QMap<QString, QVector<SeriePtr>> res;
            for (QString splitName: filtered.keys())
            {
                for (const SeriePtr& serie: filtered[splitName])
                {
                    QString v;
                    v = getSerieFiled(serie, type);
                    res[splitName + "_" + filterTypeToString(type) + ":" + v].append(serie);
                }
            }
            filtered = res;
        }
    }
    if (!filtered.contains("all"))
            filtered["all"] = res;
    return filtered;
}


#include "seriefilterwidget.h"
#include "ui_seriefilterwidget.h"
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>

SerieFilterWidget::SerieFilterWidget(QWidget *parent) :
    QGroupBox(parent),
    ui(new Ui::SerieFilterWidget),
    collection(nullptr)
{
    ui->setupUi(this);
    setLayout(ui->formLayout);
}

SerieFilterWidget::~SerieFilterWidget()
{
    delete ui;
}

void SerieFilterWidget::setSerieCollection(SerieCollection* collection)
{
    this->collection = collection;

    addDateFilter();

    for (SerieCollection::FilterType type : SerieCollection::enumerateStringFilters())
    {
        addStringFilter(type);
    }
}

void SerieFilterWidget::setMatchCount(int i)
{
    ui->pFilterMatchCount->setText(QString::number(i));
}

void SerieFilterWidget::addStringFilter(SerieCollection::FilterType type)
{
    QString label = SerieCollection::filterTypeToString(type);
    QLabel* pLabel = new QLabel(label, this);
    QComboBox* pCombo = new QComboBox(this);
    ui->formLayout->addRow(pLabel, pCombo);

    if (type > SerieCollection::FilterType::EndDate)
    {
        pCombo->clear();
        pCombo->addItem("any");
        pCombo->addItem("split");
        pCombo->insertSeparator(2);
        QStringList lst = collection->availableValues(type);
        pCombo->addItems(lst);
        int size = lst.size();
        if (size <= 1)
        {
            pCombo->setEnabled(false);
            pCombo->setCurrentIndex(pCombo->count()-1);
            pLabel->setEnabled(false);
        }

        auto onCombo = [=](int /*index*/)
        {
            collection->setFilter(type, pCombo->currentText());
        };

        connect (pCombo, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), onCombo);
    }
}

void SerieFilterWidget::addDateFilter()
{
    QString minLabel = SerieCollection::filterTypeToString(SerieCollection::FilterType::StartDate);
    QString maxLabel = SerieCollection::filterTypeToString(SerieCollection::FilterType::EndDate);

    QDate minDate = collection->minDate();
    QDate maxDate = collection->maxDate();

    auto initDateEdit = [=](SerieCollection::FilterType type, const QString& label, const QDate& value)
    {
        QLabel* pLabel = new QLabel(label, this);
        QDateEdit* pDateEdit = new QDateEdit(this);
        ui->formLayout->addRow(pLabel, pDateEdit);
        pDateEdit->setCalendarPopup(true);

        pDateEdit->setDateRange( minDate, maxDate);

        if (minDate == maxDate)
        {
            pDateEdit->setEnabled(false);
            pDateEdit->setEnabled(false);

        }

        pDateEdit->setDate(value);

        connect (pDateEdit, &QDateEdit::dateChanged, [=](const QDate& date)
        {
            collection->setFilter(type, date.toString());
        });
    };

    initDateEdit(SerieCollection::FilterType::StartDate, minLabel, minDate);
    initDateEdit(SerieCollection::FilterType::EndDate, maxLabel, maxDate);
}


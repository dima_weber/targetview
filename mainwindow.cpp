#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "targetview.h"
#include "xmlparser.h"
#include "recognizeform.h"
#include "analyzeform.h"
#include "seriecollection.h"
#include "opacitywidget.h"
#include "sessionseditwidget.h"
#include "guiserie.h"
#include "targetbuilder.h"
#include "seriesortfiltermodel.h"
#include "targetsizerdialog.h"
#include "bulletpropertieswidget.h"
#include "atmosphereparamswidget.h"
#include "targetparamswidget.h"
#include "trajectorywidget.h"
#include "projectiondialog.h"
#include "units_converter.h"

#include "qcustomplot/qcustomplot.h"

#include <QtCore/QFile>
#include <QtCore/QRegExp>
#include <QtCore/QFileSystemWatcher>

//#include <QtOpenGL/QGLWidget>

#include <QtGui/QWheelEvent>
#include <QtGui/QStandardItemModel>
#include <QtGui/QImage>
#include <QGraphicsOpacityEffect>

#include <QtWidgets/QFileDialog>
#include <QtWidgets/QGraphicsScene>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QBoxLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QFileSystemModel>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QTableView>
#include <QtWidgets/QFrame>

#include <QPrinter>
#include <QPrintDialog>
#include <QPropertyAnimation>
#include <QScreen>

QColor getSerieColor(int num)
{
   static
   QVector<QColor> serieColors = {	0xb21f35,
                                    0xD82735,
                                    0xFF7435,
                                    0xFFA135,
                                    0xFFCB35,
                                    0xFFF735,
                                    0x00753a,
                                    0x009e47,
                                    0x16dd36,
                                    0x0052a5,
                                    0x0079e7,
                                    0x06a9fc,
                                    0x681e7e,
                                    0x7d3cb5,
                                    0xbd7af6};
    return serieColors[(num * 4) % serieColors.size()];
}

double square_dist(const QPointF& a, const QPointF& b)
{
    return gsl_pow_2(a.x()-b.x()) + gsl_pow_2(a.y()-b.y());
}

double dist(const QPointF &a, const QPointF &b)
{
    return gsl_sf_hypot(a.x()-b.x(), a.y()-b.y());
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    pCurrentSerie(nullptr),
    pTargetGroup(nullptr),
    pProblematicZonesGroup(nullptr)
{
    pTargetXmlWatcher = new QFileSystemWatcher(this);
    connect (pTargetXmlWatcher, SIGNAL(fileChanged(QString)), SLOT(loadTargets(QString)));

    pScene = new QGraphicsScene(this);

    connect(this, SIGNAL(targetsUpdated()), SLOT(drawTarget()));

    ui->setupUi(this);

    QWidget* pWidget = centralWidget();
    QSplitter* pSplit = new QSplitter(pWidget);

    QBoxLayout* pLayout = new QHBoxLayout(pWidget);
    pLayout->addWidget(pSplit);

    pView = new TargetView(pSplit);
    pCommandPanel = new QWidget(pSplit);

    pSplit->addWidget(pView);
    pSplit->addWidget(pCommandPanel);
    pSplit->setHandleWidth(4);
    pSplit->setCollapsible(1, true);
    pSplit->setCollapsible(0, false);

//    pView->setViewport(new QGLWidget(pWidget));
    //pView->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    pView->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    pView->setMinimumSize(510, 510);

    pCommandPanelLayout = new QVBoxLayout();
    pCommandPanel->setLayout(pCommandPanelLayout);

    pView->setScene(pScene);
    pView->setDragMode(QGraphicsView::ScrollHandDrag);
    pView->setInteractive(true);
    pView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    pView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    pView->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);

    pFsModel = new QFileSystemModel(this);
    pFsModel->setRootPath(QDir::currentPath());
//    pFsModel->setNameFilters(QStringList() << "*.csv" << "*.xml");

    pFsView = new QTreeView(pCommandPanel);
    pCommandPanelLayout->addWidget(pFsView);
    pFsView->setModel(pFsModel);
    pFsView->setRootIndex(pFsModel->index(QDir::current().filePath(QStringLiteral("data"))));
    connect (pFsView, &QTreeView::clicked, [=](const QModelIndex& index)
    {
       onLoad(pFsModel->filePath(index));
    });
    pFsView->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);

    pLoadButton = new QPushButton(pCommandPanel);
    pLoadButton->setText(tr("Load"));
    connect (pLoadButton, SIGNAL(released()), SLOT(onLoad()));
    pCommandPanelLayout->addWidget(pLoadButton);

    pLoadButton->hide();
    ui->actionLoad->setVisible(false);

    pSeriesButtonGroup = new QButtonGroup(pCommandPanel);

    pUnloadButton = new QPushButton(pCommandPanel);
    pUnloadButton->setText(tr("Unload"));
    connect(pUnloadButton, SIGNAL(released()), SLOT(onUnload()));
    pCommandPanelLayout->addWidget(pUnloadButton);

    pSeriesSettings = new QGroupBox(tr("Settings"), pCommandPanel);
    QVBoxLayout* pSettingsLayout = new QVBoxLayout();
    pSeriesSettings->setLayout(pSettingsLayout);

    pEnableMultiSelect = new QCheckBox(tr("Enable multi select"), pSeriesSettings);
    connect (pEnableMultiSelect, &QCheckBox::clicked, [=](bool on)
    {
        pSeriesButtonGroup->setExclusive(!on);
    });

    pShowRect = new QCheckBox(tr("Show bounding rect"), pSeriesSettings);
    connect (pShowRect, SIGNAL(released()), SLOT(onBoundingRect()));

    pShowCenter = new QCheckBox(tr("Show center"), pSeriesSettings);
    connect(pShowCenter, SIGNAL(released()), SLOT(onCenter()));

    pShowMaxDist = new QCheckBox(tr("Show max distance"), pSeriesSettings);
    connect(pShowMaxDist, SIGNAL(released()), SLOT(onMaxDistance()));

    pMilRadCircles = new QRadioButton(tr("MilRad circles"), pSeriesSettings);
    connect(pMilRadCircles, SIGNAL(released()), SLOT(onCircles()));

    pMoaCircles = new QRadioButton(tr("MOA circles"), pSeriesSettings);
    connect(pMoaCircles, SIGNAL(released()), SLOT(onCircles()));

    pNoneCirlces = new QRadioButton(tr("no circles"), pSeriesSettings);
    connect(pNoneCirlces, SIGNAL(released()), SLOT(onCircles()));

    pSettingsLayout->addWidget(pEnableMultiSelect);

    pSettingsLayout->addWidget(pShowRect);
    pSettingsLayout->addWidget(pShowCenter);
    pSettingsLayout->addWidget(pShowMaxDist);

    pSettingsLayout->addWidget(pMilRadCircles);
    pSettingsLayout->addWidget(pMoaCircles);
    pSettingsLayout->addWidget(pNoneCirlces);

    pNoneCirlces->setChecked(true);

    pSessionModel = new QStandardItemModel(this);
    pSortSessionModel = new SerieSortFilterModel(this);
    pSortSessionModel->setSourceModel(pSessionModel);
    pSessionsView = new QTreeView(pCommandPanel);
    pCommandPanelLayout->addWidget(pSessionsView);
    pSessionsView->setModel(pSortSessionModel);
    pSessionsView->header()->setSectionResizeMode(QHeaderView::Stretch);
    pSessionsView->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);
    pSessionsView->setMinimumHeight(100);
    //pSessionsView->setSortingEnabled(true);
    pSortSessionModel->sort(1, Qt::DescendingOrder);

    connect (pSessionsView, SIGNAL(clicked(QModelIndex)), SLOT(onSessionViewClick(QModelIndex)));
    connect (pSessionModel, SIGNAL(itemChanged(QStandardItem*)), SLOT(onSerieItemChange(QStandardItem*)));
    connect (pSessionsView, SIGNAL(doubleClicked(QModelIndex)), SLOT(onSessionViewDoubleClick(QModelIndex)));

    //pCommandPanelLayout->addStretch();
    pCommandPanelLayout->addWidget(pSeriesSettings);

    //pCommandPanelLayout->addStretch();
    pSeriesButtonGroup->setExclusive(true);

    QBoxLayout* pHudLayout = new QVBoxLayout(pView);
    pView->setLayout(pHudLayout);
    QBoxLayout* pHudButtonsLayout = new QHBoxLayout(pView);
    pHudLayout->addLayout(pHudButtonsLayout);
    pHudLayout->addStretch();

    pSaveImageButton = new OpacityWidget(0.2, 0.9, tr("Save"), pView);
    connect (pSaveImageButton, &QPushButton::clicked, [=](bool)
    {
        QImage image(1500, 1500, QImage::Format_ARGB32);
        QPainter painter(&image);
        painter.setRenderHint(QPainter::Antialiasing);
        pScene->render(&painter);
        QString proposedFileName = QString(QStringLiteral("data/shots-%1.png")).arg(QDateTime::currentDateTime().toString(QStringLiteral("yyyyMMddhhmmss")));
        QString fileName = QFileDialog::getSaveFileName(pWidget, tr("Save image"),
                                                        QDir::current().filePath(proposedFileName), tr("PNG Image (*.png)") );
        if (!fileName.isEmpty())
            image.save(fileName);
    });

    pShowZonesButton = new OpacityWidget(0.2, 0.9, tr("Show problematic zones"), pView);
    pShowZonesButton->setCheckable(true);
    connect (pShowZonesButton, &QPushButton::toggled, [=](bool on)
    {
        if (pProblematicZonesGroup)
            pProblematicZonesGroup->setVisible(on);
    });

    pHudButtonsLayout->addWidget(pSaveImageButton);
    pHudButtonsLayout->addStretch();
    pHudButtonsLayout->addWidget(pShowZonesButton);

    onUnload();
    loadTargets();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::drawTarget(const QString& targetName)
{
//    if (pTargetGroup)
//    {
//        pScene->removeItem(pTargetGroup);
//    }

    pCurrentTarget = TargetBuilder::ref().target(targetName);
    if (pCurrentTarget)
    {
        pTargetGroup = pCurrentTarget->pImage.get();
        pScene->setSceneRect(pTargetGroup->boundingRect());
        pView->setSceneRect( pTargetGroup->boundingRect() );

        buidProblematicZonesGroup(*pCurrentTarget);

        pTargetGroup->setZValue(-1);
        pScene->addItem(pTargetGroup);
    }

    //pView->fitInView(pTargetGroup, Qt::KeepAspectRatio);
}

void MainWindow::buidProblematicZonesGroup(const Target& /*target*/)
{
    bool visible = false;
    if (pProblematicZonesGroup)
    {
        visible = pProblematicZonesGroup->isVisible();
        delete pProblematicZonesGroup;
    }

    pProblematicZonesGroup = new QGraphicsItemGroup();

    int okZoneRadius = 100;
    int totalZoneRadius = okZoneRadius * 2.5;

    QPen pen (QBrush("red"), 2);
    QBrush brush(QColor(145, 45, 0, 100));
    QRectF ellipseRect;
    QGraphicsEllipseItem* pEllipse = nullptr;
    QVector<int> angles = {25, 65, 110, 155, 200, 225, 250, 290, 325, 385};
    QVector<QString> problemDescription =
    {
        tr("Ok"),
        tr("Heeling (anticipating recoil)"),
        tr("Breaking wrist up"),
        tr("Pushing (anticipating recoil) or No follow through"),
        tr("Too little trigger finger"),
        tr("Tightening fingers"),
        tr("Jerking or Slapping trigger"),
        tr("Breaking wrist down, Pushing forward or Drooping head"),
        tr("Tightening grip while pulling trigger"),
        tr("Thumbing (squeezing thumb) or Too much trigger finger")
    };

    QPainterPath path;

    // ok zone
    QBrush okBrush(QColor(45, 145, 0, 100));
    QPainterPath okZonePath;
    ellipseRect.setRect(-okZoneRadius,-okZoneRadius, 2 * okZoneRadius, 2*okZoneRadius);
    pEllipse = new QGraphicsEllipseItem(ellipseRect, pProblematicZonesGroup);
    pEllipse->setPen(pen);
    pEllipse->setBrush(okBrush);
    pEllipse->setToolTip(problemDescription[0]);
    okZonePath.addEllipse(ellipseRect);

    ellipseRect.setRect(-totalZoneRadius, -totalZoneRadius, 2 * totalZoneRadius, 2*totalZoneRadius);

    for (int i=1; i<angles.size(); i++)
    {
        QPainterPath arcPath;
        arcPath.arcTo(ellipseRect, angles[i-1], angles[i] - angles[i-1]);
        path =arcPath.subtracted(okZonePath);

        QGraphicsPathItem* pPath = new QGraphicsPathItem(path, pProblematicZonesGroup);
        pPath->setPen(pen);
        pPath->setBrush(brush);

        pPath->setToolTip(problemDescription[i]);
    }

    pProblematicZonesGroup->setVisible(visible);
    pScene->addItem(pProblematicZonesGroup);
}

void MainWindow::onSessionViewClick(const QModelIndex& index)
{
    QModelIndex origIdx = pSortSessionModel->mapToSource(index);
    QStandardItem* pItem = pSessionModel->itemFromIndex(origIdx);
    SerieItem* pSerieItem = dynamic_cast<SerieItem*>(pItem);
    if (pSerieItem)
    {
        pSerieItem->toggle();
        onSerieChange(pSerieItem->wpSerie.lock()->serie().lock().get());
    }
    else
    {
        qDebug() << "invalid item clicked";
    }
}

void MainWindow::onSessionViewDoubleClick(const QModelIndex& index)
{
    Q_UNUSED(index);
}

bool MainWindow::loadTargets(const QString &filePath)
{
    bool ok;
    ok = TargetBuilder::ref().loadXml(filePath);
    if (ok)
    {
        if (filePath != targetsXmlPath)
        {
            if (!targetsXmlPath.isEmpty())
                pTargetXmlWatcher->removePath(targetsXmlPath);
            targetsXmlPath = filePath;
            pTargetXmlWatcher->addPath(filePath);
        }
        emit targetsUpdated();
    }
    else
        qWarning() << "fail to parse targets";
    return ok;
}

bool MainWindow::onLoad(const QString &filePath)
{
    QElapsedTimer timer;
    timer.start();
    if (!sessions.isEmpty())
        if (!onUnload())
            return false;

    bool ok;

    XmlSessionReader xmlReader(sessions);

    ok = xmlReader.read(filePath);
    if (!ok)
    {
        QMessageBox::warning(this, QStringLiteral("Fail to open"), QStringLiteral("Fail to open TargetShow xml file"), QMessageBox::Ok);
        qWarning() << "Fail to open file " << filePath;
        return false;
    }

    if (sessions.isEmpty())
        return false;

    pSessionModel->setColumnCount(5);
    pSessionModel->setHorizontalHeaderLabels(QStringList()
        << QStringLiteral("name")
        << QStringLiteral("total")
        << QStringLiteral("count")
        << QStringLiteral("mean")
        << QStringLiteral("sd") );
    QStandardItem* pRootItem = pSessionModel->invisibleRootItem();
    QWidget* parentWidget = centralWidget();

    auto addItemToList = [this, parentWidget](SeriePtr p, const QString& title, bool isSession, QStandardItem* parent, SerieItem*& pSessionItem) -> GuiSeriePtr
    {
        GuiSeriePtr spGui = std::make_shared<GuiSerie>(*pScene, p, parentWidget);
        uuidGuiMap[p->uuid()] = spGui;

        pSessionItem = new SerieItem(spGui, title, isSession);
        QStandardItem* pShotValueItem	= new SerieItem(spGui, QString::number(p->scoreTotal()));
        QStandardItem* pShotCountItem	= new SerieItem(spGui, QString::number(p->holesCount()));
        QStandardItem* pMeanValueItem   = new SerieItem(spGui, QString::number(p->scoreAvg(), 'f', 1));
        QStandardItem* pSdItem      	= new SerieItem(spGui, QString::number(p->scoreSd(), 'f', 1));

        QList<QStandardItem*> columns;
        columns << pSessionItem << pShotValueItem << pShotCountItem << pMeanValueItem << pSdItem;
        parent->appendRow(columns);

        return spGui;
    };

    // TODO: redo tree view population on demand: prevent greating gui elements / calculate statistics for items that won't be  shown ever
    for(SessionsItem session: sessions)
    {
        SerieItem* pSessionItem = nullptr;
        GuiSeriePtr spGuiSession = addItemToList(session, QStringLiteral("%2 (%1)").arg(session->uuid(), session->date().toString()), true, pRootItem, pSessionItem);
        for (SeriesItem serie: session->series)
        {
            SerieItem* pSerieItem;
            GuiSeriePtr spGuiSerie = addItemToList(serie, serie->name(), false, pSessionItem, pSerieItem);
            spGuiSession->addGuiSerie(spGuiSerie);
            pSerieItem->setCheckable(true);
            pSerieItem->setToolTip( serie->info());
            pCommandPanelLayout->addWidget(spGuiSerie->pButton);
            pSeriesButtonGroup->addButton(spGuiSerie->pButton);
        }
        spGuiSession->pButton->setVisible(false);
        spGuiSession->setVisible(false);
    }
    pSessionsView->header()->setSectionResizeMode(0, QHeaderView::ResizeToContents);

    if (sessions.size() > 0)
        onSerieChange(sessions.values()[0].get());

    pLoadButton->setEnabled(false);
    ui->actionLoad->setEnabled(false);

    pUnloadButton->setVisible(true);
    ui->actionClose->setEnabled(true);

    ui->actionAnalyze->setEnabled(true);
    ui->actionEdit->setEnabled(true);

    pFsView->setVisible(false);
    pSessionsView->setVisible(true);

    pSaveImageButton->setVisible(true);
    pShowZonesButton->setVisible(true);

    pSeriesSettings->setVisible(true);

    qDebug() << "load xml in " << timer.elapsed() << "ms";

    return true;
}

void MainWindow::onSerieChange(Serie* pSerie)
{
    // prevent any action when already selected session clicked
    if (pSerie == pCurrentSerie)
        return;

    // hide previously selected session if any
    if (pCurrentSerie)
    {
        uuidGuiMap[pCurrentSerie->uuid()]->setVisible(false);
    }

    pCurrentSerie = pSerie;
    drawTarget(pCurrentSerie->targetName());
    uuidGuiMap[pCurrentSerie->uuid()]->setVisible(true);

    onBoundingRect();
    onMaxDistance();
    onCircles();
    onCenter();
}

void MainWindow::onSerieItemChange(QStandardItem* pItem)
{
    SerieItem* pSerieItem = dynamic_cast<SerieItem*>(pItem);
    if (pSerieItem)
    {
        if (!pSerieItem->session)
        {
            SerieItem* pSessionItem = dynamic_cast<SerieItem*>(pSerieItem->parent());
            if (pSessionItem)
            {
                Session* pSession = dynamic_cast<Session*>(pSessionItem->wpSerie.lock()->serie().lock().get());
                if (pSession)
                {
                    if (pItem->checkState() == Qt::Checked)
                        pSession->selectSerie(pSerieItem->wpSerie.lock()->serie().lock());
                    else if (pItem->checkState() == Qt::Unchecked)
                        pSession->unselectSerie(pSerieItem->wpSerie.lock()->serie().lock());

                }
            }
        }
    }
}

bool MainWindow::onUnload()
{

    sessions.clear();
    pSessionModel->clear();
    uuidGuiMap.clear();

    pCurrentSerie = nullptr;

    drawTarget(QStringLiteral("panda"));

    for(QAbstractButton* pButton: pSeriesButtonGroup->buttons())
    {
        pSeriesButtonGroup->removeButton(pButton);
        delete pButton;
    }

    pProblematicZonesGroup->setVisible(false);

    pLoadButton->setEnabled(true);
    pUnloadButton->setVisible(false);
    pSaveImageButton->setVisible(false);
    pShowZonesButton->setVisible(false);
    ui->actionClose->setEnabled(false);
    ui->actionLoad->setEnabled(true);
    ui->actionAnalyze->setEnabled(false);
    ui->actionEdit->setEnabled(false);
    pSeriesSettings->setVisible(false);

    pFsView->setVisible(true);
    pSessionsView->setVisible(false);

    dynamic_cast<TargetView*>(pView)->originalScale();
    return true;
}

void MainWindow::onBoundingRect()
{
    for (GuiSeriePtr spGui: uuidGuiMap.values())
        spGui->showBoundingRect(pShowRect->isChecked());
}

void MainWindow::onMaxDistance()
{
    for (GuiSeriePtr spGui: uuidGuiMap.values())
        spGui->showMaxDistance(pShowMaxDist->isChecked());
}

void MainWindow::onCenter()
{
    for (GuiSeriePtr spGui: uuidGuiMap.values())
        spGui->showMassCenter(pShowCenter->isChecked());
}

void MainWindow::onCircles()
{
    GuiSerie::CircleType t;
    if (pMoaCircles->isChecked())
        t = GuiSerie::CircleType::MOA;
    else if (pMilRadCircles->isChecked())
        t = GuiSerie::CircleType::MilRad;
    else
        t = GuiSerie::CircleType::None;

    for (GuiSeriePtr spGui: uuidGuiMap.values())
        spGui->showCircle(t);
}

void MainWindow::onRecognize()
{
    RecognizeForm* pForm = new RecognizeForm(this);
    pForm->exec();
    pForm->deleteLater();
}

void MainWindow::onAnalyze()
{
    SerieCollection collect(sessions);
    AnalyzeForm* pForm = new AnalyzeForm(collect, this);
    pForm->exec();
    delete pForm;
}

void MainWindow::onEdit()
{
    QDialog* pDialog = new QDialog(this);
    QBoxLayout* pLayout = new QVBoxLayout(pDialog);
    SessionsEditWidget* pWidget = new SessionsEditWidget(sessions, pDialog);
    pLayout->addWidget(pWidget);
    QPushButton* pOk = new QPushButton(tr("Save"), pDialog);
    pLayout->addWidget(pOk);
    connect (pOk, SIGNAL(clicked(bool)), pDialog, SLOT(accept()));
    if (pDialog->exec() == QDialog::Accepted)
    {
        Session::save(sessions, QStringLiteral("data/save.xml"), Session::SaveFormat::Xml);
    }
    delete pDialog;
    // pDialog->deleteLater();
}

void MainWindow::onTargetSize()
{
    QWidget* pWidget = this;

    QString targetName = QStringLiteral("ipsc");
    bool everySizeOwnLine = true;
    int repeatCnt = 1;
    double real_range = 50; // m
    double min_range_m = 50;
    double max_range_m = 300;
    double range_step_m = 50;

    TargetSizerDialog inputParams(pWidget);
    inputParams.setTargets(TargetBuilder::ref().availableTargets());
    if (inputParams.getFullSettings() == QDialog::Rejected)
        return;

    repeatCnt = inputParams.count();
    everySizeOwnLine = inputParams.newLine();
    real_range = inputParams.realRange();
    min_range_m = inputParams.minRange();
    max_range_m = inputParams.maxRange();
    range_step_m = inputParams.stepRange();
    targetName = inputParams.targetName();

    QGraphicsScene* pScene = new QGraphicsScene(pWidget);

    QPrinter printer(QPrinter::HighResolution);
    printer.setPageSize(QPrinter::A4);
    printer.setFullPage(false);
    printer.setOrientation(QPrinter::Landscape);
    setDpi(qApp->primaryScreen()->physicalDotsPerInch());
    if (!inputParams.showOnDisplay())
    {
        QPrintDialog dialog(&printer, pWidget);
        dialog.exec();
        setDpi(printer.resolution());
        printer.setColorMode(QPrinter::GrayScale);
    }
    QRectF pageRect = printer.pageRect(QPrinter::DevicePixel);

//	pScene->setSceneRect(0, 0, pageRect.width(), pageRect.height());

    double y = 10;
    double x = 10;
    double y_inc = 0;

    QRectF realRect = TargetBuilder::ref().target(targetName)->pImage->boundingRect();
    double targetHeight_mm = realRect.height();

    QFont font;
    font.setBold(true);
    font.setPixelSize(Length(6).value(Length::Unit::px));

    for (double range_m = min_range_m; range_m<=max_range_m; range_m+=range_step_m)
    {
        double scale = real_range / range_m * dpm();
        QRectF targetRect;
        for (int repeat = 0; repeat < repeatCnt; repeat++)
        {
            TargetPtr target = TargetBuilder::ref().target(targetName, scale);
            target->bw();
            QGraphicsItemGroup* pGroup = new QGraphicsItemGroup();
            pScene->addItem(pGroup);
            QGraphicsItemGroup* pTargetImg = target->release();
            targetRect = pTargetImg->boundingRect();
            pGroup->addToGroup(pTargetImg);

            QGraphicsTextItem* pText = pScene->addText(QString::number(range_m), font);
//			pText->setPen(QColor("black"));
            pGroup->addToGroup(pText);
            QRectF textRect = pText->boundingRect();
            pText->setPos(-textRect.width()/2, targetRect.height()/2 +4 );

            QRectF grpRect = pGroup->boundingRect();
            grpRect.setHeight(grpRect.height() + textRect.height() + 4);
            grpRect.setWidth(qMax(grpRect.width(), textRect.width()));

//			pGroup->addToGroup(pScene->addRect(grpRect, QPen(Qt::DashLine)));

            double old_x = x;
            x += grpRect.width() + 20;
            if ( x > pageRect.width() )
            {
                old_x = 10;
                x = 10 + grpRect.width()+20;
                y += y_inc;
                y_inc = 0;
            }
            pGroup->setPos(old_x + grpRect.width()/2,y + grpRect.height()/2);
            y_inc = qMax(y_inc, grpRect.height() + 10);
        }
        if (everySizeOwnLine)
        {
            x = 10;
            y += y_inc;
            y_inc = 0;
        }

        qDebug() << targetHeight_mm << " mm on range " << range_m << "m looks like " << targetRect.height() << "mm on " << real_range << "m range";
    }

    QGraphicsTextItem* pText = pScene->addText(tr("Put this target on %1 m range\n"
                                                   "Target name: %2")
                                           .arg(real_range)
                                           .arg(targetName), font);
    pText->setPos(pageRect.width() - pText->boundingRect().width() - 10,
                  pageRect.height() - pText->boundingRect().height() - 10);

    if (inputParams.showOnDisplay())
    {
        pWidget = new QWidget(nullptr);
        QBoxLayout* pLayout = new QHBoxLayout;
        pWidget->setLayout(pLayout);
        QGraphicsView* pView = new QGraphicsView(pWidget);
        pLayout->addWidget(pView);
        pView->setScene(pScene);
        pView->setRenderHint(QPainter::Antialiasing, true);
        pView->setRenderHint(QPainter::TextAntialiasing, true);
        pWidget->show();
    }
    else
    {
        multiPagePrint(pScene, printer);
    }
}


QGraphicsItemGroup* MainWindow::drawReticle(QGraphicsScene* pScene, const Length& real_range, double reticleScale, bool showGrid, bool showTails)
{
    QGraphicsItemGroup* pGroup = new QGraphicsItemGroup();

    int mil_dot_count = 14;

    auto mil_to_pixel_scaled = [&real_range, reticleScale](double mil) -> double
    {
        return Angle(mil, Angle::Unit::mil).toLength(real_range).value(Length::Unit::px) * reticleScale;
    };

    double lineWidth = mil_to_pixel_scaled(0.05);
    QColor reticleColor = QColor("blue");
    QColor calibrationColor = QColor("blue");
    calibrationColor.setAlphaF(0.3);
    QPen reticlePen(reticleColor, lineWidth);
    QPen calibrationPen(calibrationColor, lineWidth / 3);
    QBrush reticleBrush(reticleColor, Qt::SolidPattern);
    QPointF A(0, - mil_to_pixel_scaled(mil_dot_count/2));
    QPointF B(0, mil_to_pixel_scaled(mil_dot_count/2));
    QPointF C( - mil_to_pixel_scaled(mil_dot_count/2), 0);
    QPointF D( mil_to_pixel_scaled(mil_dot_count/2), 0);

    QGraphicsLineItem* pLine = nullptr;
    QGraphicsEllipseItem* pEllipse = nullptr;
    QGraphicsRectItem* pRect = nullptr;

    pLine = pScene->addLine(QLineF(C,D), reticlePen);
    pGroup->addToGroup( pLine );

    pLine = pScene->addLine(QLineF(A,B), reticlePen);
    pGroup->addToGroup(pLine);

    double dotRadius_px = mil_to_pixel_scaled(0.1);
    for (int i=-(mil_dot_count/2-1); i<=(mil_dot_count/2-1); i++)
    {
        if (i==0)
        {
            continue;
        }

        double circle_x =  - dotRadius_px;
        double circle_y = mil_to_pixel_scaled(i) - dotRadius_px;
        QRectF cirlceRect(circle_x, circle_y, 2*dotRadius_px, 2*dotRadius_px);

        pEllipse = pScene->addEllipse(cirlceRect, reticlePen, reticleBrush);
        pGroup->addToGroup(pEllipse);

        pLine = pScene->addLine(circle_x-dotRadius_px, circle_y+dotRadius_px, circle_x+3*dotRadius_px, circle_y+dotRadius_px, reticlePen);
        pGroup->addToGroup(pLine);

        if (showGrid)
        {
            pLine = pScene->addLine(C.x(), circle_y+dotRadius_px, D.x(), circle_y+dotRadius_px, calibrationPen);
            pGroup->addToGroup(pLine);
        }

        circle_x = mil_to_pixel_scaled(i) - dotRadius_px;
        circle_y =  - dotRadius_px;
        cirlceRect.setRect(circle_x, circle_y, 2*dotRadius_px, 2*dotRadius_px);

        pEllipse = pScene->addEllipse(cirlceRect, reticlePen, reticleBrush);
        pGroup->addToGroup(pEllipse);

        pLine = pScene->addLine(circle_x+dotRadius_px, circle_y-dotRadius_px, circle_x+dotRadius_px, circle_y+3*dotRadius_px, reticlePen);
        pGroup->addToGroup(pLine);

        if (showGrid)
        {
            pLine = pScene->addLine(circle_x+dotRadius_px, A.y(), circle_x+dotRadius_px, B.y(), calibrationPen);
            pGroup->addToGroup(pLine);
        }
    }

    if (showTails)
    {
        QRectF tailRect;
        tailRect.setRect( - mil_to_pixel_scaled(mil_dot_count/2) - mil_to_pixel_scaled(3),
                          - mil_to_pixel_scaled(0.1),
                         mil_to_pixel_scaled(3),
                         mil_to_pixel_scaled(0.2));
        pRect = pScene->addRect(tailRect, reticlePen, reticleBrush);
        pGroup->addToGroup(pRect);

        tailRect.setRect(mil_to_pixel_scaled(mil_dot_count/2),
                          - mil_to_pixel_scaled(0.1),
                         mil_to_pixel_scaled(3),
                         mil_to_pixel_scaled(0.2));
        pRect = pScene->addRect(tailRect, reticlePen, reticleBrush);
        pGroup->addToGroup(pRect);

        tailRect.setRect( - mil_to_pixel_scaled(0.1),
                         mil_to_pixel_scaled(mil_dot_count/2),
                         mil_to_pixel_scaled(0.2),
                         mil_to_pixel_scaled(3));
        pRect = pScene->addRect(tailRect, reticlePen, reticleBrush);
        pGroup->addToGroup(pRect);

        tailRect.setRect(- mil_to_pixel_scaled(0.1),
                         - mil_to_pixel_scaled(mil_dot_count/2) - mil_to_pixel_scaled(3),
                         mil_to_pixel_scaled(0.2),
                         mil_to_pixel_scaled(3));
        pRect = pScene->addRect(tailRect, reticlePen, reticleBrush);
        pGroup->addToGroup(pRect);

        pEllipse = pScene->addEllipse(- mil_to_pixel_scaled(mil_dot_count/2) - mil_to_pixel_scaled(3),
                                      - mil_to_pixel_scaled(mil_dot_count/2) - mil_to_pixel_scaled(3),
                                      mil_to_pixel_scaled(mil_dot_count) + mil_to_pixel_scaled(3) * 2,
                                      mil_to_pixel_scaled(mil_dot_count) + mil_to_pixel_scaled(3) * 2);
        pGroup->addToGroup(pEllipse);
    }

    //if (!showGrid)
    {
        double cross_width = mil_to_pixel_scaled(0.2);
        for (int i=-5; i <= 5; i+=10) // cm
            for (int j=-5; j<=5; j+= 10) // cm
            {
                QBrush pointsBrush(QColor("red"));
                pEllipse = pScene->addEllipse(mil_to_pixel_scaled(i) - cross_width/2,
                                              mil_to_pixel_scaled(j) - cross_width/2,
                                              cross_width, cross_width, reticlePen, pointsBrush);
                pGroup->addToGroup(pEllipse);
            }
    }

    return pGroup;
}

void MainWindow::multiPagePrint(QGraphicsScene* pScene, QPrinter& printer)
{
    QPainter painter(&printer);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setRenderHint(QPainter::TextAntialiasing);
    QRectF pageRectPixel = printer.pageRect(QPrinter::DevicePixel);
    QRectF sceneRect = pScene->sceneRect();
    int rows = qCeil(sceneRect.height() / pageRectPixel.height());
    int cols = qCeil(sceneRect.width() / pageRectPixel.width());
    bool firstPage = true;
    int curPageNum = 0;
    int totalPageNum = rows * cols;
    for( int col=0; col < cols; col++)
    {
        for( int row=0; row < rows; row++)
        {
            curPageNum++;
            if (    (printer.fromPage() !=0 && curPageNum < printer.fromPage())
                 || (printer.toPage() != 0 && curPageNum > printer.toPage()))
                continue;

            double x = sceneRect.left() + col * pageRectPixel.width();
            double y = sceneRect.top() + row * pageRectPixel.height();
            QRectF printRect (x,y, pageRectPixel.width(), pageRectPixel.height());

            QList<QGraphicsItem*> miniaturesCollection;
            double miniatureWidth = printRect.width() / 100;
            double miniatureHeight = printRect.height() / 100;
            double miniatureMargin = Length(1).value(Length::Unit::px);
            QBrush currentPageColor ("grey");
            QBrush nonCurrentPageColor ("white");
            if (totalPageNum > 1)
            {
                for(int i=0; i<cols;i++)
                {
                    for(int j=0;j<rows;j++)
                    {
                        QRectF rect (printRect.x() + i * (miniatureWidth + miniatureMargin) + 10,
                                    printRect.y() + j * (miniatureHeight + miniatureMargin) + 10,
                                    miniatureWidth, miniatureHeight);
                        miniaturesCollection.append(pScene->addRect(rect, QPen(), (i == col && j == row)?currentPageColor:nonCurrentPageColor));
                    }
                }
                QPen marginPen(QColor("grey"), Length(0.3).value(Length::Unit::px), Qt::DashLine);
                pScene->addRect(printRect-QMargins(1,1,1,1), marginPen);
            }
            if (!firstPage)
                printer.newPage();

            pScene->render(&painter, QRect(), printRect);
            firstPage = false;
        }
    }
}

void MainWindow::onCalibration()
{
    QWidget* pWidget = this;

    double real_range = 50; // m
    double reticleScale = 1;
    int scopeScale = 1;

    TargetSizerDialog inputParams(pWidget);
    if (inputParams.getRangeOnly() == QDialog::Rejected)
        return;

    real_range = inputParams.realRange();
    reticleScale = inputParams.scale();
    scopeScale = inputParams.scopeScale();

    QGraphicsScene* pScene = new QGraphicsScene(pWidget);

    QPrinter printer(QPrinter::HighResolution);
    printer.setPageSize(QPrinter::A4);
    printer.setFullPage(false);
    printer.setOrientation(QPrinter::Landscape);
    setDpi(qApp->primaryScreen()->physicalDotsPerInch());
    if (!inputParams.showOnDisplay())
    {
        QPrintDialog dialog(&printer, pWidget);
        dialog.exec();
        setDpi(printer.resolution());
        printer.setColorMode(QPrinter::GrayScale);
    }
    QRectF pageRectMm  = printer.pageRect(QPrinter::Millimeter);
    QRectF pageRectPixel(0,0,Length(pageRectMm.width()).value(Length::Unit::px), Length(pageRectMm.height()).value(Length::Unit::px));

    QGraphicsItemGroup* pGrid = drawReticle(pScene, Length(real_range, Length::Unit::m), reticleScale, true);
    pScene->addItem(pGrid);

    QFont font;
    font.setBold(true);
    font.setPixelSize(Length(3).value(Length::Unit::px));

    QGraphicsTextItem* pText = pScene->addText(tr("Put this target on %1 m range\n"
                                                   "Set scope scale to x%2\n"
                                                   "Distance between lines: %3 mil, %4 mm")
                                           .arg(real_range)
                                           .arg(scopeScale)
                                           .arg(QString::number(reticleScale, 'f', 1))
                                           .arg(QString::number(real_range * reticleScale, 'f', 1)), font);

    double s = Length(5).value(Length::Unit::px);
    pText->setPos(s,s);

    if (inputParams.showOnDisplay())
    {
        QGraphicsView* pView = new QGraphicsView(pScene, nullptr);
        pView->setRenderHint(QPainter::Antialiasing, true);
        pView->setRenderHint(QPainter::TextAntialiasing, true);
        pView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        pView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        pView->setMinimumSize(pageRectPixel.size().toSize() + QSize(2,2));
        pView->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        pView->show();
    }
    else
    {
        multiPagePrint(pScene, printer);
    }
}


void MainWindow::onCalculator()
{
    BulletParams bulletParams;
    bulletParams.bc = 0.28;
    bulletParams.muzzleVelocity.setValue(951, Length::Unit::m, Time::Unit::s);
    bulletParams.zero_range.setValue(50, Length::Unit::m);
    bulletParams.sightHeight.setValue(85, Length::Unit::mm);
    bulletParams.shootingAngle.setValue(0, Angle::Unit::deg);
    bulletParams.dragFunction = G1;

    AtmosphereParams atmosphereParams;
    atmosphereParams.windSpeed.setValue(5, Length::Unit::m, Time::Unit::s);
    atmosphereParams.windAngle.setValue(90, Angle::Unit::deg);
    atmosphereParams.altitude.setValue(120, Length::Unit::m);
    atmosphereParams.pressure.setValue(1016, Pressure::Unit::hpa); //30.09;
    atmosphereParams.temperature.setValue(6, Temperature::Unit::c);
    atmosphereParams.humidity = 0.52;

    // range
    TargetParams targetParams;
    targetParams.rangeBegin.setValue(25, Length::Unit::m);
    targetParams.rangeEnd.setValue(551, Length::Unit::m);
    targetParams.rangeStep.setValue(5,Length::Unit::m);
    targetParams.targetHeight.setValue(170, Length::Unit::cm);
    targetParams.targetWidth.setValue(60, Length::Unit::cm);

    QDialog* pDialog = new QDialog(this);
    QBoxLayout* pDialogLayout = new QVBoxLayout(pDialog);
    QBoxLayout* pParamsLayout = new QHBoxLayout(pDialog);

    QFrame* pHLine = new QFrame(pDialog);
    BulletPropertiesWidget* pBulletProperties = new BulletPropertiesWidget(pDialog);
    AtmosphereParamsWidget* pAtmosphereProperties = new AtmosphereParamsWidget(pDialog);
    TargetParamsWidget* pTargetProperties = new TargetParamsWidget(pDialog);

    pHLine->setFrameShape(QFrame::HLine);

    Solution::Ptr solution = std::make_shared<Solution>();
    TrajectoryWidget* pTrajectory = new TrajectoryWidget(solution, this);

    pDialogLayout->addLayout(pParamsLayout);
    pDialogLayout->addWidget(pHLine);
    pDialogLayout->addWidget(pTrajectory);

    pParamsLayout->addWidget(pBulletProperties);
    pParamsLayout->addWidget(pAtmosphereProperties);
    pParamsLayout->addWidget(pTargetProperties);

    connect (pBulletProperties, &BulletPropertiesWidget::changed, [pTrajectory, pBulletProperties]()
    {
        pTrajectory->recalculate(pBulletProperties->params());
    });
    connect (pAtmosphereProperties, &AtmosphereParamsWidget::changed, [pTrajectory, pAtmosphereProperties]()
    {
        pTrajectory->recalculate(pAtmosphereProperties->params());
    });
    connect (pTargetProperties, &TargetParamsWidget::changed, [pTrajectory, pTargetProperties]()
    {
        pTrajectory->populateView(pTargetProperties->params());
    });

    pBulletProperties->setParams(bulletParams);
    pAtmosphereProperties->setParams(atmosphereParams);
    pTargetProperties->setParams(targetParams);

    pTrajectory->init(pBulletProperties->params(), pAtmosphereProperties->params(), pTargetProperties->params());

    pDialog->show();
}

void MainWindow::onAnimation()
{
    QDialog* pDialog = new QDialog(this);
    pDialog->showFullScreen();

    QGraphicsScene* pScene = new QGraphicsScene(pDialog);
    QGraphicsView* pView = new QGraphicsView(pDialog);
    pView->setScene(pScene);
    //pView->setViewport(new QGLWidget(pDialog));
    //pView->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);

    QLayout* pLayout = new QVBoxLayout(pDialog);
    pDialog->setLayout(pLayout);
    pLayout->addWidget(pView);

    pLayout->setMargin(0);

    int screenWidth;
    int screenHeight;
    screenWidth = qApp->primaryScreen()->geometry().width();
    screenHeight = qApp->primaryScreen()->geometry().height();
    setDpi(qApp->primaryScreen()->physicalDotsPerInch());

    pView->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    pView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    pView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);


    QString landscapeFilename = QStringLiteral("landscape");
    double screenAspectRation = double(screenWidth) / screenHeight;
    if (qFuzzyCompare(screenAspectRation, 16.0 / 10))
        landscapeFilename = QStringLiteral("16x10");
    else if (qFuzzyCompare(screenAspectRation, 16.0 / 9))
        landscapeFilename = QStringLiteral("16x10");
    else if (qFuzzyCompare(screenAspectRation, 4.0 / 3))
        landscapeFilename = QStringLiteral("4x3");

    QPixmap landscape(QStringLiteral(":/%1.jpg").arg(landscapeFilename));
    QGraphicsPixmapItem* pLandscape = pScene->addPixmap(landscape.scaled(screenWidth,screenHeight,Qt::KeepAspectRatioByExpanding));
    pView->setSceneRect(0,-screenHeight/2,screenWidth,screenHeight);
    pLandscape->moveBy(0, -screenHeight/2);

#define REAL_IMG_OBJ_SIZE 6.4
#define VIS_IMG_OBJ_SIZE 0.05

    pScene->addLine(0,screenHeight/2,screenWidth,screenHeight/2, QPen("white"));


    TargetPtr target = TargetBuilder::ref().target(QStringLiteral("idpa"));
    QGraphicsItemGroup* pTargetImg = target->release();
    int targetWidth = pTargetImg->boundingRect().width();
    int targetHeight = pTargetImg->boundingRect().height();
    pScene->addItem(pTargetImg);



    QPropertyAnimation* pPositionAnima = new QPropertyAnimation(dynamic_cast<QObject*>(pTargetImg), "pos", pDialog);
    QPropertyAnimation* pScaleAnima = new QPropertyAnimation(dynamic_cast<QObject*>(pTargetImg), "scale", pDialog);
    QParallelAnimationGroup* pAnimaGroup = new QParallelAnimationGroup(pDialog);
    pAnimaGroup->addAnimation(pPositionAnima);
    pAnimaGroup->addAnimation(pScaleAnima);

    QEasingCurve::Type t = QEasingCurve::Linear;
    int duration = 0;
    double whole_path_len = 1;
    double timePoint = 0;
    QPointF posPoint = {1,1};
    QPropertyAnimation::KeyValues posStops;
    QPropertyAnimation::KeyValues scaleStops;
    double vert_move = 0;
    double hor_move = 0;
    double targetScale = 0.33;

    int trajectory = 2;
    switch (trajectory)
    {
        case 0:
            targetScale = VIS_IMG_OBJ_SIZE / REAL_IMG_OBJ_SIZE * dpm();
            targetScale  =0.33;
            pTargetImg->setScale(targetScale);
            targetWidth *= targetScale;
            targetHeight *= targetScale;
            duration = 10;
            vert_move = 20;
            hor_move = screenWidth - targetWidth;
            whole_path_len = ( hor_move + vert_move) * 2;

            timePoint = 0;
            posPoint = QPointF(targetWidth/2, 220);
            posStops.append({timePoint, posPoint});

            timePoint += vert_move / whole_path_len;
            posPoint.ry() -= vert_move;
            posStops.append({timePoint, posPoint});

            timePoint += hor_move/whole_path_len;
            posPoint.rx() += hor_move;
            posStops.append({timePoint, posPoint});

            timePoint += vert_move / whole_path_len;
            posPoint.ry() += vert_move;
            posStops.append({timePoint, posPoint});

            timePoint += hor_move/whole_path_len;
            posPoint.rx() -= hor_move;
            posStops.append({timePoint, posPoint});

//			scaleStops.append({0, 2.5});
//			scaleStops.append({0.25, 1});
//			scaleStops.append({0.5, 2.5});
//			scaleStops.append({0.75, 1});
//			scaleStops.append({1, 2.5});

            t = QEasingCurve::Linear;
            break;
        case 1:
            duration = 10;
            targetScale = .33;
            pTargetImg->setScale(targetScale);
            targetWidth *= targetScale;
            targetHeight *= targetScale;
            vert_move = targetHeight + 50;
            hor_move = screenWidth * 2 / 3;

            whole_path_len = vert_move * 2 + hor_move;

            timePoint = 0;
            posPoint = QPointF(screenWidth/6, screenHeight/2 + targetHeight/2);
            posStops.append({timePoint, posPoint});

            timePoint +=  vert_move / whole_path_len;
            posPoint.ry() -= vert_move;
            posStops.append({timePoint, posPoint});

            timePoint +=  hor_move / whole_path_len;
            posPoint.rx() += hor_move;
            posStops.append({timePoint, posPoint});

            timePoint +=  vert_move / whole_path_len;
            posPoint.ry() += vert_move;
            posStops.append({timePoint, posPoint});

            t = QEasingCurve::InOutQuart; // InOutBounce
            break;
        case 2:
            targetScale = .33;

            posPoint = QPointF(qrand() % 400 + screenWidth - 200, qrand() % 400 - 200);
            posStops.append({0, posPoint});
            posStops.append({1, posPoint});

            scaleStops.append({0, targetScale /100});
            scaleStops.append({0.5, targetScale});
            scaleStops.append({1, targetScale / 100});

            duration=10;
            t = QEasingCurve::OutInQuart;
    }
    pPositionAnima->setKeyValues(posStops);
    pPositionAnima->setDuration(duration * 1000);
    pPositionAnima->setEasingCurve(t);
    pPositionAnima->setLoopCount(-1);

    pScaleAnima->setKeyValues(scaleStops);
    pScaleAnima->setDuration(duration * 1000);
    pScaleAnima->setEasingCurve(t);
    pScaleAnima->setLoopCount(-1);

    pAnimaGroup->start();
}

void MainWindow::onPerspective()
{
    QDialog* pWidget = new ProjectionDialog();
    pWidget->exec();
}

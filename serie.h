#ifndef SERIE_H
#define SERIE_H

#include "types.h"
#include "hole.h"
#include "xmlhelper.h"
#include "target.h"

#include <QtCore/QMap>
#include <QtCore/QPointer>
#include <QtCore/QDate>
#include <QtCore/QUuid>

class QPushButton;
class QGraphicsItem;
class QGraphicsRectItem;
class QGraphicsLineItem;
class QGraphicsItemGroup;

class Serie
{
public:
    // input values
    enum class Position {Unknown, Standing, Sitting, Laying, Bench, _End = Bench};
    enum class Scope {Unknown, None, IronSight, RedDot, Laser, Optic, _End=Optic};
    enum class Firestyle {Unknown, TargetSwitch, Slow, Rapid, _End=Rapid};
    enum class HoleShape{Unknown, Cirle, Triangle = 3, Diamond = 4, Pentagon = 5, Hexagon = 6, Square,  CrossCirle, PlusCirlce};

    static QString positionToString(Position position);
    static QString scopeToString(Scope scope);
    static QString firestyleToString(Firestyle firestyle);
    static QString shapeToString(HoleShape shape);

    static Position stringToPosition(const QString& str);
    static Scope stringToScope(const QString& str);
    static Firestyle stringToFirestyle (const QString& str);
    static HoleShape stringToShape(const QString& str);

    int number;
    Position position;
    Scope scope;
    Firestyle firestyle;
    QString comment;
    Length range;


    Serie(SessionPtr session = nullptr);
    virtual ~Serie();

    virtual QColor getColor() const;
    void setColor(QColor color);

    virtual HoleShape getShape() const;
    void setHoleShape(HoleShape shape);

    virtual QString name() const;
    void setName(const QString&  name);

    QString uuid() const { return _uuid.toString();}
    void setUuid(const QString& uuid) { _uuid = uuid;}

    QString info();

    virtual Holes holes() const;
    virtual void setHoles(const Holes& holes);
    virtual void addHole(const HolesItem& hole);
    virtual int holesCount() const;

    virtual QDate date() const;
    virtual Caliber caliber() const;
    virtual const QString& targetName() const;
    virtual const QString& shooter() const;

    void setSession(SessionPtr p);
    PolarCoordinate massCenter() const;
    Length sd() const;
    double maxDistance() const;
    std::pair<QPointF, QPointF> mostDistantPoints() const;
    double maxRadius() const;
    QRectF boundingRect() const;

    int holeValue (const QPointF& coord ) const;
    int valueCount(int v) const;
    const QMap<int, int>& values() const;
    int scoreTotal();
    double scoreAvg();
    double scoreSd();

protected:
    void invalidateCache();

private:
    // calculated mass values
    mutable std::unique_ptr<QPointF> _massCenter;
    mutable std::unique_ptr<Length> _sd;
    mutable std::unique_ptr<std::pair<QPointF, QPointF>>  _mostDistantPoints;
    mutable std::unique_ptr<double> _maxRadius;
    mutable std::unique_ptr<QMap<int, int>> _values;
    std::unique_ptr<int> _totalScore;
    mutable std::unique_ptr<QRectF> _boundingRect;

    // data elements
    QUuid _uuid;
    QColor _color;
    QString _name;
    HoleShape _shape;

    // c++ junk
    Serie(const Serie& s) = delete;
    Serie& operator=(const Serie& e) = delete;

    Session* session() const;
    SessionWPtr wpSession;
    Holes _holes;

    virtual TargetPtr target() const;

    // calculate mass values functions
    PolarCoordinate calculateMassCenter() const;
    Length calculateSd() const;
    std::pair<QPointF, QPointF> calculateMostDistantPoints() const;
    double calculateMaxRadius() const;
    QMap<int, int>  calculateValues() const;
    QRectF calculateBoundingRect() const;
};

bool readXmlTag(const QDomElement& serieElement, Serie& serie, const ReadXmlContext& context, bool optional = true);
bool writeXmlTag(QDomDocument &doc, QDomElement& serieElement, const Serie& serie);

#endif // SERIE_H


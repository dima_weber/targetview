#ifndef XMLPARSER_H
#define XMLPARSER_H

#include "parser.h"
#include "session.h"

class QDomElement;
class Serie;

class XmlSessionReader : public SessionReader
{
public :
    XmlSessionReader(Sessions& sessions):SessionReader(sessions){}
protected:
    virtual bool parse(QFile&);

};

#endif

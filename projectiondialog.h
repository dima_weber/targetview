#ifndef PROJECTIONDIALOG_H
#define PROJECTIONDIALOG_H

#include <QtWidgets/QDialog>
#include "units.h"
#include <QAbstractTableModel>
#include <QStyledItemDelegate>

namespace Ui {
class ProjectionDialog;
}

class QGraphicsScene;
class QGraphicsView;
class QSlider;
class QPropertyAnimation;
class AtmosphereParamsWidget;
class BulletPropertiesWidget;
class TargetParamsWidget;
class QGroupBox;
class QGraphicsItem;
class QTableView;

struct TargetPos
{
    QString name;
    Length distance;
    Length offset;
    Speed speed;
    QColor color;
    QPropertyAnimation* pAnima;
    QGraphicsItem* pImg;
    Time curTime;
    enum {Running, Paused, Hidden} hidden;
};

class TargetSelectDelegate: public QStyledItemDelegate
{
    Q_OBJECT
public:
    TargetSelectDelegate(QObject* parent = nullptr);
    QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                              const QModelIndex& index) const override;

        void setEditorData(QWidget *editor, const QModelIndex &index) const override;
        void setModelData(QWidget *editor, QAbstractItemModel *model,
                          const QModelIndex &index) const override;

        void updateEditorGeometry(QWidget *editor,
            const QStyleOptionViewItem &option, const QModelIndex &index) const override;
};

class DistanceEditDelegate: public QStyledItemDelegate
{
    Q_OBJECT
    int _min;
    int _max;
    int _step;
    QString _suff;

public:
    DistanceEditDelegate(int min, int max, int step, const QString& suff, QObject* parent = nullptr);
    QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                              const QModelIndex& index) const override;

        void setEditorData(QWidget *editor, const QModelIndex &index) const override;
        void setModelData(QWidget *editor, QAbstractItemModel *model,
                          const QModelIndex &index) const override;

        void updateEditorGeometry(QWidget *editor,
            const QStyleOptionViewItem &option, const QModelIndex &index) const override;
};

class ColorSelectDelegate: public QStyledItemDelegate
{
    Q_OBJECT
    const QStringList colorNames;
public:
    ColorSelectDelegate(const QStringList& colors = QColor::colorNames(), QObject* parent = nullptr);
    QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                              const QModelIndex& index) const override;

        void setEditorData(QWidget *editor, const QModelIndex &index) const override;
        void setModelData(QWidget *editor, QAbstractItemModel *model,
                          const QModelIndex &index) const override;

        void updateEditorGeometry(QWidget *editor,
            const QStyleOptionViewItem &option, const QModelIndex &index) const override;
};

class TargetModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    TargetModel(QObject* parent = nullptr);

     int rowCount(const QModelIndex &parent = QModelIndex()) const override;
     int columnCount(const QModelIndex &parent = QModelIndex()) const override;
     QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
     QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
     bool setData(const QModelIndex &index, const QVariant &value, int role) override;
     Qt::ItemFlags flags(const QModelIndex &index) const override;

     void addTarget(const QString &name, const Length &distance, const Length &offset, const Speed &speed, const QColor &color);
     QMap<int, TargetPos>& targets() { return _targets;}
private:
     QMap<int, TargetPos> _targets;
};

class ProjectionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ProjectionDialog(QWidget *parent = 0);
    ~ProjectionDialog();
protected slots:
    void render();

    void renderTrajectory();
    void renderTargets();
    void renderWalls();
    void renderFloor();

    void removeSceneItems(QList<QGraphicsItem *> &itemList);
    void setCustomDpi(double dpi);
    void setVertCorr(double moa);
    void setSceneDistance(double meters);
    void setSceneWidth(double meters);

private:
    void addTarget(const QString& name, const Length& distance, const Length& offset, const Speed& speed = Speed(), const QColor& color=QColor("olive") );

    Length sceneWidth() const;
    Length sceneDistance() const;
    double rescaleMmAtRangeToPxAtRealRange(const Length &distance, const Length &size) const;

    QColor rangeCorrectedColor(Length range, const QColor& c) const;
    QColor floorColor() const;
    QColor wallColor() const;
    QColor trajectoryColor(bool above_ground = true) const;
    Length wallHeight() const;
    Length min_range() const;
    Length max_range() const;
    double x_min() const;
    double y_min() const;
    double x_max() const;
    double y_max() const;
    double y_coord (double x);
    double viewVertPosition() const;
    double viewHorzPosition() const;
    Length trajectoryPosition() const;
    Length weaponHeight() const;
    Angle trajectoryVertCorrection() const;


    Ui::ProjectionDialog *ui;

    QGraphicsScene* pScene;

    QList<QGraphicsItem*> trajectoryItems;
    QList<QGraphicsItem*> wallItems;
    QList<QGraphicsItem*> floorItems;
    QList<QGraphicsItem*> targetItems;

    TargetModel* pTargetModel;
    double customDpi;
    Angle vertCorr;
    Length sceneDist;
    Length sceneWth;
};

#endif // PROJECTIONDIALOG_H

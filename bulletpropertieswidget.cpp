#include "bulletpropertieswidget.h"
#include "ui_bulletpropertieswidget.h"

#include "ballistics/ballistics.h"

BulletPropertiesWidget::BulletPropertiesWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BulletPropertiesWidget)
{
    ui->setupUi(this);

    connect (ui->bcInput, SIGNAL(valueChanged(QString)), SIGNAL(changed()));
    connect (ui->dragFunctionInput, SIGNAL(currentIndexChanged(int)), SIGNAL(changed()));
    connect (ui->muzzleVelocityInput, SIGNAL(valueChanged(int)), SIGNAL(changed()));
    connect (ui->shootingAngleInput, SIGNAL(valueChanged(double)), SIGNAL(changed()));
    connect (ui->sightHeightInput, SIGNAL(valueChanged(double)), SIGNAL(changed()));
    connect (ui->zeroRangeInput, SIGNAL(valueChanged(int)), SIGNAL(changed()));
}

BulletPropertiesWidget::~BulletPropertiesWidget()
{
    delete ui;
}

const BulletParams& BulletPropertiesWidget::params() const
{
    static BulletParams param;
    param.bc = ui->bcInput->value();
    param.muzzleVelocity.setValue(ui->muzzleVelocityInput->value(), Length::Unit::m, Time::Unit::s);
    param.shootingAngle.setValue(ui->shootingAngleInput->value(), Angle::Unit::deg);
    param.sightHeight.setValue(ui->sightHeightInput->value(), Length::Unit::cm);
    param.zero_range.setValue(ui->zeroRangeInput->value(), Length::Unit::m);
    param.dragFunction = ui->dragFunctionInput->currentIndex()+1;

    return param;
}

void BulletPropertiesWidget::setParams(const BulletParams& param)
{
    blockSignals(true);
    ui->bcInput->setValue(param.bc);
    ui->dragFunctionInput->setCurrentIndex(param.dragFunction - 1);
    ui->muzzleVelocityInput->setValue(param.muzzleVelocity.value(Length::Unit::m, Time::Unit::s));
    ui->shootingAngleInput->setValue(param.shootingAngle.value(Angle::Unit::deg));
    ui->sightHeightInput->setValue(param.sightHeight.value(Length::Unit::cm));
    ui->zeroRangeInput->setValue(param.zero_range.value(Length::Unit::m));
    blockSignals(false);
//    emit changed();
}

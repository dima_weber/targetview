#-------------------------------------------------
#
# Project created by QtCreator 2015-10-11T22:32:05
#
#-------------------------------------------------

#QT += opengl
QT       += core gui xml concurrent script positioning network opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = TargetShow
TEMPLATE = app

CONFIG += c++11
CONFIG += silent

linux-* {
    QMAKE_CXX = ccache g++
    QMAKE_CXXFLAGS +=  -Wpedantic -Wall
}


win32 {
    debug{
        INCLUDEPATH += C:\projects\gsl\gsl\build
        LIBS += C:\projects\gsl\gsl\build\Debug\gsl.lib \
                C:\projects\gsl\gsl\build\Debug\gslcblas.lib
    }
    release {
        INCLUDEPATH += C:\projects\gsl\gsl\build
        LIBS += C:\projects\gsl\gsl\build\Release\gsl.lib \
                C:\projects\gsl\gsl\build\Release\gslcblas.lib
    }
} else {
    LIBS += -lgsl -lgslcblas
}

SOURCES +=  main.cpp\
            mainwindow.cpp \
            targetview.cpp \
            xmlparser.cpp \
            parser.cpp \
            xmlhelper.cpp \
            recognizeform.cpp \
            serie.cpp \
            session.cpp \
            qcustomplot/qcustomplot.cpp \
            analyzeform.cpp \
            seriecollection.cpp \
            seriefilterwidget.cpp \
            sessionseditwidget.cpp \
            sessioneditwidget.cpp \
            serieseditwidget.cpp \
            serieeditwidget.cpp \
            guiserie.cpp \
            targetbuilder.cpp \
            target.cpp \
    hole.cpp \
    seriesortfiltermodel.cpp \
    targetsizerdialog.cpp \
    ballistics/ballistics.cpp \
    bulletpropertieswidget.cpp \
    atmosphereparamswidget.cpp \
    targetparamswidget.cpp \
    trajectorywidget.cpp \
    projectiondialog.cpp \
    units_converter.cpp \
    units.cpp

HEADERS  += mainwindow.h \
            targetview.h \
            xmlparser.h \
            parser.h \
            types.h \
            recognizeform.h \
            serie.h \
            session.h \
            xmlhelper.h \
            qcustomplot/qcustomplot.h \
            analyzeform.h \
            seriecollection.h \
            opacitywidget.h \
            seriefilterwidget.h \
            sessionseditwidget.h \
            sessioneditwidget.h \
            serieseditwidget.h \
            serieeditwidget.h \
            guiserie.h \
            targetbuilder.h \
            target.h \
    hole.h \
    accessor.h \
    nodelistiterator.h \
    seriesortfiltermodel.h \
    targetsizerdialog.h \
    ballistics/ballistics.h \
    bulletpropertieswidget.h \
    atmosphereparamswidget.h \
    targetparamswidget.h \
    units.h \
    trajectorywidget.h \
    projectiondialog.h \
    units_converter.h

FORMS    += mainwindow.ui \
            recognizeform.ui \
            analyzeform.ui \
            seriefilterwidget.ui \
    targetsizerdialog.ui \
    bulletpropertieswidget.ui \
    atmosphereparamswidget.ui \
    targetparamswidget.ui \
    trajectorywidget.ui \
    projectiondialog.ui

DISTFILES += \
    data/Holes.xml \
    data/targets.xml \
    TODO \
    icon.png

TRANSLATIONS += ru.ts

DESTDIR = bin


OBJECTS_DIR = .obj
UI_DIR = .ui
MOC_DIR = .moc
RCC_DIR = .rcc

RESOURCES += \
    resource.qrc

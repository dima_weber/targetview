#include "parser.h"
#include "mainwindow.h"

#include <QtCore/QFile>

bool SessionReader::read(const QString& filePath)
{
	QFile file(filePath);
	bool ok = file.open(QFile::ReadOnly);
	if (ok)
	{
		_sessions.clear();
		ok = parse(file);
		file.close();
	}
	return ok;
}


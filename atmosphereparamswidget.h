#ifndef ATMOSPHEREPARAMSWIDGET_H
#define ATMOSPHEREPARAMSWIDGET_H

#include <QtWidgets/QWidget>
#include <QtNetwork/QNetworkAccessManager>

namespace Ui {
class AtmosphereParamsWidget;
}

struct AtmosphereParams;
class QNetworkReply;

class AtmosphereParamsWidget : public QWidget
{
	Q_OBJECT


public:
	explicit AtmosphereParamsWidget(QWidget *parent = 0);
	~AtmosphereParamsWidget();

	const AtmosphereParams& params() const;
	void setParams(const AtmosphereParams& param);

public slots:
	void onGetFromInternet();
	void onInternetReply(QNetworkReply* pReply);

signals:
	void changed();

private:
	Ui::AtmosphereParamsWidget *ui;
	QNetworkAccessManager manager;
};

#endif // ATMOSPHEREPARAMSWIDGET_H

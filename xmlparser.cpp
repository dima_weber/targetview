#include "xmlparser.h"
#include "mainwindow.h"
#include "xmlhelper.h"

#include <QtXml/QDomDocument>
#include <QtCore/QFile>
#include <QtCore/QDebug>
#include <QtCore/QElapsedTimer>

bool XmlSessionReader::parse(QFile& file)
{
    bool ok = false;

    QElapsedTimer timer;
    timer.start();

    QDomDocument doc("holes");
    QString errMsg;
    int line, column;
    if (!doc.setContent(&file, &errMsg, &line, &column))
    {
        ok = false;
        qWarning() << QString("Broken xml file: %1 Line %2, column %3")
                    .arg(errMsg)
                    .arg(line)
                    .arg(column);
    }
    else
    {
        QDomElement rootElem = doc.documentElement();
        if (rootElem.tagName() == "targetshow")
        {
            QString version = rootElem.attribute("version");
            if (version != "1")
            {
                qWarning() << "wrong TargetShow xml file version" << version;
                ok = false;
            }
            else
            {
                MutexVector<SessionsItem> sess;
                ok = readChildArray(rootElem, "sessions", "session", sess, ReadXmlContext(), true);
                for(SessionsItem session: sess)
                    _sessions[session->name()] = session;
            }
        }
        else
        {
            ok = false;
            qWarning() << "unknown tag" << rootElem.tagName();
        }
    }

    qDebug() << "xml read in " << timer.elapsed() << "ms";

    file.close();
    return ok;
}



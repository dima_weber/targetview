#include "analyzeform.h"
#include "ui_analyzeform.h"
#include "session.h"
#include "opacitywidget.h"

#include <QtCore/QDebug>
//#include <qcustomplot/qcustomplot.h>


AnalyzeForm::AnalyzeForm(SerieCollection& collection, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AnalyzeForm),
    collection(collection)
{
    ui->setupUi(this);
    setLayout(ui->verticalLayout);

    ui->pScoreTab->plotLayout()->insertRow(0);
    ui->pScoreTab->plotLayout()->addElement(0,0,new QCPPlotTitle(ui->pScoreTab, tr("Serie score")));
    ui->pScoreTab->xAxis->setTickLabelRotation(60);
    ui->pScoreTab->xAxis->setSubTickCount(0);
    ui->pScoreTab->xAxis->setTickLength(0, 4);
    ui->pScoreTab->xAxis->grid()->setVisible(true);
    ui->pScoreTab->yAxis->setPadding(5);
    ui->pScoreTab->xAxis->setLabel(tr("Serie"));
    ui->pScoreTab->yAxis->setLabel(tr("Score"));
    ui->pScoreTab->yAxis->setSubTickCount(0);

    ui->pBarTab->plotLayout()->insertRow(0);
    ui->pBarTab->plotLayout()->addElement(0,0, new QCPPlotTitle(ui->pBarTab, tr("Shots distribution per serie")));
    ui->pBarTab->xAxis->setTickLabelRotation(60);
    ui->pBarTab->xAxis->setSubTickCount(0);
    ui->pBarTab->xAxis->setTickLength(0, 4);
    ui->pBarTab->xAxis->grid()->setVisible(true);
    ui->pBarTab->yAxis->setPadding(5);
    ui->pBarTab->xAxis->setLabel(tr("Score"));
    ui->pBarTab->yAxis->setLabel(tr("Shots"));
    ui->pBarTab->yAxis->setSubTickCount(0);

    ui->pDispersionTab->plotLayout()->insertRow(0);
    ui->pDispersionTab->plotLayout()->addElement(0,0,new QCPPlotTitle(ui->pDispersionTab, tr("Serie dispersion")));
    ui->pDispersionTab->xAxis->setTickLabelRotation(60);
    ui->pDispersionTab->xAxis->setSubTickCount(0);
    ui->pDispersionTab->xAxis->setTickLength(0, 4);
    ui->pDispersionTab->xAxis->grid()->setVisible(true);
    ui->pDispersionTab->yAxis->setPadding(5);
    ui->pDispersionTab->xAxis->setLabel(tr("Serie"));
    ui->pDispersionTab->yAxis->setLabel(tr("Shift / Dispersion, mm"));
    ui->pDispersionTab->yAxis->setSubTickCount(0);

    // setup legend:
    ui->pBarTab->legend->setVisible(true);
    ui->pBarTab->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignTop|Qt::AlignLeft);
    ui->pBarTab->legend->setBrush(QColor(255, 255, 255, 200));
    QPen legendPen;
    legendPen.setColor(QColor(130, 130, 130, 200));
    ui->pBarTab->legend->setBorderPen(legendPen);
    QFont legendFont = font();
    legendFont.setPointSize(10);
    ui->pBarTab->legend->setFont(legendFont);
    ui->pBarTab->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);


    ui->pBarTab->setAntialiasedElements(QCP::aeAll);
    ui->pScoreTab->setAntialiasedElements(QCP::aeAll);
    ui->pDispersionTab->setAntialiasedElements(QCP::aeAll);

    QPushButton* pSaveBarImageButton = new OpacityWidget(0.4, 0.9, tr("Save image"), ui->tabWidget);
    connect (pSaveBarImageButton, &QPushButton::clicked, [=](bool)
    {

        QString proposedFileName = QString("data/analyze-%1.png").arg(QDateTime::currentDateTime().toString("yyyyMMddhhmmss"));
        QString fileName = QFileDialog::getSaveFileName(this, tr("Save image"),
                                                        QDir::current().filePath(proposedFileName), tr("PNG Image (*.png)") );
        if (!fileName.isEmpty())
        {
            QCustomPlot* pTab = dynamic_cast<QCustomPlot*>(ui->tabWidget->currentWidget());
            if (pTab)
                pTab->savePng(fileName);
        }
    });
    ui->tabWidget->setCornerWidget(pSaveBarImageButton,Qt::TopRightCorner);

    ui->pFilter->setSerieCollection(&collection);
    connect (&collection, SIGNAL(filterChanged()), SLOT(onFilterChanged()));
    onFilterChanged();
}

AnalyzeForm::~AnalyzeForm()
{
    delete ui;
}

void AnalyzeForm::drawShotsBar(const QMap<QString, QVector<SeriePtr>>& data)
{
    ui->pBarTab->clearPlottables();

    if (!data.isEmpty() && !data["all"].isEmpty())
    {
        const QVector<SeriePtr>& allSerie = data["all"];
        int minCnt = INT_MAX;
        int maxCnt = 0;
        QVector<double> ticks;
        QVector<QString> labels;

        QCPBarsGroup* pGroup = new QCPBarsGroup(ui->pBarTab);
        for (auto serie: allSerie)
        {
            QCPBars* shots = new QCPBars(ui->pBarTab->xAxis, ui->pBarTab->yAxis);
            ui->pBarTab->addPlottable(shots);
            shots->setBarsGroup(pGroup);
            shots->setWidth(1.0 / (allSerie.size()+3));
            QPen pen;
            pen.setWidthF(1.2);
            shots->setName(serie->date().toString("yyyy-MM-dd") + " " + serie->name());
            QColor color = serie->getColor();
            pen.setColor(color);
            shots->setPen(pen);
            color.setAlpha(50);
            shots->setBrush(color);

            QVector<double> values;
            for (int i=10; i>=0; i--)
            {
                ticks << i;
                minCnt = qMin(minCnt, serie->valueCount(i));
                maxCnt = qMax(maxCnt, serie->valueCount(i));
                labels << QString::number(i);
                values << serie->valueCount(i);
            }

            shots->setData(ticks, values);
        }
        ui->pBarTab->yAxis->setRange(0, maxCnt+1);

        ui->pBarTab->xAxis->setAutoTicks(false);
        ui->pBarTab->xAxis->setAutoTickLabels(false);

        ui->pBarTab->xAxis->setTickVector(ticks);
        ui->pBarTab->xAxis->setTickVectorLabels(labels);
        ui->pBarTab->xAxis->setRange(ticks[ticks.size()-1]-1, ticks[0]+1);
    }
    ui->pBarTab->replot();
}

void AnalyzeForm::drawScoreGraph(const QMap<QString, QVector<SeriePtr>>& data)
{
//    ui->pScoreTab->clearGraphs();
//    ui->pScoreTab->clearItems();
    ui->pScoreTab->clearPlottables();

    if (!data.isEmpty() && !data["all"].isEmpty())
    {
        QVector<double> timePoints;
        QVector<QString> labels;
        double minScore = 10;
        double maxScore = 4;

        ui->pScoreTab->legend->setVisible(true);
        ui->pScoreTab->legend->setFont(QFont("Helvetica", 9));

        int graphCnt = 0;
        for (const QString& splitName: data.keys())
        {
            if (splitName == "all" && data.size() > 1)
                continue;

            const QVector<SeriePtr>& split = data[splitName];

            ui->pScoreTab->addGraph();

            QPen boundsPen(QColor(180, 180, 20));
            QPen valuePen(QColor(qrand() % 255,qrand() % 255, qrand() % 255));
            QBrush brush(QColor(255,50,30,20));

            ui->pScoreTab->graph(graphCnt+0)->setLineStyle(QCPGraph::lsLine);
            ui->pScoreTab->graph(graphCnt+0)->setPen(boundsPen);
            ui->pScoreTab->graph(graphCnt+0)->setBrush(brush);
            ui->pScoreTab->graph(graphCnt+0)->setName("Lower bound (34%)");
            ui->pScoreTab->graph(graphCnt+0)->setScatterStyle(QCPScatterStyle::ssPlus);

            ui->pScoreTab->addGraph();
            ui->pScoreTab->graph(graphCnt+1)->setLineStyle(QCPGraph::lsLine);
            ui->pScoreTab->graph(graphCnt+1)->setPen(boundsPen);
            ui->pScoreTab->graph(graphCnt+0)->setChannelFillGraph(ui->pScoreTab->graph(graphCnt+1));
            ui->pScoreTab->graph(graphCnt+1)->setName("Upper bound (34%)");
            ui->pScoreTab->graph(graphCnt+1)->setScatterStyle(QCPScatterStyle::ssPlus);

            ui->pScoreTab->addGraph();
            ui->pScoreTab->graph(graphCnt+2)->setPen(valuePen);
//            ui->pScoreTab->graph(graphCnt+2)->setName("Mean value");
            ui->pScoreTab->graph(graphCnt+2)->setScatterStyle(QCPScatterStyle::ssCircle);
            ui->pScoreTab->graph(graphCnt+2)->setName(splitName);

            QCPPlottableLegendItem* pLegentItem = ui->pScoreTab->legend->itemWithPlottable(ui->pScoreTab->graph(graphCnt+0));
            ui->pScoreTab->legend->removeItem(pLegentItem);
            pLegentItem = ui->pScoreTab->legend->itemWithPlottable(ui->pScoreTab->graph(graphCnt+1));
            ui->pScoreTab->legend->removeItem(pLegentItem); // don't show two confidence band graphs in legend

            QVector<double> meanScore;
            QVector<double> lowerBound;
            QVector<double> upperBound;

            int i=0;
            for(SeriePtr serie: split)
            {
                if (!timePoints.contains(i))
                {
                    timePoints << i;
                    labels << QString::number(i);
                }

                double scoreAvg = serie->scoreAvg();
                double scoreSd = serie->scoreSd();
                double min = scoreAvg - scoreSd;
                double max = scoreAvg + scoreSd;
                meanScore << scoreAvg;
                lowerBound << min;
                upperBound << max;
                minScore = qMin(min, minScore);
                maxScore = qMax(max, maxScore);
//                if (labels.size() <= i)
//                    labels << serie->name;
//                else
//                    labels[i] += " " + serie->name;

                i++;
            }
            ui->pScoreTab->graph(graphCnt+0)->setData(timePoints, lowerBound);
            ui->pScoreTab->graph(graphCnt+1)->setData(timePoints, upperBound);
            ui->pScoreTab->graph(graphCnt+2)->setData(timePoints, meanScore);


            graphCnt += 3;
        }

        ui->pScoreTab->xAxis->setRange(timePoints[0]-1, timePoints[timePoints.size()-1]+1);
        ui->pScoreTab->xAxis->setTickVector(timePoints);
        ui->pScoreTab->xAxis->setTickVectorLabels(labels);

        ui->pScoreTab->yAxis->setRange(0, maxScore);
    }

    ui->pScoreTab->xAxis->setAutoTicks(false);
    ui->pScoreTab->xAxis->setAutoTickLabels(false);

    ui->pScoreTab->replot();
}

void AnalyzeForm::drawColorMap(const QMap<QString, QVector<SeriePtr>>& data)
{
    ui->pColorTab->clearPlottables();
    if (!data.isEmpty() && !data["all"].isEmpty())
    {
        QCPColorMap *colorMap = new QCPColorMap( ui->pColorTab->xAxis,  ui->pColorTab->yAxis);
         ui->pColorTab->addPlottable(colorMap);
        int granularity = ui->pGranularity->value();
        int nx = 500 / granularity;
        int ny = 500 / granularity;
        colorMap->data()->setSize(nx, ny); // we want the color map to have nx * ny data points
        colorMap->data()->setRange(QCPRange(-250, 250), QCPRange(-250, 250)); // and span the coordinate range -4..4 in both key (x) and value (y) dimensions
        // now we assign some data, by accessing the QCPColorMapData instance of the color map:
        int z;
        for (SeriePtr pSerie: data["all"])
            for(const HolesItem& p: pSerie->holes())
            {
                int x =   p->x() / granularity + nx/2;
                int y = -(p->y() / granularity ) + ny/2;
                z = colorMap->data()->cell(x, y);
                colorMap->data()->setCell(x, y, z + 1);
            }

        // add a color scale:
        QCPColorScale *colorScale = new QCPColorScale( ui->pColorTab);
        colorScale->setGradient(QCPColorGradient::gpGeography);
        QCPLayoutElement* pElem =  ui->pColorTab->plotLayout()->element(0,1);
        if (pElem)
             ui->pColorTab->plotLayout()->remove(pElem);
         ui->pColorTab->plotLayout()->addElement(0, 1, colorScale); // add it to the right of the main axis rect
        colorScale->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
        colorMap->setColorScale(colorScale); // associate the color map with the color scale
        colorScale->axis()->setLabel(tr("Shots density"));

        // set the color gradient of the color map to one of the presets:
        colorMap->setGradient(QCPColorGradient::gpPolar);
        // we could have also created a QCPColorGradient instance and added own colors to
        // the gradient, see the documentation of QCPColorGradient for what's possible.

        // rescale the data dimension (color) such that all data points lie in the span visualized by the color gradient:
        colorMap->rescaleDataRange();

        // make sure the axis rect and color scale synchronize their bottom and top margins (so they line up):
        QCPMarginGroup *marginGroup = new QCPMarginGroup( ui->pColorTab);
         ui->pColorTab->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
        colorScale->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);

        // rescale the key (x) and value (y) axes so the whole color map is visible:
         ui->pColorTab->rescaleAxes();
    }
    ui->pColorTab->replot();
}

void AnalyzeForm::drawDispersionGraph(const QMap<QString, QVector<SeriePtr> >& data)
{
    ui->pDispersionTab->clearPlottables();
    if (!data.isEmpty() && !data["all"].isEmpty())
    {
        QVector<double> timePoints;
        QVector<QString> labels;
        double minScore = 10;
        double maxScore = 4;

        ui->pDispersionTab->legend->setVisible(true);
        ui->pDispersionTab->legend->setFont(QFont("Helvetica", 9));

        int graphCnt = 0;
        for (const QString& splitName: data.keys())
        {
            if (splitName == "all" && data.size() > 1)
                continue;

            const QVector<SeriePtr>& split = data[splitName];


            QPen boundsPen(QColor(qrand() % 255, qrand() % 255, qrand() % 255));
            QPen  valuePen(QColor(qrand() % 255, qrand() % 255, qrand() % 255));

            ui->pDispersionTab->addGraph();
            ui->pDispersionTab->graph(graphCnt + 0)->setPen(valuePen);
            ui->pDispersionTab->graph(graphCnt + 0)->setScatterStyle(QCPScatterStyle::ssCircle);
            ui->pDispersionTab->graph(graphCnt + 0)->setName(splitName + " mean");

            ui->pDispersionTab->addGraph();
            ui->pDispersionTab->graph(graphCnt + 1)->setPen(boundsPen);
            ui->pDispersionTab->graph(graphCnt + 1)->setScatterStyle(QCPScatterStyle::ssCircle);
            ui->pDispersionTab->graph(graphCnt + 1)->setName(splitName + " sd");

            QVector<double> meanScore;
            QVector<double> sdScore;

            int i=0;
            for(SeriePtr serie: split)
            {
                if (!timePoints.contains(i))
                {
                    timePoints << i;
                    labels << QString::number(i);
                }
                double mean = serie->massCenter().radius();
                double sd = serie->sd().value(Length::Unit::mm);
                meanScore << mean;
                sdScore << sd;
                minScore = qMin(minScore, qMin(sd, mean));
                maxScore = qMax(maxScore, qMax(sd, mean));

                i++;
            }
            ui->pDispersionTab->graph(graphCnt+0)->setData(timePoints, meanScore);
            ui->pDispersionTab->graph(graphCnt+1)->setData(timePoints, sdScore);


            graphCnt += 2;
        }

        ui->pDispersionTab->xAxis->setRange(timePoints[0]-1, timePoints[timePoints.size()-1]+1);
        ui->pDispersionTab->xAxis->setTickVector(timePoints);
        ui->pDispersionTab->xAxis->setTickVectorLabels(labels);

        ui->pDispersionTab->yAxis->setRange(0, maxScore);
    }

    ui->pDispersionTab->xAxis->setAutoTicks(false);
    ui->pDispersionTab->xAxis->setAutoTickLabels(false);

    ui->pDispersionTab->replot();
}

void AnalyzeForm::onFilterChanged()
{
    auto filtered = collection
            .setSort(SerieCollection::SortCriteria::Date, SerieCollection::SortOrder::Asc)
            .setSort(SerieCollection::SortCriteria::SerieNumber, SerieCollection::SortOrder::Asc)
            .filter();
    ui->pFilter->setMatchCount(filtered["all"].size());

    drawShotsBar(filtered);
    drawScoreGraph(filtered);
    drawColorMap(filtered);
    drawDispersionGraph(filtered);
}

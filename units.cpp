#include "units.h"

Length Angle::toLength(const Length &atDistance) const
{
    double m = atDistance.value(Length::Unit::m);
    double mrad = value(Angle::Unit::mil);
    return Length( m * mrad , Length::Unit::mm);
}

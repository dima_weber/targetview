// GNU Ballistics Library
// Originally created by Derek Yates
// Now available free under the GNU GPL

#ifndef lib_ballistics
#define lib_ballistics

#include "units.h"

#include <gsl/gsl_spline.h>
#include <gsl/gsl_const_mks.h>

#include <memory>
#include <array>

#define BCOMP_MAXRANGE 20000

enum __DragFunctions {G1=1,G2,G3,G4,G5,G6,G7,G8};

struct AtmosphereParams
{
    Speed windSpeed;
    Angle windAngle;
    Length altitude;
    Pressure pressure;
    Temperature temperature;
    double humidity;
};

struct BulletParams
{
    int dragFunction;
    double bc;
    Speed muzzleVelocity;
    Length zero_range;
    Length sightHeight;
    Angle shootingAngle;
};

class Solution
{
    enum {Distance=0, Drop, FlightTime, Wind, Velocity, X, Y, _SIZE};

    std::array<gsl_interp_accel*, _SIZE> acc;
    std::array<gsl_spline*, _SIZE> spline ;

    BulletParams bulletParams;
    AtmosphereParams atmosphereParams;

    Length _max_range;
public:
    typedef std::shared_ptr<Solution> Ptr;
    Solution();
    ~Solution();

    Solution(const Solution&)  = delete;
    Solution& operator=(const Solution& ) = delete;

    const BulletParams& bullet() const {return bulletParams;}
    const AtmosphereParams& atmosphere() const {return atmosphereParams;}

    Length max_range() const { return _max_range;}

    void build (const BulletParams& bulletParams, const AtmosphereParams& atmosphereParams);


    Length drop(Length range) const { return Length(gsl_spline_eval(spline[Drop], range.rawValue(), acc[Drop])); }
    Angle vertCorr(Length range) const { return drop(range).toAngle(range);}
    Time time(Length range) const { return Time(gsl_spline_eval(spline[FlightTime], range.rawValue(), acc[FlightTime]));}
    Length windage(Length range) const { return Length(gsl_spline_eval(spline[Wind], range.rawValue(), acc[Wind]));}
    Speed velocity(Length range) const { return Speed(gsl_spline_eval(spline[Velocity], range.rawValue(), acc[Velocity]));}
    Angle horCorr(Length range) const { return windage(range).toAngle(range);}
    Length x(Length range) const {  return Length(gsl_spline_eval(spline[X], range.rawValue(), acc[X])); }
    Length y(Length range) const {  return Length(gsl_spline_eval(spline[Y], range.rawValue(), acc[Y])); }
private:
    /*!  @brief A function to determine the bore angle needed to achieve a target zero at Range yards (at standard conditions and on level ground.)

         @param DragFunction:  The drag function to use (G1, G2, G3, G5, G6, G7, G8)
         @param DragCoefficient:  The coefficient of drag for the projectile, for the supplied drag function.
         @param Vi:  The initial velocity of the projectile, in feet/s
         @param SightHeight:  The height of the sighting system above the bore centerline, in inches.
                          Most scopes fall in the 1.6 to 2.0 inch range.
         @param ZeroRange:  The range in yards, at which you wish the projectile to intersect yIntercept.
         @param yIntercept:  The height, in inches, you wish for the projectile to be when it crosses ZeroRange yards.
                        This is usually 0 for a target zero, but could be any number.  For example if you wish
                        to sight your rifle in 1.5" high at 100 yds, then you would set yIntercept to 1.5, and ZeroRange to 100

        @return  Returns the angle of the bore relative to the sighting system, in degrees.
    */
    Angle ZeroAngle(int dragFunction, double dragCoefficient, const Speed& muzzleVelocity, const Length& sightHeight, const Length& zeroRange, const Length& yIntercept = Length(0));

    /*! \brief A function to correct a "standard" Drag Coefficient for differing atmospheric conditions.

     @return Returns the corrected drag coefficient for supplied drag coefficient and atmospheric conditions.
            \param DragCoefficient:  The coefficient of drag for a given projectile.
            \param Altitude:  The altitude above sea level in feet.  Standard altitude is 0 feet above sea level.
            \param Barometer:  The barometric pressure in inches of mercury (in Hg).
                        This is not "absolute" pressure, it is the "standardized" pressure reported in the papers and news.
                        Standard pressure is 29.53 in Hg.
            \param Temperature:  The temperature in Fahrenheit.  Standard temperature is 59 degrees.
            \param RelativeHumidity:  The relative humidity fraction.  Ranges from 0.00 to 1.00, with 0.50 being 50% relative humidity.
                                Standard humidity is 78%

            \return The function returns a ballistic coefficient, corrected for the supplied atmospheric conditions.
    */
     double AtmCorrect(double dragCoefficient, const Length& alt, const Pressure& pressure, const Temperature& temp, double humidity);

    /*!
        @brief A function to generate a ballistic solution table in 1 yard increments, up to __BCOMP_MAXRANGE__.

        @param DragFunction:  The drag function you wish to use for the solution (G1, G2, G3, G5, G6, G7, or G8)
        @param DragCoefficient:  The coefficient of drag for the projectile you wish to model.
        @param Vi:  The projectile initial velocity.
        @param SightHeight:  The height of the sighting system above the bore centerline.
                        Most scopes are in the 1.5"-2.0" range.
        @param ShootingAngle:  The uphill or downhill shooting angle, in degrees.  Usually 0, but can be anything from
                        90 (directly up), to -90 (directly down).
        @param ZeroAngle:  The angle of the sighting system relative to the bore, in degrees.  This can be easily computed
                    using the ZeroAngle() function documented above.
        @param WindSpeed:  The wind velocity, in mi/hr
        @param WindAngle:  The angle at which the wind is approaching from, in degrees.
                    0 degrees is a straight headwind
                    90 degrees is from right to left
                    180 degrees is a straight tailwind
                    -90 or 270 degrees is from left to right.
        @param Solution:	A pointer provided for accessing the solution after it has been generated.
                    Memory for this pointer is allocated in the function, so the user does not need
                    to worry about it.  This solution can be passed to the retrieval functions to get
                    useful data from the solution.
            @return			This function returns an integer representing the maximum valid range of the
                    solution.  This also indicates the maximum number of rows in the solution matrix,
                    and should not be exceeded in order to avoid a memory segmentation fault.

    */
    double SolveAll(int dragFunction, double dragCoefficient, const Speed& muzzleVelocity, const Length& sightHeight, \
            Angle shootingAngle, Angle zeroAngle, const Speed& windSpeed, const Angle& windAngle);

    // For very steep shooting angles, Vx can actually become what you would think of as Vy relative to the ground,
    // because Vx is referencing the bore's axis.  All computations are carried out relative to the bore's axis, and
    // have very little to do with the ground's orientation.

    struct PointBlankRange
    {
        Length nearZero;
        Length farZero;
        Length pbrStart;
        Length pbrEnd;
    };
    // A helper function for finding the PBR for a cartridge.
    // TODO:  Document this function.
    int pointBlankRange(int dragFunction, double dragCoefficient, const Speed& muzzleVelocity, const Length& sightHeight, const Length& vitalSize, PointBlankRange &outResult);
};


#endif




#include "ballistics.h"

#include <gsl/gsl_poly.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_roots.h>

#define SPD_POW

Acceleration GRAVITY (Speed(-GSL_CONST_MKS_GRAV_ACCEL));
Length iterationStep (10, Length::Unit::cm);

inline double power(double a, double b)
{
#ifdef STD_POW
    return ::pow(a,b);
#else
    // http://martin.ankerl.com/2007/10/04/optimized-pow-approximation-for-java-and-c-c/
    union {
      double d;
      int x[2];
    } u = { a };
    u.x[1] = (int)(b * (u.x[1] - 1072632447) + 1072632447);
    u.x[0] = 0;
    return u.d;
#endif
}

/// ############ Functions for correcting for atmosphere.
inline double calcFR(const Temperature& temp, const Pressure&  press, double humidity)
{
    const int len = 4;
    const double coeff[len] = {-0.2517, 0.0234, -0.0004, 4e-6};
    double VPw = gsl_poly_eval(coeff, len, temp.value(Temperature::Unit::f));

    double FRH = press / (press - Pressure(0.3783 * humidity * VPw, Pressure::Unit::inchhg)) * 0.995;
    return FRH;
}

inline double calcFP(const Pressure& press)
{
    Pressure  Pstd (29.53, Pressure::Unit::inchhg);
    return (press-Pstd)/(Pstd);
}

inline double calcFT(const Temperature& temp, const Length& alt)
{
    Temperature Tstd (15 - alt.value(Length::Unit::m) / 100 * .55, Temperature::Unit::c);
    double FT = 1.8 * ((temp-Tstd)/(Temperature(459.6, Temperature::Unit::c)+Tstd));
    return FT;
}

inline double calcFA(const Length& alt)
{
    double fa=0;
    const int len = 4;
    const double coeff[len] = {1, -3e-5, 4e-10, -4e-15};
    fa = gsl_poly_eval(coeff, len, alt.value(Length::Unit::yard));
    return (1/fa);
}

double Solution::AtmCorrect(double dragCoefficient, const Length& alt, const Pressure& pressure, const Temperature& temp, double humidity)
{

    double FA = calcFA(alt);
    double FT = calcFT(temp, alt);
    double FR = calcFR(temp, pressure, humidity);
    double FP = calcFP(pressure);

    // Calculate the atmospheric correction factor
    double CD = (FA*(1+FT-FP)*FR);
    return dragCoefficient*CD;
}


// ############ Functions for correcting for ballistic drag.
/* A function to calculate ballistic retardation values based on standard drag functions.
   Arguments:
        DragFunction:  G1, G2, G3, G4, G5, G6, G7, or G8.  All are enumerated above.
        DragCoefficient:  The coefficient of drag for the projectile for the given drag function.
        Vi:  The Velocity of the projectile.

    Return Value:
        The function returns the projectile drag retardation velocity, in ft/s per second.

*/
Acceleration retard(int DragFunction, double DragCoefficient, const Speed& Velocity)
{
    double vp=Velocity.value(Length::Unit::ft, Time::Unit::s);
    double A=-1;
    double M=-1;
    switch(DragFunction)
    {
        case G1:
                 if (vp> 4230) { A = 1.477404177730177e-04; M = 1.9565; }
            else if (vp> 3680) { A = 1.920339268755614e-04; M = 1.925 ; }
            else if (vp> 3450) { A = 2.894751026819746e-04; M = 1.875 ; }
            else if (vp> 3295) { A = 4.349905111115636e-04; M = 1.825 ; }
            else if (vp> 3130) { A = 6.520421871892662e-04; M = 1.775 ; }
            else if (vp> 2960) { A = 9.748073694078696e-04; M = 1.725 ; }
            else if (vp> 2830) { A = 1.453721560187286e-03; M = 1.675 ; }
            else if (vp> 2680) { A = 2.162887202930376e-03; M = 1.625 ; }
            else if (vp> 2460) { A = 3.209559783129881e-03; M = 1.575 ; }
            else if (vp> 2225) { A = 3.904368218691249e-03; M = 1.55  ; }
            else if (vp> 2015) { A = 3.222942271262336e-03; M = 1.575 ; }
            else if (vp> 1890) { A = 2.203329542297809e-03; M = 1.625 ; }
            else if (vp> 1810) { A = 1.511001028891904e-03; M = 1.675 ; }
            else if (vp> 1730) { A = 8.609957592468259e-04; M = 1.75  ; }
            else if (vp> 1595) { A = 4.086146797305117e-04; M = 1.85  ; }
            else if (vp> 1520) { A = 1.954473210037398e-04; M = 1.95  ; }
            else if (vp> 1420) { A = 5.431896266462351e-05; M = 2.125 ; }
            else if (vp> 1360) { A = 8.847742581674416e-06; M = 2.375 ; }
            else if (vp> 1315) { A = 1.456922328720298e-06; M = 2.625 ; }
            else if (vp> 1280) { A = 2.419485191895565e-07; M = 2.875 ; }
            else if (vp> 1220) { A = 1.657956321067612e-08; M = 3.25  ; }
            else if (vp> 1185) { A = 4.745469537157371e-10; M = 3.75  ; }
            else if (vp> 1150) { A = 1.379746590025088e-11; M = 4.25  ; }
            else if (vp> 1100) { A = 4.070157961147882e-13; M = 4.75  ; }
            else if (vp> 1060) { A = 2.938236954847331e-14; M = 5.125 ; }
            else if (vp> 1025) { A = 1.228597370774746e-14; M = 5.25  ; }
            else if (vp>  980) { A = 2.916938264100495e-14; M = 5.125 ; }
            else if (vp>  945) { A = 3.855099424807451e-13; M = 4.75  ; }
            else if (vp>  905) { A = 1.185097045689854e-11; M = 4.25  ; }
            else if (vp>  860) { A = 3.566129470974951e-10; M = 3.75  ; }
            else if (vp>  810) { A = 1.045513263966272e-08; M = 3.25  ; }
            else if (vp>  780) { A = 1.291159200846216e-07; M = 2.875 ; }
            else if (vp>  750) { A = 6.824429329105383e-07; M = 2.625 ; }
            else if (vp>  700) { A = 3.569169672385163e-06; M = 2.375 ; }
            else if (vp>  640) { A = 1.839015095899579e-05; M = 2.125 ; }
            else if (vp>  600) { A = 5.71117468873424e-05 ; M = 1.950 ; }
            else if (vp>  550) { A = 9.226557091973427e-05; M = 1.875 ; }
            else if (vp>  250) { A = 9.337991957131389e-05; M = 1.875 ; }
            else if (vp>  100) { A = 7.225247327590413e-05; M = 1.925 ; }
            else if (vp>   65) { A = 5.792684957074546e-05; M = 1.975 ; }
            else if (vp>    0) { A = 5.206214107320588e-05; M = 2.000 ; }
            break;

        case G2:
            if (vp> 1674 ) { A = .0079470052136733   ;  M = 1.36999902851493; }
            else if (vp> 1172 ) { A = 1.00419763721974e-03;  M = 1.65392237010294; }
            else if (vp> 1060 ) { A = 7.15571228255369e-23;  M = 7.91913562392361; }
            else if (vp>  949 ) { A = 1.39589807205091e-10;  M = 3.81439537623717; }
            else if (vp>  670 ) { A = 2.34364342818625e-04;  M = 1.71869536324748; }
            else if (vp>  335 ) { A = 1.77962438921838e-04;  M = 1.76877550388679; }
            else if (vp>    0 ) { A = 5.18033561289704e-05;  M = 1.98160270524632; }
            break;

        case G5:
            if (vp> 1730 ){ A = 7.24854775171929e-03; M = 1.41538574492812; }
            else if (vp> 1228 ){ A = 3.50563361516117e-05; M = 2.13077307854948; }
            else if (vp> 1116 ){ A = 1.84029481181151e-13; M = 4.81927320350395; }
            else if (vp> 1004 ){ A = 1.34713064017409e-22; M = 7.8100555281422 ; }
            else if (vp>  837 ){ A = 1.03965974081168e-07; M = 2.84204791809926; }
            else if (vp>  335 ){ A = 1.09301593869823e-04; M = 1.81096361579504; }
            else if (vp>    0 ){ A = 3.51963178524273e-05; M = 2.00477856801111; }
            break;

        case G6:
            if (vp> 3236 ) { A = 0.0455384883480781   ; M = 1.15997674041274; }
            else if (vp> 2065 ) { A = 7.167261849653769e-02; M = 1.10704436538885; }
            else if (vp> 1311 ) { A = 1.66676386084348e-03 ; M = 1.60085100195952; }
            else if (vp> 1144 ) { A = 1.01482730119215e-07 ; M = 2.9569674731838 ; }
            else if (vp> 1004 ) { A = 4.31542773103552e-18 ; M = 6.34106317069757; }
            else if (vp>  670 ) { A = 2.04835650496866e-05 ; M = 2.11688446325998; }
            else if (vp>    0 ) { A = 7.50912466084823e-05 ; M = 1.92031057847052; }
            break;

        case G7:
            if (vp> 4200 ) { A = 1.29081656775919e-09; M = 3.24121295355962; }
            else if (vp> 3000 ) { A = 0.0171422231434847  ; M = 1.27907168025204; }
            else if (vp> 1470 ) { A = 2.33355948302505e-03; M = 1.52693913274526; }
            else if (vp> 1260 ) { A = 7.97592111627665e-04; M = 1.67688974440324; }
            else if (vp> 1110 ) { A = 5.71086414289273e-12; M = 4.3212826264889 ; }
            else if (vp>  960 ) { A = 3.02865108244904e-17; M = 5.99074203776707; }
            else if (vp>  670 ) { A = 7.52285155782535e-06; M = 2.1738019851075 ; }
            else if (vp>  540 ) { A = 1.31766281225189e-05; M = 2.08774690257991; }
            else if (vp>    0 ) { A = 1.34504843776525e-05; M = 2.08702306738884; }
            break;

        case G8:
            if (vp> 3571 ) { A = .0112263766252305   ; M = 1.33207346655961; }
            else if (vp> 1841 ) { A = .0167252613732636   ; M = 1.28662041261785; }
            else if (vp> 1120 ) { A = 2.20172456619625e-03; M = 1.55636358091189; }
            else if (vp> 1088 ) { A = 2.0538037167098e-16 ; M = 5.80410776994789; }
            else if (vp>  976 ) { A = 5.92182174254121e-12; M = 4.29275576134191; }
            else if (vp>    0 ) { A = 4.3917343795117e-05 ; M = 1.99978116283334; }
            break;

        default:
            break;

    }

    if (A>0 && M>0 && vp>0 && vp<10000)
        return Acceleration(Speed(A*power(vp,M)/DragCoefficient, Length::Unit::ft, Time::Unit::s), Time::Unit::s);
    else
        return Acceleration(Speed(-1));
}

/* A function to compute the windage deflection for a given crosswind speed,
   given flight time in a vacuum, and given flight time in real life.
   Returns the windage correction needed in inches.
   Arguments:
        WindSpeed:  The wind velocity in mi/hr.
        Vi:  The initial velocity of the projectile (muzzle velocity).
        x:  The range at which you wish to determine windage, in feet.
        t:  The time it has taken the projectile to traverse the range x, in seconds.

    Return Value:
        Returns the amount of windage correction, in inches, required to achieve zero on a target at the given range.

*/
Length Windage(const Speed& WindSpeed, const Speed& Vi, const Length& x, const Time& t)
{
    return (WindSpeed*(t-x/Vi));
}


/* Functions to resolve any wind / angle combination into headwind and crosswind components.
   Arguments:
        WindSpeed:  The wind velocity, in mi/hr.
        WindAngle:  The angle from which the wind is coming, in degrees.
                    0 degrees is from straight ahead
                    90 degrees is from right to left
                    180 degrees is from directly behind
                    270 or -90 degrees is from left to right.

    Return value:
        Returns the headwind or crosswind velocity component, in mi/hr.
    Headwind is positive at WindAngle=0
    Crosswind is positive from Shooter's Right to Left (Wind from 90 degree)
*/
void HeadCrossWind(const Speed& WindSpeed, const Angle& WindAngle, Speed& headwind, Speed& crosswind)
{
    headwind = WindSpeed * WindAngle.cos();
    crosswind = WindSpeed * WindAngle.sin();
}

Solution::Solution()
{
    for (size_t i = 0; i < _SIZE; i++)
    {
        acc[i] = gsl_interp_accel_alloc ();
        spline[i] = gsl_spline_alloc (gsl_interp_cspline, BCOMP_MAXRANGE);
    }
}

Solution::~Solution()
{
    for (size_t i=0; i < _SIZE; i++)
    {
        gsl_spline_free (spline[i]);
        gsl_interp_accel_free (acc[i]);
    }
}

void Solution::build(const BulletParams& bulletParams, const AtmosphereParams& atmosphereParams)
{
    this->bulletParams = bulletParams;
    this->atmosphereParams = atmosphereParams;
    double bc = AtmCorrect(bullet().bc, atmosphere().altitude, atmosphere().pressure, atmosphere().temperature, atmosphere().humidity);
    Angle zeroAngle = ZeroAngle(bullet().dragFunction, bc, bullet().muzzleVelocity, bullet().sightHeight, bullet().zero_range);
    double max_range =  SolveAll(bullet().dragFunction, bc, bullet().muzzleVelocity, bullet().sightHeight, bullet().shootingAngle, zeroAngle, atmosphere().windSpeed, atmosphere().windAngle);
    _max_range = Length (max_range, Length::Unit::yard);
}

struct GetZeroDistParams
{
    double dragCoefficient;
    Speed vi;
    Length sightHeight;
    Length zeroRange;
    Length yIntercept;
    int dragFunction;
};

double get_zero_distance_interception_height(double angle, void* p) // radians
{
    Angle alpha(angle,Angle::Unit::rad);
    GetZeroDistParams* params = static_cast<GetZeroDistParams*>(p);
    Speed vy=params->vi*alpha.sin();
    Speed vx=params->vi*alpha.cos();
    Acceleration Gx=GRAVITY*alpha.sin();
    Acceleration Gy=GRAVITY*alpha.cos();

    Time t(0);
    Length x(0);
    Length y = -params->sightHeight;
    Time dt = iterationStep/params->vi;
    Speed dvx;
    Speed dvy;
    Speed vy1;
    Speed vx1;
    Speed v;

    for (;x<=params->zeroRange;t+=dt)
    {
        vy1=vy;
        vx1=vx;
        v.setOrts(vx, vy);
        dt=iterationStep/v;

        Acceleration dv = retard(params->dragFunction, params->dragCoefficient, v);
        dvy = (dv * dt) * (-vy/v);
        dvx = (dv * dt) * (-vx/v);

        vx += dvx + Gx * dt;
        vy += dvy + Gy * dt;

        x += (vx+vx1) * dt / 2;
        y += (vy+vy1) * dt / 2;
        // Break early to save CPU time if we won't find a solution.
        if ((vy<Speed(0) && y<params->yIntercept) || vx < vy)
        {
            break;
        }
    }
    return (y - params->yIntercept).rawValue();
}

Angle Solution::ZeroAngle(int dragFunction, double dragCoefficient, const Speed& muzzleVelocity, const Length& sightHeight, const Length& zeroRange, const Length& yIntercept)
{
    double angle=0; // The actual angle of the bore.

    double minAngle = 0;
    double maxAngle = Angle::DegToRad(45);
    const double precision = Angle::MoaToRad(0.001);

    GetZeroDistParams params = {dragCoefficient, muzzleVelocity, sightHeight, zeroRange, yIntercept, dragFunction};

    int status;
    int iter = 0;
    int max_iter = 100;
    const gsl_root_fsolver_type *T;
    gsl_root_fsolver *s;
    gsl_function F;

    F.function = &get_zero_distance_interception_height;
    F.params = &params;

    T = gsl_root_fsolver_brent;
    s = gsl_root_fsolver_alloc (T);
    gsl_root_fsolver_set (s, &F, minAngle, maxAngle);

    do
    {
        iter++;
        status = gsl_root_fsolver_iterate (s);
        if (status != GSL_SUCCESS)
            break;
        angle = gsl_root_fsolver_root (s);
        minAngle = gsl_root_fsolver_x_lower (s);
        maxAngle = gsl_root_fsolver_x_upper (s);
        status = gsl_root_test_interval (minAngle, maxAngle, 0, precision);
    } while (status == GSL_CONTINUE && iter < max_iter);

    gsl_root_fsolver_free (s);

    return Angle(angle); // Convert to degrees for return value.
}



// The solve-all solution.
double Solution::SolveAll(int dragFunction, double dragCoefficient, const Speed& muzzleVelocity, const Length& sightHeight,
            Angle shootingAngle, Angle zeroAngle, const Speed& windSpeed, const Angle& windAngle)
{
    Time t(0);
    Time dt=iterationStep/muzzleVelocity;
    Speed v, vx, vx1, vy, vy1;
    Acceleration dv, dvx, dvy;
    Length x, y;

    Speed headwind;
    Speed crosswind;
    HeadCrossWind(windSpeed, windAngle, headwind, crosswind);

    Angle alpha = shootingAngle + zeroAngle;
    Acceleration Gy;
    Acceleration Gx;

    vx = muzzleVelocity * zeroAngle.cos();
    vy = muzzleVelocity * zeroAngle.sin();

    y=-sightHeight;

    std::array<double, BCOMP_MAXRANGE> data[_SIZE] = {{{0}}};

    size_t n=0;
    do
    {
        t += dt;
        vx1=vx;
        vy1=vy;
        v.setOrts(vy, vx);
        dt=iterationStep/v;

        Gx = GRAVITY * alpha.sin();
        Gy = GRAVITY * alpha.cos();

        // Compute acceleration using the drag function retardation
        dv = retard(dragFunction,dragCoefficient,v+headwind);
        dvx = dv * (-vx/v);
        dvy = dv * (-vy/v);

        // Compute velocity, including the resolved gravity vectors.
        vx +=  (dvx + Gx) * dt;
        vy +=  (dvy + Gy) * dt;

        //alpha.setValue(atan(vy/vx), Angle::Unit::rad);

        if (x >= Length(n, Length::Unit::m))
        {
            data[Distance][n] = x.rawValue();
            data[Drop][n] = y.rawValue();
            data[FlightTime][n] = t.rawValue();
            data[Wind][n] = Windage(crosswind,muzzleVelocity,x,t).rawValue();
            data[Velocity][n] = v.rawValue();
            data[X][n] = (x * alpha.cos() + y * alpha.sin()).rawValue();
            data[Y][n] = (x * alpha.sin() - y * alpha.cos()).rawValue();
            n++;
        }

        // Compute position based on average velocity.
        x += (vx+vx1)*dt/2;
        y += (vy+vy1)*dt/2;
    } while (fabs(vy.rawValue())<fabs(vx.rawValue()) && n<BCOMP_MAXRANGE);


    size_t maxN = n;

    // this loop just fill data past break -- spline deny to work with not full dataset
    for (; n < BCOMP_MAXRANGE; n++)
    {
        data[Distance][n] = data[Distance][n-1] + 1;
    }

    for (size_t i=Drop; i<_SIZE; i++)
    {
        gsl_spline_init (spline[i], data[Distance].data(), data[i].data(), n);
    }

    return data[Distance][maxN-1];
}

#define TOO_LOW 1
#define TOO_HIGH 2
#define PBR_ERROR 3

//TODO: redo with one-dimension solver (GSL)
// Solves for the maximum Point blank range and associated details.
int Solution::pointBlankRange(int dragFunction, double dragCoefficient, const Speed& muzzleVelocity, const Length& sightHeight, const Length& vitalSize, Solution::PointBlankRange& outResult)
{
    Time t;
    Time dt = iterationStep/muzzleVelocity;
    Speed v, vx, vx1, vy, vy1;
    Acceleration dv, dvx, dvy;
    Length x, y;
    Angle shootingAngle;
    Angle zAngle;
    Angle step(1);

    int result=0;

    Length& zero = outResult.nearZero;
    Length& farzero = outResult.farZero;

    zero.setRawValue(-1);
    farzero.setRawValue(0);

    bool vertex_keep = false;
    Length y_vertex;
//	double x_vertex=0;

    Length& min_pbr_range = outResult.pbrStart;
    bool min_pbr_keep = false;

    Length& max_pbr_range = outResult.pbrEnd;
    bool max_pbr_keep = false;

    Length tin100;

    Acceleration Gy;
    Acceleration Gx;

    do
    {
        Gy = GRAVITY * (shootingAngle + zAngle).cos();
        Gx = GRAVITY * (shootingAngle + zAngle).sin();

        vx = muzzleVelocity * zAngle.cos();
        vy = muzzleVelocity * zAngle.sin();

        y = -sightHeight;
        x.setRawValue(0);

        bool keep = false;
        bool keep2 = false;
        bool tinkeep = false;
        min_pbr_keep = false;
        max_pbr_keep = false;
        vertex_keep = false;

        tin100.setRawValue(0);

        int n=0;
        t.setRawValue(0);

        do
        {
            vx1 = vx;
            vy1 = vy;
            v.setOrts(vx, vy);
            dt = iterationStep/v;

            // Compute acceleration using the drag function retardation
            dv = retard(dragFunction,dragCoefficient,v);
            dvx = dv * (-vx/v);
            dvy = dv * (-vy/v);

            // Compute velocity, including the resolved gravity vectors.
            vx += (dvx + Gx) * dt;
            vy += (dvy + Gy) * dt;

            // Compute position based on average velocity.
            x += (vx+vx1) * dt/2;
            y += (vy+vy1) * dt/2;

            if (y > Length(0) && !keep && Speed(0) < vy)
            {
                zero=x;
                keep = true;
            }

            if (y < Length(0) && !keep2 && vy<= Speed(0))
            {
                farzero=x;
                keep2=true;
            }

            if ( y > -vitalSize/2 && !min_pbr_keep){
                min_pbr_range=x;
                min_pbr_keep=true;
            }

            if ((y)<-(vitalSize/2) && min_pbr_keep && !max_pbr_keep){
                max_pbr_range=x;
                max_pbr_keep=true;
            }

            if (x >= Length(100, Length::Unit::m) && !tinkeep)
            {
                tin100=y;
                tinkeep=true;
            }


            if (fabs(vy.rawValue())>fabs(vx.rawValue())) { result=PBR_ERROR; break; }
            if (n>BCOMP_MAXRANGE) { result=PBR_ERROR; break;}


            // The PBR will be maximum at the point where the vertex is 1/2 vital zone size.
            if (vy < Speed(0) && !vertex_keep)
            {
                y_vertex=y;
                //					x_vertex=x;
                vertex_keep=true;
            }

            t += dt;

        } while (!keep || !keep2 || !min_pbr_keep || !max_pbr_keep || !vertex_keep || !tinkeep);

        if (    (y_vertex >  vitalSize/2 && step > Angle(0))
             || (y_vertex <= vitalSize/2 && step < Angle(0)))
                step /= -2; // Vertex too high.  Go downwards.

        zAngle += step;

    } while (fabs(step.rawValue())<(Angle(0.01, Angle::Unit::moa).rawValue()));

    return result;
}
